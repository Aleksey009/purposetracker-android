package ru.alex009.crossgrid.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.TextView;

import ru.alex009.crossgrid.adapter.GameAdapter;
import ru.alex009.crossgrid.view.BoardView;
import ru.alex009.purpose.R;


public class GameActivity extends Activity implements GameAdapter.onGameEndListener, BoardView.onCellClickListener, GameAdapter.GameScoreHolder
{
	private GameAdapter mBoardAdapter;
	private int mScore;
	private TextView mScoreText;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_game);

		BoardView gameView = (BoardView) findViewById(R.id.gameView);
		mScoreText = (TextView) findViewById(R.id.scoreTextView);

		mBoardAdapter = new GameAdapter(8, 1);
		mBoardAdapter.setOnGameEndListener(this);

		gameView.setAdapter(mBoardAdapter);
		gameView.setOnCellClickListener(this);

		mBoardAdapter.setGameScoreHolder(this);

		setGameScore(0);
	}

	@Override
	public void onCellClick(int i, int j)
	{
		if (mBoardAdapter.getCellState(i, j) != BoardView.EMPTY) return;

		mBoardAdapter.setCellState(i, j, BoardView.OPENED);
	}

	@Override
	public void onGameEnd()
	{
		new AlertDialog.Builder(GameActivity.this)
				.setTitle("Игра окончена")
				.setMessage("Ходов не осталось!")
				.setPositiveButton("OK", new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						setGameScore(0);
					}
				})
				.show();
	}

	@Override
	public int getGameScore()
	{
		return mScore;
	}

	@Override
	public void setGameScore(int score)
	{
		mScore = score;
		mScoreText.setText("Очки: " + score);
	}
}

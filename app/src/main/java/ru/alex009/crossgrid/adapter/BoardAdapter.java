package ru.alex009.crossgrid.adapter;

import android.graphics.Bitmap;

/**
 * Created by AlekseyMikhailov on 17.08.14.
 */
public abstract class BoardAdapter
{
	protected DataSetObserver mObserver;

	public interface DataSetObserver
	{
		public void onDataSetChanged();
	}

	public void notifyDataSetChanged()
	{
		if (mObserver != null) {
			mObserver.onDataSetChanged();
		}
	}

	public void setDataSetObserver(DataSetObserver observer)
	{
		mObserver = observer;
	}

	public abstract int getSize();

	public abstract int getCellState(int i, int j);

	public abstract int getCellColor(int i, int j);

	public abstract boolean isCellChanged(int i, int j);

	public abstract Bitmap getCellStateImage(int state);
}

package ru.alex009.crossgrid.adapter;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import java.util.Stack;
import java.util.Vector;

import ru.alex009.crossgrid.view.BoardView;
import ru.alex009.purpose.R;

/**
 * Created by AlekseyMikhailov on 17.08.14.
 */
public class GameAdapter extends BoardAdapter
{
	public interface GameScoreHolder
	{
		public int getGameScore();

		public void setGameScore(int score);
	}

	public interface onGameEndListener
	{
		public void onGameEnd();
	}

	private int mSize;
	private int[][] mCellState;
	private int[][] mCellDanger;
	private int[][] mCellColor;
	private boolean[][] mCellChanged;
	private Bitmap mOpenedImage;
	private Bitmap mClosedImage;
	private GameScoreHolder mScoreHolder;
	private onGameEndListener mOnGameEndListener;
	private int mActionsCounter;
	private int mActionsMaxCount;

	public GameAdapter(int size, int actionsCount)
	{
		mScoreHolder = null;
		mSize = size;

		mCellState = new int[mSize][mSize];
		mCellDanger = new int[mSize][mSize];
		mCellColor = new int[mSize][mSize];
		mCellChanged = new boolean[mSize][mSize];
		for (int i = 0; i < mSize; i++) {
			for (int j = 0; j < mSize; j++) {
				mCellState[i][j] = BoardView.EMPTY;
				mCellColor[i][j] = Color.WHITE;
				mCellChanged[i][j] = false;
			}
		}
		mActionsMaxCount = actionsCount;
		mActionsCounter = mActionsMaxCount;
	}

	@Override
	public int getSize()
	{
		return mSize;
	}

	@Override
	public int getCellState(int i, int j)
	{
		return mCellState[i][j];
	}

	@Override
	public int getCellColor(int i, int j)
	{
		int last = mCellColor[i][j];


		int a = Color.alpha(mCellColor[i][j]);
		int r = Color.red(mCellColor[i][j]);
		int g = Color.green(mCellColor[i][j]);
		int b = Color.blue(mCellColor[i][j]);

		if (mCellColor[i][j] != 0xFFFFFFFF) {
			mCellColor[i][j] = 0xFFFFFFFF;
		}

		if (mCellColor[i][j] != last) mCellChanged[i][j] = true;

		return last;
	}

	@Override
	public boolean isCellChanged(int i, int j)
	{
		boolean old = mCellChanged[i][j];
		mCellChanged[i][j] = false;
		return old;
	}

	@Override
	public Bitmap getCellStateImage(int state)
	{
		int cellSize = ((BoardView) mObserver).getCellSize();

		switch (state) {
			case BoardView.OPENED: {
				if (mOpenedImage != null) {
					if (mOpenedImage.getHeight() != cellSize) {
						mOpenedImage = Bitmap.createScaledBitmap(mOpenedImage, cellSize, cellSize, false);
					}
				} else {
					Resources res = ((BoardView) mObserver).getResources();
					mOpenedImage = BitmapFactory.decodeResource(res, R.drawable.circle);
					mOpenedImage = Bitmap.createScaledBitmap(mOpenedImage, cellSize, cellSize, false);
				}

				return mOpenedImage;
			}
			case BoardView.CLOSED: {
				if (mClosedImage != null) {
					if (mClosedImage.getHeight() != cellSize) {
						mClosedImage = Bitmap.createScaledBitmap(mClosedImage, cellSize, cellSize, false);
					}
				} else {
					Resources res = ((BoardView) mObserver).getResources();
					mClosedImage = BitmapFactory.decodeResource(res, R.drawable.star);
					mClosedImage = Bitmap.createScaledBitmap(mClosedImage, cellSize, cellSize, false);
				}

				return mClosedImage;
			}
		}

		return null;
	}

	public void setCellState(int i, int j, int state)
	{
		mCellState[i][j] = BoardView.OPENED;

		int bonus = checkCompleteLines();
		if (bonus > 0) {
			if (mScoreHolder != null) {
				int score = mScoreHolder.getGameScore() + bonus;
				mScoreHolder.setGameScore(score);
			}
		} else {
			mActionsCounter--;

			if (mActionsCounter <= 0) {
				int danger = dangerCells();
				Stack<Point> nextPoints = new Stack<Point>();

				for (int li = 0; li < mSize; li++) {
					for (int lj = 0; lj < mSize; lj++) {
						if (mCellDanger[li][lj] == danger) {
							Point p = new Point();
							p.i = li;
							p.j = lj;
							nextPoints.add(p);
						}
					}
				}


				if (nextPoints.size() > 0) {
					int select = (int) Math.floor(Math.random() * nextPoints.size());
					Point selected = nextPoints.get(select);

					mCellState[selected.i][selected.j] = BoardView.CLOSED;
				}

				mActionsCounter = mActionsMaxCount;
			}

			if (getCellEmptyCount() == 0) {
				if (mOnGameEndListener != null) {
					mOnGameEndListener.onGameEnd();
				}

				reset();
			}
		}

		notifyDataSetChanged();
	}

	public void setGameScoreHolder(GameScoreHolder holder)
	{
		mScoreHolder = holder;
	}

	public void setOnGameEndListener(onGameEndListener listener)
	{
		mOnGameEndListener = listener;
	}

	public void reset()
	{
		for (int li = 0; li < mSize; li++) {
			for (int lj = 0; lj < mSize; lj++) {
				mCellState[li][lj] = BoardView.EMPTY;
				mCellColor[li][lj] = Color.WHITE;
			}
		}
		mActionsCounter = mActionsMaxCount;

		notifyDataSetChanged();
	}

	private int dangerCells()
	{
		int maxDanger = 0;
		CellCounter counter = new CellCounter();

		for (int i = 0; i < mSize; i++) {
			for (int j = 0; j < mSize; j++) {
				mCellDanger[i][j] = 0;

				if (mCellState[i][j] != BoardView.EMPTY)
					continue;

				counter.setCount(0);
				searchCells(i, j, -1, 0, BoardView.OPENED, counter);
				if (mCellDanger[i][j] < counter.getCount()) mCellDanger[i][j] = counter.getCount();

				counter.setCount(0);
				searchCells(i, j, +1, 0, BoardView.OPENED, counter);
				if (mCellDanger[i][j] < counter.getCount()) mCellDanger[i][j] = counter.getCount();

				counter.setCount(0);
				searchCells(i, j, 0, -1, BoardView.OPENED, counter);
				if (mCellDanger[i][j] < counter.getCount()) mCellDanger[i][j] = counter.getCount();

				counter.setCount(0);
				searchCells(i, j, 0, +1, BoardView.OPENED, counter);
				if (mCellDanger[i][j] < counter.getCount()) mCellDanger[i][j] = counter.getCount();

				counter.setCount(0);
				searchCells(i, j, -1, +1, BoardView.OPENED, counter);
				if (mCellDanger[i][j] < counter.getCount()) mCellDanger[i][j] = counter.getCount();

				counter.setCount(0);
				searchCells(i, j, +1, -1, BoardView.OPENED, counter);
				if (mCellDanger[i][j] < counter.getCount()) mCellDanger[i][j] = counter.getCount();

				counter.setCount(0);
				searchCells(i, j, -1, -1, BoardView.OPENED, counter);
				if (mCellDanger[i][j] < counter.getCount()) mCellDanger[i][j] = counter.getCount();

				counter.setCount(0);
				searchCells(i, j, +1, +1, BoardView.OPENED, counter);
				if (mCellDanger[i][j] < counter.getCount()) mCellDanger[i][j] = counter.getCount();

				if (mCellDanger[i][j] > maxDanger)
					maxDanger = mCellDanger[i][j];
			}
		}

		return maxDanger;
	}

	private int checkCompleteLines()
	{
		int bonus = 0;

		for (int i = 0; i < mSize; i++) {
			for (int j = 0; j < mSize; j++) {
				if (mCellState[i][j] != BoardView.OPENED)
					continue;

				bonus += clearCompletedLines(i, j, +1, 0);
				bonus += clearCompletedLines(i, j, +1, +1);
				bonus += clearCompletedLines(i, j, -1, +1);
				bonus += clearCompletedLines(i, j, 0, +1);
			}
		}

		return bonus;
	}

	private int clearCompletedLines(int i, int j, int horizontal, int vertical)
	{
		CellCounter counter = new CellCounter();

		counter.setCount(0);
		searchCells(i, j, -horizontal, -vertical, BoardView.OPENED, counter);
		searchCells(i, j, +horizontal, +vertical, BoardView.OPENED, counter);

		int len = counter.getCount() + 1;

		if (len >= 5) {
			CellDestroyer destroyer = new CellDestroyer();

			searchCells(i, j, -horizontal, -vertical, -1, destroyer);
			mCellState[i][j] = BoardView.EMPTY;
			searchCells(i, j, +horizontal, +vertical, -1, destroyer);

			destroyer.boomClosedCells();

			return len;
		}
		return 0;
	}

	private int getCellEmptyCount()
	{
		int count = 0;

		for (int li = 0; li < mSize; li++) {
			for (int lj = 0; lj < mSize; lj++) {
				if (mCellState[li][lj] == BoardView.EMPTY) count++;
			}
		}

		return count;
	}

	private void searchCells(int starti, int startj, int horizontal, int vertical, int filter_state, onCellFoundListener listener)
	{
		for (int k = 1; k < mSize; k++) {
			int ni = starti + horizontal * k;
			int nj = startj + vertical * k;

			if ((ni >= 0) &&
					(ni < mSize) &&
					(nj >= 0) &&
					(nj < mSize)) {
				if (filter_state != -1) {
					if (mCellState[ni][nj] == filter_state) listener.onCellFound(ni, nj);
					else break;
				} else listener.onCellFound(ni, nj);
			} else break;
		}
	}

	private interface onCellFoundListener
	{
		public void onCellFound(int i, int j);
	}

	private class CellCounter implements onCellFoundListener
	{
		private int mCount;

		public void setCount(int count)
		{
			mCount = count;
		}

		public int getCount()
		{
			return mCount;
		}

		@Override
		public void onCellFound(int i, int j)
		{
			mCount++;
		}
	}

	private class CellDestroyer implements onCellFoundListener
	{
		private Vector<Point> mClosedCells;

		public CellDestroyer()
		{
			mClosedCells = new Vector<Point>();
		}

		@Override
		public void onCellFound(int i, int j)
		{
			if (mCellState[i][j] == BoardView.CLOSED) {
				Point p = new Point();
				p.i = i;
				p.j = j;
				mClosedCells.add(p);
			}

			mCellState[i][j] = BoardView.EMPTY;
		}

		public void boomClosedCells()
		{
			onCellFoundListener starBoom = new onCellFoundListener()
			{
				@Override
				public void onCellFound(int i, int j)
				{
					mCellState[i][j] = BoardView.EMPTY;
					mCellColor[i][j] = 0xFF999999;
				}
			};

			for (Point p : mClosedCells) {
				searchCells(p.i, p.j, -1, 0, -1, starBoom);
				searchCells(p.i, p.j, +1, 0, -1, starBoom);
				searchCells(p.i, p.j, 0, -1, -1, starBoom);
				searchCells(p.i, p.j, 0, +1, -1, starBoom);
				searchCells(p.i, p.j, -1, +1, -1, starBoom);
				searchCells(p.i, p.j, +1, -1, -1, starBoom);
				searchCells(p.i, p.j, -1, -1, -1, starBoom);
				searchCells(p.i, p.j, +1, +1, -1, starBoom);
			}
		}
	}

	class Point
	{
		public int i;
		public int j;
	}
}

package ru.alex009.crossgrid.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import ru.alex009.crossgrid.adapter.BoardAdapter;

/**
 * Created by AlekseyMikhailov on 17.08.14.
 */
public class BoardView extends SurfaceView implements BoardAdapter.DataSetObserver
{
	public final static int EMPTY = 0;
	public final static int OPENED = 1;
	public final static int CLOSED = 2;

	public interface onCellClickListener
	{
		public void onCellClick(int i, int j);
	}

	private BoardAdapter mAdapter;
	private onCellClickListener mOnCellClickListener;
	private Paint mGridPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private Paint mItemsPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private Runnable mRefreshAction = new Runnable()
	{
		@Override
		public void run()
		{
			BoardView.this.onDataSetChanged();
		}
	};

	private void init()
	{
		mGridPaint.setStyle(Paint.Style.STROKE);
		mGridPaint.setStrokeWidth(5);
		mGridPaint.setColor(Color.BLACK);

		mItemsPaint.setStyle(Paint.Style.FILL);
		mItemsPaint.setColor(Color.BLACK);
		mItemsPaint.setTextSize(30);

		mAdapter = null;
		mOnCellClickListener = null;

		if (!isInEditMode()) {
			setZOrderOnTop(true);
			getHolder().setFormat(PixelFormat.TRANSLUCENT);
		}
	}

	public BoardView(Context context)
	{
		super(context);

		init();
	}

	public BoardView(Context context, AttributeSet attrs)
	{
		super(context, attrs);

		init();
	}

	public BoardView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);

		init();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		ViewGroup.LayoutParams lp = getLayoutParams();

		int widthSize = View.MeasureSpec.getSize(widthMeasureSpec);
		int heightSize = View.MeasureSpec.getSize(heightMeasureSpec);

		if (lp.width > 0)
			widthSize = lp.width;
		if (lp.height > 0)
			heightSize = lp.height;

		if (View.MeasureSpec.getMode(widthMeasureSpec) == View.MeasureSpec.EXACTLY) {
			heightSize = widthSize;
		} else
			widthSize = heightSize;

		setMeasuredDimension(widthSize, heightSize);
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		canvas.drawColor(0, android.graphics.PorterDuff.Mode.CLEAR);

		if (mAdapter != null) {
			int count = mAdapter.getSize();
			int width = getWidth();
			int height = getHeight();
			int fieldSize = (width < height ? width : height);
			int cellSize = fieldSize / count;

			Bitmap openedImage = mAdapter.getCellStateImage(OPENED);
			Bitmap closedImage = mAdapter.getCellStateImage(CLOSED);

			boolean changed = false;

			// items
			for (int i = 0; i < count; i++) {
				for (int j = 0; j < count; j++) {
					if (mAdapter.isCellChanged(i, j)) changed = true;

					int color = mAdapter.getCellColor(i, j);

					mItemsPaint.setColor(color);
					mItemsPaint.setStyle(Paint.Style.FILL);
					canvas.drawRect((cellSize * j), (cellSize * i), (cellSize * j) + cellSize, (cellSize * i) + cellSize, mItemsPaint);

					switch (mAdapter.getCellState(i, j)) {
						case OPENED: {
							if (openedImage != null)
								canvas.drawBitmap(openedImage, (cellSize * j), (cellSize * i), mItemsPaint);
							break;
						}
						case CLOSED: {
							if (closedImage != null)
								canvas.drawBitmap(closedImage, (cellSize * j), (cellSize * i), mItemsPaint);
							break;
						}
					}
				}
			}

			// grid
			for (int i = 0; i <= count; i++) {
				canvas.drawLine(cellSize * i, 0, cellSize * i, fieldSize, mGridPaint);
				canvas.drawLine(0, cellSize * i, fieldSize, cellSize * i, mGridPaint);
			}

			if (changed) {
				postDelayed(mRefreshAction, 1000);
			}
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if (mOnCellClickListener == null) return false;
		if (event.getAction() != MotionEvent.ACTION_DOWN) return false;
		if (mAdapter == null) return false;

		int count = mAdapter.getSize();
		int cellSize = getCellSize();

		int x = (int) Math.floor(event.getX() / cellSize);
		int y = (int) Math.floor(event.getY() / cellSize);

		if (x >= count) return false;
		if (y >= count) return false;

		mOnCellClickListener.onCellClick(y, x);
		return true;
	}

	@Override
	public void onDataSetChanged()
	{
		SurfaceHolder holder = getHolder();

		Canvas canvas = holder.lockCanvas();
		draw(canvas);
		holder.unlockCanvasAndPost(canvas);

		invalidate();
	}

	public void setAdapter(BoardAdapter adapter)
	{
		mAdapter = adapter;
		mAdapter.setDataSetObserver(this);
	}

	public void setOnCellClickListener(onCellClickListener listener)
	{
		mOnCellClickListener = listener;
	}

	public int getCellSize()
	{
		if (mAdapter == null) return 0;

		int count = mAdapter.getSize();
		int width = getWidth();
		int height = getHeight();
		int fieldSize = (width < height ? width : height);

		return fieldSize / count;
	}
}

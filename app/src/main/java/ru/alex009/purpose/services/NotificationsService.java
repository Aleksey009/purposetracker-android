package ru.alex009.purpose.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import ru.alex009.purpose.R;
import ru.alex009.purpose.activities.MainActivity;
import ru.alex009.purpose.core.DataBaseConnector;
import ru.alex009.purpose.core.Notifications;
import ru.alex009.purpose.core.QueryBuilder;
import ru.alex009.purpose.core.UserSettings;
import ru.alex009.purpose.receivers.NotificationsAlarm;

public class NotificationsService extends Service
{
	public static final int ACTION_INIT = 0;
	public static final int ACTION_ALARM = 1;

	@Override
	public IBinder onBind(Intent intent)
	{
		throw new UnsupportedOperationException("Not use!");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		int action = intent.getIntExtra("action", ACTION_INIT);

		switch (action) {
			case ACTION_ALARM: {
				SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();
				Cursor cursor = db.rawQuery(QueryBuilder.buildTaskQuery(false), null);

				int columnText = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_TEXT);
				int columnDate = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_DATE);

				long currentDate = Calendar.getInstance().getTimeInMillis();
				String closestText = null;
				long closestDate = 0;
				int failedTasks = 0;
				int allTasks = 0;

				while (cursor.moveToNext()) {
					long date = cursor.getLong(columnDate);

					if (date == 0) continue;

					allTasks++;

					if (date > currentDate) {
						if (closestDate == 0) {
							closestDate = date;
							closestText = cursor.getString(columnText);
						}
					} else {
						failedTasks++;
					}
				}

				cursor.close();

				String title = getResources().getString(R.string.notifications_title_all) + " " + allTasks;
				String text = "";

				if (closestDate != 0) {
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM HH:mm");
					Date date = new Date(closestDate);
					text = getResources().getString(R.string.notifications_closest_task) + " " + closestText + " (" + simpleDateFormat.format(date) + ")";
				} else if (allTasks != 0 && failedTasks == allTasks) {
					text = getResources().getString(R.string.notifications_all_tasks_failed) + " (" + allTasks + ")";
				}

				if (failedTasks > 0) {
					title = getResources().getString(R.string.notifications_tasks_failed) + " (" + failedTasks + "/" + allTasks + ")";
				}

				if (!text.isEmpty()) {
					Intent notificationIntent = new Intent(this, MainActivity.class);
					PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

					NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
							.setSmallIcon(R.drawable.ic_notification)
							.setTicker(getResources().getString(R.string.notifications_tiker))
							.setAutoCancel(true)
							.setWhen(System.currentTimeMillis())
							.setContentTitle(title)
							.setContentText(text)
							.setContentIntent(contentIntent);

					NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
					notificationManager.notify(Notifications.NOTIFICATION_NORMAL, mBuilder.build());
				}
				break;
			}
		}

		long date = Calendar.getInstance().getTimeInMillis() + UserSettings.getNotificationsPeriod() * 60 * 1000;
		NotificationsAlarm.setAlarm(this, date);

		stopSelfResult(startId);

		return super.onStartCommand(intent, flags, startId);
	}
}

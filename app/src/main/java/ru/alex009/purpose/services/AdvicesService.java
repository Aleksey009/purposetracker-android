package ru.alex009.purpose.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import ru.alex009.purpose.R;
import ru.alex009.purpose.activities.MainActivity;
import ru.alex009.purpose.core.DataBaseConnector;
import ru.alex009.purpose.core.NetworkConnector;
import ru.alex009.purpose.core.Notifications;
import ru.alex009.purpose.core.QueryBuilder;
import ru.alex009.purpose.core.UserSettings;
import ru.alex009.purpose.receivers.AdvicesAlarm;

/**
 * Created by AlekseyMikhailov on 16.11.14.
 */
public class AdvicesService extends Service
{
	private Thread mThread;

	@Override
	public IBinder onBind(Intent intent)
	{
		throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		if (mThread == null) {
			mThread = new Thread(new SearchAdvicesRunnable(startId));
			mThread.start();
		} else {
			stopSelfResult(startId);
		}

		return super.onStartCommand(intent, flags, startId);
	}

	private class SearchAdvicesRunnable implements Runnable
	{
		private int mStartId;

		public SearchAdvicesRunnable(int startId)
		{
			mStartId = startId;
		}

		@Override
		public void run()
		{
			int maxAdviceId = 0;
			ArrayList<String> addedAdvices = new ArrayList<String>();

			SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();

			Cursor cursor = db.rawQuery(QueryBuilder.buildMaxAdviceIdQuery(), null);
			if (cursor.moveToFirst()) {
				maxAdviceId = cursor.getInt(0);
			}
			cursor.close();

			cursor = db.rawQuery(QueryBuilder.buildAdvicesTargetsQuery(), null);
			int columnType = cursor.getColumnIndex(QueryBuilder.FIELD_ADVICESTARGETS_TYPE);
			int columnId = cursor.getColumnIndex(QueryBuilder.FIELD_ADVICESTARGETS_ID);
			int columnText = cursor.getColumnIndex(QueryBuilder.FIELD_ADVICESTARGETS_TEXT);

			while (cursor.moveToNext()) {
				int type = cursor.getInt(columnType);
				int id = cursor.getInt(columnId);
				String text = cursor.getString(columnText);

				Uri.Builder builder = new Uri.Builder();
				builder.scheme("http")
						.authority("ajax.googleapis.com")
						.appendPath("ajax")
						.appendPath("services")
						.appendPath("search")
						.appendPath("web")
						.appendQueryParameter("v", "1.0")
						.appendQueryParameter("q", text);

				JSONObject response = NetworkConnector.getJSONFromURL(builder.build().toString());

				if (response == null) continue;

				JSONObject responseData = response.optJSONObject("responseData");
				int responseStatus = response.optInt("responseStatus", 0);

				if (responseStatus == 200 && responseData != null) {
					JSONArray results = responseData.optJSONArray("results");

					if (results != null) {
						for (int i = 0; i < results.length(); i++) {
							JSONObject result = results.optJSONObject(i);

							if (result == null) continue;

							String url = result.optString("url");
							String title = result.optString("titleNoFormatting");
							String content = result.optString("content");

							int adviceId = maxAdviceId + 1;

							ContentValues cv = new ContentValues();
							cv.put("id", adviceId);
							cv.put("title", title);
							cv.put("description", content);
							cv.put("url", url);
							cv.put("visible", 1);

							try {
								db.insertOrThrow("Advice", null, cv);
							}
							catch (Exception exception) {
								// такой совет уже имеется в базе, получим его айдишник
								Cursor adviceCursor = db.query("Advice", new String[]{ "id" } , "url = ?", new String[]{ url }, null, null, null);

								if(adviceCursor.moveToFirst()) {
									adviceId = adviceCursor.getInt(0);
								}
								else {
									adviceId = -1;
								}

								adviceCursor.close();
							}

							if(adviceId != -1) {
								try {
									if (type == 0) {
										cv = new ContentValues();
										cv.put("purpose_id", id);
										cv.put("advice_id", adviceId);

										db.insertOrThrow("Purpose_Advice", null, cv);
									} else if (type == 1) {
										cv = new ContentValues();
										cv.put("task_id", id);
										cv.put("advice_id", adviceId);

										db.insertOrThrow("Task_Advice", null, cv);
									}

									if(adviceId > maxAdviceId) maxAdviceId = adviceId;

									addedAdvices.add(title + ": " + content);
								}
								catch (Exception exception) {
									// этот совет уже привязан к данному объекту, не паримся
								}
							}
						}
					}
				}
			}

			cursor.close();

			if (!addedAdvices.isEmpty()) {
				Resources resources = AdvicesService.this.getResources();

				Intent notificationIntent = new Intent(AdvicesService.this, MainActivity.class);
				PendingIntent contentIntent = PendingIntent.getActivity(AdvicesService.this, 0, notificationIntent, 0);

				NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(AdvicesService.this)
						.setSmallIcon(R.drawable.ic_notification)
						.setTicker(resources.getString(R.string.advices_notify_ticker))
						.setAutoCancel(true)
						.setWhen(System.currentTimeMillis())
						.setContentTitle(resources.getString(R.string.app_name))
						.setContentText(resources.getString(R.string.advices_founded) + " (" + addedAdvices.size() + ")")
						.setContentIntent(contentIntent);

				NotificationManager notificationManager = (NotificationManager) AdvicesService.this.getSystemService(Context.NOTIFICATION_SERVICE);
				notificationManager.notify(Notifications.NOTIFICATION_ADVICE, mBuilder.build());
			}

			long date = Calendar.getInstance().getTimeInMillis() + UserSettings.getAdvicesCheckPeriod() * 60 * 1000;
			AdvicesAlarm.setAlarm(AdvicesService.this, date);

			stopSelfResult(mStartId);
		}
	}
}

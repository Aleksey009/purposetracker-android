package ru.alex009.purpose.services;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;

import java.util.Calendar;

import ru.alex009.purpose.activities.ReminderActivity;
import ru.alex009.purpose.core.DataBaseConnector;
import ru.alex009.purpose.core.QueryBuilder;
import ru.alex009.purpose.receivers.RemindersAlarm;

/**
 * Created by AlekseyMikhailov on 22.11.14.
 */
public class RemindersService extends Service
{
	public static final int ACTION_INIT = 0;
	public static final int ACTION_ALARM = 1;

	public static Ringtone mRingtone;

	@Override
	public IBinder onBind(Intent intent)
	{
		throw new UnsupportedOperationException("Not use!");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		int action = intent.getIntExtra("action", ACTION_INIT);
		int id = intent.getIntExtra("id", -1);

		if (action == ACTION_ALARM) {
			Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
			if (mRingtone != null) {
				mRingtone.stop();
				mRingtone = null;
			}
			mRingtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
			mRingtone.setStreamType(AudioManager.STREAM_ALARM);
			mRingtone.play();

			Intent activityIntent = new Intent(this, ReminderActivity.class);
			activityIntent.putExtra("taskid", id);
			activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			startActivity(activityIntent);
		}

		SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();
		Cursor cursor = db.rawQuery(QueryBuilder.buildTaskQuery(false), null);

		int columnId = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_ID);
		int columnDate = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_DATE);
		int columnDays = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_REMINDER_DAYS);
		int columnTime = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_REMINDER_TIME);

		Calendar calendar = Calendar.getInstance();
		long currentDate = calendar.getTimeInMillis();
		int currentDay = calendar.get(Calendar.DAY_OF_WEEK) - 2; // первым идет ВС = 1...а надо ПН = 0
		if (currentDay == -1) currentDay = 6;
		int closestTaskId = -1;
		long closestDate = 0;

		while (cursor.moveToNext()) {
			int taskId = cursor.getInt(columnId);
			long date = cursor.getLong(columnDate);
			String daysString = cursor.getString(columnDays);
			int time = cursor.getInt(columnTime);
			int hour = time / 60;
			int minute = time % 60;

			if (date > currentDate) {
				if ((closestDate == 0) || (closestDate - currentDate) > (date - currentDate)) {
					closestDate = date;
					closestTaskId = taskId;
				}
			}

			if (daysString.charAt(currentDay) == '1') {
				calendar.set(Calendar.HOUR_OF_DAY, hour);
				calendar.set(Calendar.MINUTE, minute);

				date = calendar.getTimeInMillis();

				if (date > currentDate) {
					if ((closestDate == 0) || (closestDate - currentDate) > (date - currentDate)) {
						closestDate = date;
						closestTaskId = taskId;
					}
				}
			}
		}

		cursor.close();

		if (closestTaskId != -1) {
			RemindersAlarm.setAlarm(this, closestDate, closestTaskId);
		} else {
			RemindersAlarm.clearAlarm(this);
		}

		stopSelfResult(startId);

		return super.onStartCommand(intent, flags, startId);
	}
}

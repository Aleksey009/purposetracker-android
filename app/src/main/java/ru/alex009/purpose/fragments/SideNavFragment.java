package ru.alex009.purpose.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import ru.alex009.purpose.R;
import ru.alex009.purpose.core.MainApplication;
import ru.alex009.purpose.core.NetworkConnector;

/**
 * Created by AlekseyMikhailov on 16.11.14.
 */
public class SideNavFragment extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener, MainApplication.LoginListener
{
	public static final int ITEM_DIVIDER = -1;
	public static final int ITEM_PURPOSES = 0;
	public static final int ITEM_TASKS = 1;
	public static final int ITEM_TRACKS = 2;
	public static final int ITEM_SETTINGS = 3;
	public static final int ITEM_HELP = 4;
	private OnSideNavItemClickListener mListener;
	private SideNavAdapter mAdapter;
	private SignInButton mSignInButton;
	private ImageView mPersonCover;
	private ImageView mPersonPhoto;
	private TextView mPersonName;
	private TextView mPersonEmail;
	private ViewGroup mPersonContainer;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		mAdapter = new SideNavAdapter();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_side_nav, container, false);

		ListView listView = (ListView) view.findViewById(R.id.list);

		View header = inflater.inflate(R.layout.view_side_nav_header, listView, false);
		mSignInButton = (SignInButton) header.findViewById(R.id.signin_button);
		mPersonCover = (ImageView) header.findViewById(R.id.person_cover);
		mPersonPhoto = (ImageView) header.findViewById(R.id.person_photo);
		mPersonName = (TextView) header.findViewById(R.id.person_name);
		mPersonEmail = (TextView) header.findViewById(R.id.person_email);
		mPersonContainer = (ViewGroup) header.findViewById(R.id.person_container);

		mSignInButton.setOnClickListener(this);
		mPersonName.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Medium.ttf"));
		mPersonEmail.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Regular.ttf"));

		updateHeader();

		listView.addHeaderView(header);
		listView.setAdapter(mAdapter);
		listView.setOnItemClickListener(this);

		return view;
	}

	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);

		try {
			mListener = (OnSideNavItemClickListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnSideNavItemClickListener");
		}
	}

	@Override
	public void onDetach()
	{
		super.onDetach();

		mListener = null;
	}

	@Override
	public void onResume()
	{
		super.onResume();

        MainApplication.getInstance().setLoginListener(this);

		updateHeader();
	}

    @Override
    public void onPause() {
        super.onPause();

        MainApplication.getInstance().setLoginListener(null);
    }

    @Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
        if(position == 0) return;

		if (mListener != null) {
			mListener.onItemClick((int) mAdapter.getItemId(position - 1)); // -1 with header
		}
	}

	@Override
	public void onClick(View v)
	{
		if(v.getId() == R.id.signin_button) {
			MainApplication.getInstance().connectGoogleApiClient(getActivity());
		}
	}

	@Override
	public void onConnected(Bundle bundle)
	{
		updateHeader();
	}

	@Override
	public void onDisconnected()
	{
		updateHeader();
	}

	private void updateHeader()
	{
		GoogleApiClient googleApiClient = MainApplication.getInstance().getGoogleApiClient();
		if(googleApiClient.isConnected()) {
			Person person = Plus.PeopleApi.getCurrentPerson(googleApiClient);
			String account = Plus.AccountApi.getAccountName(googleApiClient);

			String cover = person.getCover().getCoverPhoto().getUrl();
			String photo = person.getImage().getUrl();

            // по умолчанию размер фото - 50, запросим большего размера
            photo = photo.substring(0, photo.length() - 2) + "128";

			mPersonName.setText(person.getDisplayName());
			mPersonEmail.setText(account);
			new ImageLoader(mPersonPhoto, true).execute(photo);
			new ImageLoader(mPersonCover).execute(cover);

			mSignInButton.setVisibility(View.GONE);
			mPersonPhoto.setVisibility(View.VISIBLE);
			mPersonContainer.setVisibility(View.VISIBLE);
		}
		else {
			mPersonCover.setImageResource(R.drawable.default_profile_cover);

			mSignInButton.setVisibility(View.VISIBLE);
			mPersonPhoto.setVisibility(View.GONE);
			mPersonContainer.setVisibility(View.GONE);
		}
	}

	public interface OnSideNavItemClickListener
	{
		public void onItemClick(int itemId);
	}

	private class SideNavAdapter extends BaseAdapter
	{
		private SideNavItem[] mItems;

		public SideNavAdapter()
		{
			super();

			mItems = new SideNavItem[]{
					new SideNavItem("Цели", R.drawable.ic_checkbox_marked_circle_outline_grey600_24dp, ITEM_PURPOSES),
					new SideNavItem("Задачи", R.drawable.ic_checkbox_marked_outline_grey600_24dp, ITEM_TASKS),
					new SideNavItem(ITEM_DIVIDER),
					new SideNavItem("Настройки", R.drawable.ic_settings_grey600_24dp, ITEM_SETTINGS),
					new SideNavItem("Помощь", R.drawable.ic_help_outline_grey600_24dp, ITEM_HELP)
			};
		}

		@Override
		public int getCount()
		{
			return mItems.length;
		}

		@Override
		public Object getItem(int position)
		{
			return mItems[position];
		}

		@Override
		public long getItemId(int position)
		{
			return mItems[position].itemId;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			SideNavItem item = mItems[position];

			if (item.itemId == ITEM_DIVIDER) {
				if (convertView == null || !(convertView instanceof FrameLayout)) {
					LayoutInflater layoutInflater = getActivity().getLayoutInflater();
					convertView = layoutInflater.inflate(R.layout.view_divider, parent, false);
				}
			} else {
				if (convertView == null || !(convertView instanceof TextView)) {
					LayoutInflater layoutInflater = getActivity().getLayoutInflater();
					convertView = layoutInflater.inflate(R.layout.view_side_nav_item, parent, false);

					ViewHolder holder = new ViewHolder();

					holder.imageView = (ImageView) convertView.findViewById(R.id.iconValue);
					holder.textView = (TextView) convertView.findViewById(R.id.textValue);

					holder.textView.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Regular.ttf"));

					convertView.setTag(holder);
				}

				ViewHolder holder = (ViewHolder) convertView.getTag();

				holder.imageView.setImageResource(item.iconId);
				holder.textView.setText(item.name);
			}

			return convertView;
		}

		@Override
		public boolean isEnabled(int position)
		{
			return (mItems[position].itemId != ITEM_DIVIDER);
		}

		private class SideNavItem
		{
			public String name;
			public int iconId;
			public int itemId;

			public SideNavItem(int itemId)
			{
				this.itemId = itemId;
			}

			public SideNavItem(String name, int iconId, int itemId)
			{
				this.name = name;
				this.iconId = iconId;
				this.itemId = itemId;
			}
		}

		private class ViewHolder
		{
			public ImageView imageView;
			public TextView textView;
		}
	}

	private class ImageLoader extends AsyncTask<String, Void, Bitmap>
	{
		private ImageView mTarget;
		private boolean mRounded;

		public ImageLoader(ImageView target)
		{
			mTarget = target;
		}

		public ImageLoader(ImageView target, boolean rounded)
		{
			mTarget = target;
			mRounded = rounded;
		}

		@Override
		protected Bitmap doInBackground(String... params)
		{
			Bitmap result = NetworkConnector.getBitmapFromURL(params[0]);

			if(mRounded) {
				Bitmap circleBitmap = Bitmap.createBitmap(result.getWidth(), result.getHeight(), Bitmap.Config.ARGB_8888);

				BitmapShader shader = new BitmapShader(result, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
				Paint paint = new Paint();
				paint.setAntiAlias(true);
                paint.setFilterBitmap(true);
                paint.setDither(true);
				paint.setShader(shader);

				Canvas c = new Canvas(circleBitmap);
				c.drawCircle(result.getWidth() / 2, result.getHeight() / 2, result.getWidth() / 2, paint);

				result = circleBitmap;
			}

			return result;
		}

		@Override
		protected void onPostExecute(Bitmap bitmap)
		{
			mTarget.setImageBitmap(bitmap);
		}
	}
}

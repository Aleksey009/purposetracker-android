package ru.alex009.purpose.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.getbase.floatingactionbutton.FloatingActionButton;

import ru.alex009.purpose.R;

/**
 * Created by AlekseyMikhailov on 16.11.14.
 */
public class GridFragment extends Fragment
{
	protected RecyclerView mRecyclerView;
	protected RecyclerView.LayoutManager mRecyclerLayoutManager;
	protected FloatingActionButton mFloatingActionButton;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		mRecyclerLayoutManager = new StaggeredGridLayoutManager(getResources().getInteger(R.integer.span_count),
				StaggeredGridLayoutManager.VERTICAL);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		View view = inflater.inflate(getLayoutId(), container, false);

		mRecyclerView = (RecyclerView) view.findViewById(R.id.grid_recycler);
		mRecyclerView.setLayoutManager(mRecyclerLayoutManager);

		mFloatingActionButton = (FloatingActionButton) view.findViewById(R.id.floating_action_button);

		return view;
	}

	public int getLayoutId()
	{
		return R.layout.fragment_grid;
	}
}

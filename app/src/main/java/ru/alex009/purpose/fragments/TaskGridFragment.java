package ru.alex009.purpose.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import ru.alex009.purpose.R;
import ru.alex009.purpose.activities.EditTaskActivity;
import ru.alex009.purpose.adapters.TaskGridAdapter;
import ru.alex009.purpose.core.UserSettings;

/**
 * Created by AlekseyMikhailov on 16.11.14.
 */
public class TaskGridFragment extends GridFragment implements View.OnClickListener
{
	protected TaskGridAdapter mRecylcerAdapter;
	protected int mPurposeId;

	public static TaskGridFragment newInstance(int purposeId)
	{
		TaskGridFragment fragment = new TaskGridFragment();
		Bundle args = new Bundle();
		args.putInt("purposeId", purposeId);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setHasOptionsMenu(true);

		Bundle args = getArguments();
		mPurposeId = args.getInt("purposeId", -1);
		if (mPurposeId == -1) {
			mRecylcerAdapter = new TaskGridAdapter(getActivity());
		} else {
			mRecylcerAdapter = new TaskGridAdapter(getActivity(), mPurposeId);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		View view = super.onCreateView(inflater, container, savedInstanceState);

		mRecyclerView.setAdapter(mRecylcerAdapter);
		mFloatingActionButton.setOnClickListener(this);

		return view;
	}

	@Override
	public void onResume()
	{
		super.onResume();

		if (getActivity() instanceof ActionBarActivity) {
			ActionBarActivity activity = (ActionBarActivity) getActivity();

			activity.getSupportActionBar().setTitle(R.string.tasks);
		}

		mRecylcerAdapter.reloadData();
	}


	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		super.onCreateOptionsMenu(menu, inflater);

		inflater.inflate(R.menu.task_grid, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId()) {
			case R.id.action_sort: {
				UserSettings.SortingParams params = UserSettings.getSortingParams();

				final boolean[] checked = {
						params.byProgress,
						params.byColor,
						params.byImportant,
						params.byDate
				};

				new AlertDialog
						.Builder(getActivity())
						.setCancelable(true)
						.setMultiChoiceItems(R.array.sortings, checked, new DialogInterface.OnMultiChoiceClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog, int which, boolean isChecked)
							{
								checked[which] = isChecked;
							}
						})
						.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog, int which)
							{
								UserSettings.SortingParams sortingParams = new UserSettings.SortingParams();

								sortingParams.byProgress = checked[0];
								sortingParams.byColor = checked[1];
								sortingParams.byImportant = checked[2];
								sortingParams.byDate = checked[3];

								UserSettings.setSortingParams(sortingParams);

								mRecylcerAdapter.updateSettings();
							}
						})
						.setNegativeButton(R.string.cancel, null)
						.show();

				break;
			}
			case R.id.action_filter: {
				new AlertDialog
						.Builder(getActivity())
						.setCancelable(true)
						.setSingleChoiceItems(R.array.filters, UserSettings.getFilter(), new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog, int which)
							{
								UserSettings.setFilter(which);

								mRecylcerAdapter.updateSettings();

								dialog.dismiss();
							}
						})
						.show();

				break;
			}
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View view)
	{
		if (view == mFloatingActionButton) {
			Intent intent = new Intent(getActivity(), EditTaskActivity.class);
			intent.putExtra("purposeId", mPurposeId);
			startActivity(intent);
		}
	}
}

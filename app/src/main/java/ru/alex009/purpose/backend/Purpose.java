package ru.alex009.purpose.backend;

import java.util.Date;

/**
 * Created by Администратор on 10.12.2014.
 */
public class Purpose
{
	public String ownerId;
	public Date created;
	public Date updated;
	public String objectId;

    public int id;
	public String text;
	public Date datetime;
	public int colorId;
	public int important;
	public int needAdvices;
}

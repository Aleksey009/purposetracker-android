package ru.alex009.purpose.backend;

import java.util.Date;

/**
 * Created by AlekseyMikhailov on 12.12.14.
 */
public class Task
{
	public String ownerId;
	public Date created;
	public Date updated;
	public String objectId;

	public int id;
    public int purposeId;
	public String text;
	public Date datetime;
	public int completed;
	public int colorId;
	public int important;
	public int needAdvices;
	public String reminderDays;
	public int reminderTime;
}

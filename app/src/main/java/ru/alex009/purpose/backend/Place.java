package ru.alex009.purpose.backend;

import java.util.Date;

/**
 * Created by AlekseyMikhailov on 12.12.14.
 */
public class Place
{
	public String ownerId;
	public Date created;
	public Date updated;
	public String objectId;

	public int id;
	public int task_id;
	public String description;
	public String coords;
}

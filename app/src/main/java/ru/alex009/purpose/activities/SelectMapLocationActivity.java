package ru.alex009.purpose.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import ru.alex009.purpose.R;

/**
 * Created by AlekseyMikhailov on 01.12.14.
 */
public class SelectMapLocationActivity extends ActionBarActivity implements GoogleMap.OnMapClickListener
{
	private GoogleMap mMap;
	private Marker mMarker;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_select_map_location);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

		if (toolbar != null) {
			setSupportActionBar(toolbar);
			getSupportActionBar().setHomeButtonEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}

		MapFragment map = (MapFragment) getFragmentManager().findFragmentById(R.id.map);

		if (map != null) {
			mMap = map.getMap();

			if (mMap != null) {
				mMap.setOnMapClickListener(this);
				mMap.setMyLocationEnabled(true);
				mMap.getUiSettings().setCompassEnabled(true);
				mMap.getUiSettings().setAllGesturesEnabled(true);
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.select_map_location, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId()) {
			case android.R.id.home: {
				setResult(RESULT_CANCELED, null);
				finish();
				return true;
			}
			case R.id.action_confirm: {
				if(mMarker == null) {
					setResult(RESULT_CANCELED, null);
				}
				else {
					Intent intent = new Intent();
					intent.putExtra("lat", mMarker.getPosition().latitude);
					intent.putExtra("lng", mMarker.getPosition().longitude);

					setResult(RESULT_OK, intent);
				}
				finish();
				return true;
			}
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onMapClick(LatLng latLng)
	{
		if (mMarker != null) {
			mMarker.remove();
		}

		MarkerOptions markerOptions = new MarkerOptions();
		markerOptions.position(latLng);
		markerOptions.draggable(true);

		mMarker = mMap.addMarker(markerOptions);
	}
}

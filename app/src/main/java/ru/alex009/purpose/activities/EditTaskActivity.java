package ru.alex009.purpose.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.maps.model.LatLng;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import ru.alex009.purpose.R;
import ru.alex009.purpose.adapters.ColorAdapter;
import ru.alex009.purpose.adapters.PriorityAdapter;
import ru.alex009.purpose.core.ContactViewBuilder;
import ru.alex009.purpose.core.DataBaseConnector;
import ru.alex009.purpose.core.DataBaseEvents;
import ru.alex009.purpose.core.PlaceViewBuilder;
import ru.alex009.purpose.core.QueryBuilder;

/**
 * Created by AlekseyMikhailov on 16.11.14.
 */
public class EditTaskActivity extends ActionBarActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener
{
	public static final int PICK_CONTACT_REQUEST = 0;
	public static final int PICK_LOCATION_REQUEST = 1;
	private static final String STATE_TEXT = "stateText";
	private static final String STATE_NEED_DATE = "stateNeedDate";
	private static final String STATE_DATE = "stateDate";
	private static final String STATE_TIME = "stateTime";
	private static final String STATE_NEED_ADVICES = "stateNeedAdvices";
	private static final String STATE_COLOR = "stateColor";
	private static final String STATE_IMPORTANT = "stateImportant";
	private static final String STATE_NEED_REMINDERS = "stateNeedReminders";
	private static final String STATE_DAY_BYTTON = "stateDayButton";
	private static final String STATE_REMINDER_TIME = "stateReminderTime";
	private static final String STATE_CONTACTS = "stateContacts";
	private static final String STATE_PLACES = "statePlaces";
	private final static int PICKER_TARGET_DATE_TIME = 1;
	private final static int PICKER_TARGET_REMINDER_TIME = 2;
	private Toolbar mToolbar;
	private int mPurposeId;
	private int mTaskId;
	private EditText mTextValue;
	private Switch mNeedDateValue;
	private ViewGroup mDateContainer;
	private TextView mDateValue;
	private TextView mTimeValue;
	private Switch mAdvicesValue;
	private ImageView mColorIcon;
	private TextView mColorTitle;
	private TextView mImportantTitle;
	private Switch mNeedRemindersValue;
	private ViewGroup mRemindersContainer;
	private ToggleButton[] mDayButtons;
	private TextView mRemindersTimeValue;
	private TextView mContactAddBtn;
	private ViewGroup mContactsContainer;
	private TextView mPlaceAddBtn;
	private ViewGroup mPlaceContainer;
	private int mPickerTarget;
	private ArrayList<Uri> mContactsList;
	private ArrayList<PlaceData> mPlacesList;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_task);

		mContactsList = new ArrayList<Uri>();
		mPlacesList = new ArrayList<PlaceData>();

		Intent intent = getIntent();
		mPurposeId = intent.getIntExtra("purposeId", -1);
		mTaskId = intent.getIntExtra("taskId", -1);

		mToolbar = (Toolbar) findViewById(R.id.toolbar);
		mTextValue = (EditText) findViewById(R.id.textValue);

		mNeedDateValue = (Switch) findViewById(R.id.needDateValue);
		mDateContainer = (ViewGroup) findViewById(R.id.dateContainer);
		mDateValue = (TextView) findViewById(R.id.dateValue);
		mTimeValue = (TextView) findViewById(R.id.timeValue);

		mAdvicesValue = (Switch) findViewById(R.id.advicesValue);

		mColorIcon = (ImageView) findViewById(R.id.colorIcon);
		mColorTitle = (TextView) findViewById(R.id.colorTitle);

		mImportantTitle = (TextView) findViewById(R.id.priorityTitle);

		mNeedRemindersValue = (Switch) findViewById(R.id.needRemindersValue);
		mRemindersContainer = (ViewGroup) findViewById(R.id.remindersContainer);
		ViewGroup remindersDaysContainer = (ViewGroup) findViewById(R.id.remindersDaysContainer);

		String[] daysOfWeek = {"ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ", "ВС"};
		mDayButtons = new ToggleButton[daysOfWeek.length];

		for (int i = 0; i < daysOfWeek.length; i++) {
			View view = getLayoutInflater().inflate(R.layout.day_button, remindersDaysContainer, false);
			mDayButtons[i] = (ToggleButton) view.findViewById(R.id.dayBtn);

			mDayButtons[i].setText(daysOfWeek[i]);
			mDayButtons[i].setTextOn(daysOfWeek[i]);
			mDayButtons[i].setTextOff(daysOfWeek[i]);

			remindersDaysContainer.addView(view);
		}

		mRemindersTimeValue = (TextView) findViewById(R.id.remindersTimeValue);

		mContactAddBtn = (TextView) findViewById(R.id.contactsAddBtn);
		mContactsContainer = (ViewGroup) findViewById(R.id.contactsContainer);

		mPlaceAddBtn = (TextView) findViewById(R.id.placesAddBtn);
		mPlaceContainer = (ViewGroup) findViewById(R.id.placesContainer);

		if (mToolbar != null) {
			setSupportActionBar(mToolbar);
			getSupportActionBar().setHomeButtonEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}

		mNeedDateValue.setOnCheckedChangeListener(this);
		mNeedRemindersValue.setOnCheckedChangeListener(this);

		mDateValue.setOnClickListener(this);
		mTimeValue.setOnClickListener(this);
		mRemindersTimeValue.setOnClickListener(this);
		mContactAddBtn.setOnClickListener(this);
		mPlaceAddBtn.setOnClickListener(this);
		mColorTitle.setOnClickListener(this);
		mImportantTitle.setOnClickListener(this);

		if (savedInstanceState == null) {
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);
			int hour = c.get(Calendar.HOUR_OF_DAY);
			int minute = c.get(Calendar.MINUTE);

			mDateValue.setText(day + "." + (month + 1) + "." + year);
			mTimeValue.setText(String.format("%2d:%02d", hour, minute));
			mRemindersTimeValue.setText(String.format("%2d:%02d", hour, minute));

			mColorTitle.setTag(Integer.valueOf(0));
			mImportantTitle.setTag(Integer.valueOf(PriorityAdapter.DEFAULT_VALUE));

			if (mTaskId != -1) {
				SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();
				Cursor cursor = db.rawQuery(QueryBuilder.buildSingleTaskQuery(mTaskId), null);

				if (cursor.getCount() == 0) {
					Log.e("DEV", this + " TASK NOT FOUND");
					return;
				}

				cursor.moveToFirst();

				int columnText = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_TEXT);
				int columnDate = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_DATE);
				int columnAdvices = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_NEED_ADVICES);
				int columnColor = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_COLOR_ID);
				int columnReminderDays = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_REMINDER_DAYS);
				int columnReminderTime = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_REMINDER_TIME);
				int columnPurpose = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_PURPOSE_ID);
				int columnImportant = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_IMPORTANT);

				int colorId = cursor.getInt(columnColor);
				int importantValue = cursor.getInt(columnImportant);

				mColorTitle.setTag(colorId);
				mImportantTitle.setTag(importantValue);

				mPurposeId = cursor.getInt(columnPurpose);
				mTextValue.setText(cursor.getString(columnText));
				mAdvicesValue.setChecked(cursor.getInt(columnAdvices) == 1);

				long date = cursor.getLong(columnDate);
				if (date != 0) {
					c.setTimeInMillis(date);
					year = c.get(Calendar.YEAR);
					month = c.get(Calendar.MONTH);
					day = c.get(Calendar.DAY_OF_MONTH);
					hour = c.get(Calendar.HOUR_OF_DAY);
					minute = c.get(Calendar.MINUTE);

					mDateValue.setText(day + "." + (month + 1) + "." + year);
					mTimeValue.setText(String.format("%2d:%02d", hour, minute));

					mNeedDateValue.setChecked(true);
				}
				else {
					mNeedDateValue.setChecked(false);
				}

				String days = cursor.getString(columnReminderDays);
				if (!days.equalsIgnoreCase("0000000")) {
					for (int i = 0; i < mDayButtons.length; i++) {
						mDayButtons[i].setChecked(days.charAt(i) == '1');
					}

					int time = cursor.getInt(columnReminderTime);
					mRemindersTimeValue.setText(String.format("%2d:%02d", (time / 60), (time % 60)));

					mNeedRemindersValue.setChecked(true);
				}
				else {
					mNeedRemindersValue.setChecked(false);
				}

				cursor.close();

				cursor = db.rawQuery(QueryBuilder.buildContactsQuery(mTaskId), null);

				int columnUri = cursor.getColumnIndex(QueryBuilder.FIELD_CONTACT_CONTACT_URI);

				while (cursor.moveToNext()) {
					Uri contactUri = Uri.parse(cursor.getString(columnUri));
					mContactsList.add(contactUri);
				}

				cursor.close();

				cursor = db.rawQuery(QueryBuilder.buildPlacesQuery(mTaskId), null);

				int columnDescription = cursor.getColumnIndex(QueryBuilder.FIELD_PLACE_DESCRIPTION);
				int columnCoords = cursor.getColumnIndex(QueryBuilder.FIELD_PLACE_COORDS);

				while (cursor.moveToNext()) {
					String desc = cursor.getString(columnDescription);
					String coordsString = cursor.getString(columnCoords);

					String[] coords = coordsString.split(":");
					PlaceData data = new PlaceData(desc, Double.valueOf(coords[0]), Double.valueOf(coords[1]));

					mPlacesList.add(data);
				}

				cursor.close();
			}
		}
		else {
			mTextValue.setText(savedInstanceState.getString(STATE_TEXT));
			mNeedDateValue.setChecked(savedInstanceState.getBoolean(STATE_NEED_DATE));
			mDateValue.setText(savedInstanceState.getString(STATE_DATE));
			mTimeValue.setText(savedInstanceState.getString(STATE_TIME));
			mAdvicesValue.setChecked(savedInstanceState.getBoolean(STATE_NEED_ADVICES));
			mColorTitle.setTag(savedInstanceState.getInt(STATE_COLOR));
			mImportantTitle.setTag(savedInstanceState.getInt(STATE_IMPORTANT));

			for (int i = 0; i < mDayButtons.length; i++) {
				mDayButtons[i].setChecked(savedInstanceState.getBoolean(STATE_DAY_BYTTON + i));
			}

			mRemindersTimeValue.setText(savedInstanceState.getString(STATE_REMINDER_TIME));
			mContactsList = savedInstanceState.getParcelableArrayList(STATE_CONTACTS);
			mPlacesList = savedInstanceState.getParcelableArrayList(STATE_PLACES);
		}

		for (Uri contactUri : mContactsList) {
			View contact = ContactViewBuilder.init(this, mContactsContainer)
					.setContactUri(contactUri)
					.setDeleteClickListener(this)
					.build();

			contact.setTag(R.id.data, contactUri);

			mContactsContainer.addView(contact);
		}

		for (PlaceData data : mPlacesList) {
			View place = PlaceViewBuilder.init(this, mPlaceContainer)
					.setPlaceInfo(data.name, data.position)
					.setDeleteClickListener(this)
					.build();

			place.setTag(R.id.data, data);
			mPlaceContainer.addView(place);
		}

		int importantValue = (Integer) mImportantTitle.getTag();
		mImportantTitle.setText(new PriorityAdapter(this).getItem(importantValue));
		updateColorIcon();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		outState.putString(STATE_TEXT, mTextValue.getText().toString());
		outState.putBoolean(STATE_NEED_DATE, mNeedDateValue.isChecked());
		outState.putString(STATE_DATE, mDateValue.getText().toString());
		outState.putString(STATE_TIME, mTimeValue.getText().toString());
		outState.putBoolean(STATE_NEED_ADVICES, mAdvicesValue.isChecked());
		outState.putInt(STATE_COLOR, (Integer) mColorTitle.getTag());
		outState.putInt(STATE_IMPORTANT, (Integer) mImportantTitle.getTag());
		outState.putBoolean(STATE_NEED_REMINDERS, mNeedRemindersValue.isChecked());

		for (int i = 0; i < mDayButtons.length; i++) {
			outState.putBoolean(STATE_DAY_BYTTON + i, mDayButtons[i].isChecked());
		}

		outState.putString(STATE_REMINDER_TIME, mRemindersTimeValue.getText().toString());
		outState.putParcelableArrayList(STATE_CONTACTS, mContactsList);
		outState.putParcelableArrayList(STATE_PLACES, mPlacesList);

		super.onSaveInstanceState(outState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.edit_task, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId()) {
			case android.R.id.home: {
				finish();
				return true;
			}
			case R.id.action_confirm: {
				SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();

				int taskId = mTaskId;
				if (taskId == -1) {
					Cursor cursor = db.rawQuery(QueryBuilder.buildMaxTaskIdQuery(), null);

					cursor.moveToFirst();

					taskId = cursor.getInt(0) + 1;

					cursor.close();
				}

				SimpleDateFormat sdf = new SimpleDateFormat("d.M.yyyy H:m");

				String text = mTextValue.getText().toString();
				long date = 0;
				if (mNeedDateValue.isChecked()) {
					try {
						date = sdf.parse(mDateValue.getText() + " " + mTimeValue.getText()).getTime();
					}
					catch (ParseException e) {
						e.printStackTrace();
					}
				}
				int needAdvices = (mAdvicesValue.isChecked() ? 1 : 0);
				String remindersDays = "0000000";
				int remindersTime = 0;
				if (mNeedRemindersValue.isChecked()) {
					remindersDays = "";
					for (int i = 0; i < mDayButtons.length; i++) {
						if (mDayButtons[i].isChecked()) remindersDays += "1";
						else remindersDays += "0";
					}

					String[] time = mRemindersTimeValue.getText().toString().split("\\:");
					remindersTime = Integer.valueOf(time[0]) * 60 + Integer.valueOf(time[1]);
				}
				int color = (Integer) mColorTitle.getTag();
				int important = (Integer) mImportantTitle.getTag();

				ContentValues cv = new ContentValues();
				cv.put(QueryBuilder.FIELD_TASK_PURPOSE_ID, mPurposeId);
				cv.put(QueryBuilder.FIELD_TASK_TEXT, text);
				cv.put(QueryBuilder.FIELD_TASK_DATE, date);
				cv.put(QueryBuilder.FIELD_TASK_NEED_ADVICES, needAdvices);
				cv.put(QueryBuilder.FIELD_TASK_COLOR_ID, color);
				cv.put(QueryBuilder.FIELD_TASK_COMPLETED, 0);
				cv.put(QueryBuilder.FIELD_TASK_REMINDER_DAYS, remindersDays);
				cv.put(QueryBuilder.FIELD_TASK_REMINDER_TIME, remindersTime);
				cv.put(QueryBuilder.FIELD_TASK_IMPORTANT, important);

				if (mTaskId == -1) {
					cv.put(QueryBuilder.FIELD_TASK_ID, taskId);
					db.insert("Task", null, cv);

					DataBaseEvents.onRowCreated(DataBaseEvents.TABLE_TASK_ID, taskId, cv);

					Toast.makeText(this, R.string.task_added, Toast.LENGTH_LONG).show();
				}
				else {
					db.update("Task", cv, QueryBuilder.FIELD_TASK_ID + " = ?", new String[]{String.valueOf(mTaskId)});

					DataBaseEvents.onRowEdited(DataBaseEvents.TABLE_TASK_ID, mTaskId, cv);

					Toast.makeText(this, R.string.task_changed, Toast.LENGTH_LONG).show();
				}

				db.delete("Contact", "task_id = ?", new String[]{String.valueOf(taskId)});

				for (Uri contact : mContactsList) {
					ContentValues ccv = new ContentValues();
					ccv.put(QueryBuilder.FIELD_CONTACT_TASK_ID, taskId);
					ccv.put(QueryBuilder.FIELD_CONTACT_CONTACT_URI, contact.toString());

					db.insert("Contact", null, ccv);
				}

				db.delete("Place", "task_id = ?", new String[]{String.valueOf(taskId)});

				for (PlaceData place : mPlacesList) {
					ContentValues ccv = new ContentValues();
					ccv.put(QueryBuilder.FIELD_PLACE_TASK_ID, taskId);
					ccv.put(QueryBuilder.FIELD_PLACE_DESCRIPTION, place.name);
					ccv.put(QueryBuilder.FIELD_PLACE_COORDS, place.position.latitude + ":" + place.position.longitude);

					db.insert("Place", null, ccv);
				}

				finish();
				return true;
			}
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View view)
	{
		switch (view.getId()) {
			case R.id.dateValue: {
				final Calendar c = Calendar.getInstance();
				int year = c.get(Calendar.YEAR);
				int month = c.get(Calendar.MONTH);
				int day = c.get(Calendar.DAY_OF_MONTH);

				new DatePickerDialog(this, this, year, month, day).show();
				break;
			}
			case R.id.timeValue: {
				final Calendar c = Calendar.getInstance();
				int hour = c.get(Calendar.HOUR_OF_DAY);
				int minute = c.get(Calendar.MINUTE);

				new TimePickerDialog(this, this, hour, minute, true).show();
				mPickerTarget = PICKER_TARGET_DATE_TIME;
				break;
			}
			case R.id.colorTitle: {
				final AlertDialog dialog = new AlertDialog.Builder(this)
						.setTitle(R.string.color)
						.create();

				GridView gridView = new GridView(this);
				gridView.setNumColumns(4);
				gridView.setAdapter(new ColorAdapter(this));
				gridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
				{
					@Override
					public void onItemClick(AdapterView<?> adapterView, View view, int position, long id)
					{
						mColorTitle.setTag((int) id);
						updateColorIcon();
						dialog.dismiss();
					}
				});

				dialog.setView(gridView);
				dialog.show();
				break;
			}
			case R.id.priorityTitle: {
				new AlertDialog.Builder(this)
						.setTitle(R.string.priority)
						.setAdapter(new PriorityAdapter(this), new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialogInterface, int i)
							{
								mImportantTitle.setTag(i);
								mImportantTitle.setText(new PriorityAdapter(EditTaskActivity.this).getItem(i));
							}
						})
						.show();
				break;
			}
			case R.id.remindersTimeValue: {
				final Calendar c = Calendar.getInstance();
				int hour = c.get(Calendar.HOUR_OF_DAY);
				int minute = c.get(Calendar.MINUTE);

				new TimePickerDialog(this, this, hour, minute, true).show();
				mPickerTarget = PICKER_TARGET_REMINDER_TIME;
				break;
			}
			case R.id.contactsAddBtn: {
				Intent pickContactIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
				startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
				break;
			}
			case R.id.placesAddBtn: {
				Intent intent = new Intent(this, SelectMapLocationActivity.class);
				startActivityForResult(intent, PICK_LOCATION_REQUEST);
				break;
			}
			case R.id.contact_delete: {
				View mainView = (View) view.getTag();
				Uri contact = (Uri) mainView.getTag(R.id.data);

				mContactsList.remove(contact);
				mContactsContainer.removeView(mainView);
				break;
			}
			case R.id.place_delete: {
				View mainView = (View) view.getTag();
				PlaceData data = (PlaceData) mainView.getTag(R.id.data);

				mPlacesList.remove(data);
				mPlaceContainer.removeView(mainView);
				break;
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent)
	{
		super.onActivityResult(requestCode, resultCode, intent);

		if (resultCode != RESULT_OK) return;

		switch (requestCode) {
			case PICK_CONTACT_REQUEST: {
				Uri pickedContact = intent.getData();

				View contact = ContactViewBuilder.init(this, mContactsContainer)
						.setContactUri(pickedContact)
						.setDeleteClickListener(this)
						.build();

				contact.setTag(R.id.data, pickedContact);

				mContactsList.add(pickedContact);
				mContactsContainer.addView(contact);

				break;
			}
			case PICK_LOCATION_REQUEST: {
				double lat = intent.getDoubleExtra("lat", 0.0);
				double lng = intent.getDoubleExtra("lng", 0.0);

				final LatLng pos = new LatLng(lat, lng);
				final EditText input = new EditText(this);

				input.setInputType(InputType.TYPE_CLASS_TEXT);

				new AlertDialog.Builder(this)
						.setTitle(R.string.input_place_name)
						.setView(input)
						.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog, int which)
							{
								PlaceData data = new PlaceData(input.getText().toString(), pos.latitude, pos.longitude);

								View place = PlaceViewBuilder.init(EditTaskActivity.this, mPlaceContainer)
										.setPlaceInfo(data.name, data.position)
										.setDeleteClickListener(EditTaskActivity.this)
										.build();

								place.setTag(R.id.data, data);

								mPlacesList.add(data);
								mPlaceContainer.addView(place);
							}
						})
						.setNegativeButton(android.R.string.cancel, null)
						.show();

				break;
			}
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
	{
		if (buttonView == mNeedDateValue) {
			if (isChecked) {
				mDateContainer.setVisibility(View.VISIBLE);
			}
			else {
				mDateContainer.setVisibility(View.GONE);
			}
		}
		if (buttonView == mNeedRemindersValue) {
			if (isChecked) {
				mRemindersContainer.setVisibility(View.VISIBLE);
			}
			else {
				mRemindersContainer.setVisibility(View.GONE);
			}
		}
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
	{
		mDateValue.setText(dayOfMonth + "." + (monthOfYear + 1) + "." + year);

		checkDateTimeValid();
	}

	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute)
	{
		if (mPickerTarget == PICKER_TARGET_DATE_TIME) {
			mTimeValue.setText(String.format("%2d:%02d", hourOfDay, minute));

			checkDateTimeValid();
		}
		else if (mPickerTarget == PICKER_TARGET_REMINDER_TIME) {
			mRemindersTimeValue.setText(String.format("%2d:%02d", hourOfDay, minute));
		}
	}

	private void checkDateTimeValid()
	{
		String dateTimeString = mDateValue.getText() + " " + mTimeValue.getText();
		SimpleDateFormat sdf = new SimpleDateFormat("d.M.yyyy H:m");

		try {
			Date dateTime = sdf.parse(dateTimeString);

			if (Calendar.getInstance().getTimeInMillis() > dateTime.getTime()) {
				Toast.makeText(this, R.string.date_time_invalid, Toast.LENGTH_SHORT).show();
			}
		}
		catch (ParseException e) {
			e.printStackTrace();
		}
	}

	private void updateColorIcon()
	{
		int colorId = (Integer) mColorTitle.getTag();

		SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();
		Cursor cursor = db.rawQuery(QueryBuilder.buildSingleColorQuery(colorId), null);
		if (cursor.moveToNext()) {
			long color = cursor.getLong(cursor.getColumnIndex(QueryBuilder.FIELD_COLOR_COLOR));
			mColorIcon.setColorFilter((int) color, PorterDuff.Mode.MULTIPLY);
		}
		cursor.close();
	}

	private static class PlaceData implements Parcelable
	{
		public static final Parcelable.Creator<PlaceData> CREATOR = new Parcelable.Creator<PlaceData>()
		{
			public PlaceData createFromParcel(Parcel in)
			{
				return new PlaceData(in);
			}

			public PlaceData[] newArray(int size)
			{
				return new PlaceData[size];
			}
		};
		public String name;
		public LatLng position;

		public PlaceData(String _name, double lat, double lng)
		{
			name = _name;
			position = new LatLng(lat, lng);
		}

		private PlaceData(Parcel in)
		{
			name = in.readString();
			double lat = in.readDouble();
			double lng = in.readDouble();

			position = new LatLng(lat, lng);
		}

		@Override
		public int describeContents()
		{
			return 0;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags)
		{
			dest.writeString(name);
			dest.writeDouble(position.latitude);
			dest.writeDouble(position.longitude);
		}
	}
}

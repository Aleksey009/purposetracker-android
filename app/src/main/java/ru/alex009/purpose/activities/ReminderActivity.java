package ru.alex009.purpose.activities;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import ru.alex009.purpose.R;
import ru.alex009.purpose.core.DataBaseConnector;
import ru.alex009.purpose.core.DataBaseEvents;
import ru.alex009.purpose.core.QueryBuilder;
import ru.alex009.purpose.services.RemindersService;

public class ReminderActivity extends Activity implements View.OnClickListener
{
	private int mTaskId;
	private int mPurposeId;
	private String mTaskText;
	private TextView mReminderText;
	private Button mCancelBtn;
	private Button mCompleteBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reminder);

		Intent intent = getIntent();
		int taskId = intent.getIntExtra("taskid", -1);

		if (taskId == -1) {
			Log.e("DEV", "not assigned task");
			finish();
		}

		SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();
		Cursor cursor = db.rawQuery(QueryBuilder.buildSingleTaskQuery(taskId), null);

		int columnId = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_ID);
		int columnPurposeId = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_PURPOSE_ID);
		int columnText = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_TEXT);

		if (cursor.getCount() == 0) {
			Log.e("DEV", "task not founded");
			finish();
		}

		cursor.moveToFirst();

		mTaskId = cursor.getInt(columnId);
		mPurposeId = cursor.getInt(columnPurposeId);
		mTaskText = cursor.getString(columnText);

		mReminderText = (TextView) findViewById(R.id.reminderTextValue);
		mCancelBtn = (Button) findViewById(R.id.cancel_btn);
		mCompleteBtn = (Button) findViewById(R.id.complete_btn);

		mReminderText.setText(mTaskText);
		mCancelBtn.setOnClickListener(this);
		mCompleteBtn.setOnClickListener(this);
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
	}

	@Override
	public void onClick(View view)
	{
		if (mCancelBtn == view) {
			Toast.makeText(this, R.string.reminder_delayed, Toast.LENGTH_LONG).show();
			RemindersService.mRingtone.stop();
			finish();
		}
		if (mCompleteBtn == view) {
			SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();
			ContentValues cv = new ContentValues();
			cv.put(QueryBuilder.FIELD_TASK_COMPLETED, 1);
			db.update("Task", cv, "id = ?", new String[]{String.valueOf(mTaskId)});

			cv.put(QueryBuilder.FIELD_TASK_PURPOSE_ID, mPurposeId);

			DataBaseEvents.onRowEdited(DataBaseEvents.TABLE_TASK_ID, mTaskId, cv);

			Toast.makeText(this, R.string.reminder_completed, Toast.LENGTH_LONG).show();
			RemindersService.mRingtone.stop();
			finish();
		}
	}
}

package ru.alex009.purpose.activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.philjay.circledisplay.CircleDisplay;

import java.text.SimpleDateFormat;
import java.util.Date;

import ru.alex009.purpose.R;
import ru.alex009.purpose.adapters.PriorityAdapter;
import ru.alex009.purpose.core.AdviceViewBuilder;
import ru.alex009.purpose.core.DataBaseConnector;
import ru.alex009.purpose.core.QueryBuilder;

/**
 * Created by AlekseyMikhailov on 26.11.14.
 */
public class PurposeActivity extends ActionBarActivity implements View.OnClickListener
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_purpose);

		ViewGroup colorView = (ViewGroup) findViewById(R.id.colorView);
		CircleDisplay progressValueView = (CircleDisplay) findViewById(R.id.progressValue);
		TextView progressTextView = (TextView) findViewById(R.id.progressText);
		TextView textView = (TextView) findViewById(R.id.textValue);
		ViewGroup dateContainer = (ViewGroup) findViewById(R.id.dateContainer);
		TextView dateValueView = (TextView) findViewById(R.id.dateValue);
		TextView priorityTitleView = (TextView) findViewById(R.id.priorityTitle);
		ViewGroup advicesContainer = (ViewGroup) findViewById(R.id.advicesContainer);
		TextView advicesTitleView = (TextView) findViewById(R.id.advicesTitle);
		ViewGroup advicesListContainer = (ViewGroup) findViewById(R.id.advicesListContainer);
		ImageButton advicesButton = (ImageButton) findViewById(R.id.advicesButton);

		textView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf"));
		progressTextView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf"));
		dateValueView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf"));
		advicesTitleView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf"));
		priorityTitleView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf"));

		progressValueView.setDrawText(false);
		progressValueView.setTouchEnabled(false);

		advicesButton.setOnClickListener(this);

		Intent intent = getIntent();
		int purposeId = intent.getIntExtra("purposeId", -1);

		if (purposeId == -1) {
			Log.e("DEV", this + " incorrect purpose id!");
			finish();
		}

		SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();
		Cursor cursor = db.rawQuery(QueryBuilder.buildPurposeInfoQuery(purposeId), null);

		int columnText = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSELIST_TEXT);
		int columnProgress = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSELIST_PROGRESS);
		int columnDate = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSELIST_DATE);
		int columnNeedAdvice = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSELIST_NEED_ADVICES);
		int columnAdvicesCount = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSELIST_ADVICES_COUNT);
		int columnColor = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSELIST_COLOR);
		int columnImportant = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSELIST_IMPORTANT);

		cursor.moveToFirst();

		int color = cursor.getInt(columnColor);
		colorView.setBackgroundColor(color);

		String text = cursor.getString(columnText);
		textView.setText(text);

		float progress = cursor.getFloat(columnProgress);
		progressValueView.showValue(progress, 100.0f, false);
		progressTextView.setText(String.format("%s %.0f%%", getResources().getString(R.string.progress_text), progress));

		long date = cursor.getLong(columnDate);
		if (date == 0) {
			dateContainer.setVisibility(View.GONE);
		} else {
			dateContainer.setVisibility(View.VISIBLE);
			SimpleDateFormat sdf = new SimpleDateFormat("dd.M.yyyy HH:mm");

			dateValueView.setText(sdf.format(new Date(date)));
		}

		priorityTitleView.setText(new PriorityAdapter(this).getItem(cursor.getInt(columnImportant)));

		int needAdvice = cursor.getInt(columnNeedAdvice);
		int advicesCount = cursor.getInt(columnAdvicesCount);

		cursor.close();

		if (needAdvice == 1) {
			advicesContainer.setVisibility(View.VISIBLE);

			if (advicesCount > 0) {
				advicesButton.setVisibility(View.VISIBLE);

				cursor = db.rawQuery(QueryBuilder.buildPurposeAdvicesQuery(purposeId, true), null);

				int columnTitle = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSEADVICES_TITLE);
				int columnDescription = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSEADVICES_DESCRIPTION);
				int columnUrl = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSEADVICES_URL);

				while (cursor.moveToNext()) {
					String title = cursor.getString(columnTitle);
					String desc = cursor.getString(columnDescription);
					String url = cursor.getString(columnUrl);

					View adviceView = AdviceViewBuilder
							.init(this, advicesContainer)
							.setTitle(title)
							.setDescription(desc)
							.build();

					adviceView.setTag(url);
					adviceView.setOnClickListener(this);

					advicesListContainer.addView(adviceView);
				}

				cursor.close();
			}
			else {
				advicesButton.setVisibility(View.GONE);
			}
		}
		else {
			advicesContainer.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v)
	{
		if(v.getId() == R.id.advicesButton) {
			ImageButton button = (ImageButton) v;
			ViewGroup advicesListContainer = (ViewGroup) findViewById(R.id.advicesListContainer);

			if(advicesListContainer.getVisibility() == View.VISIBLE) {
				advicesListContainer.setVisibility(View.GONE);
				button.setImageResource(R.drawable.ic_chevron_down_grey600_24dp);
			}
			else {
				advicesListContainer.setVisibility(View.VISIBLE);
				button.setImageResource(R.drawable.ic_chevron_up_grey600_24dp);
			}
		}
		else {
			String url = (String) v.getTag();
			if (url == null) return;

			Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			startActivity(myIntent);
		}
	}
}

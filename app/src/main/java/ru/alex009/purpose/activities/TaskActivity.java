package ru.alex009.purpose.activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.android.gms.maps.model.LatLng;
import com.philjay.circledisplay.CircleDisplay;

import java.text.SimpleDateFormat;
import java.util.Date;

import ru.alex009.purpose.R;
import ru.alex009.purpose.adapters.PriorityAdapter;
import ru.alex009.purpose.core.AdviceViewBuilder;
import ru.alex009.purpose.core.ContactViewBuilder;
import ru.alex009.purpose.core.DataBaseConnector;
import ru.alex009.purpose.core.PlaceViewBuilder;
import ru.alex009.purpose.core.QueryBuilder;

/**
 * Created by AlekseyMikhailov on 26.11.14.
 */
public class TaskActivity extends ActionBarActivity implements View.OnClickListener
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_task);

		ViewGroup colorView = (ViewGroup) findViewById(R.id.colorView);
		ImageView completedIcon = (ImageView) findViewById(R.id.completedIcon);
		TextView textView = (TextView) findViewById(R.id.textValue);
		ViewGroup dateContainer = (ViewGroup) findViewById(R.id.dateContainer);
		TextView dateValueView = (TextView) findViewById(R.id.dateValue);
		ViewGroup remindersContainer = (ViewGroup) findViewById(R.id.remindersContainer);
		ViewGroup remindersDaysContainer = (ViewGroup) findViewById(R.id.remindersDaysContainer);
		TextView remindersTimeTitle = (TextView) findViewById(R.id.remindersTimeTitle);
		TextView remindersTimeValue = (TextView) findViewById(R.id.remindersTimeValue);
		ViewGroup contactsContainer = (ViewGroup) findViewById(R.id.contactsContainer);
		ViewGroup contactsListContainer = (ViewGroup) findViewById(R.id.contactsListContainer);
		ViewGroup placesContainer = (ViewGroup) findViewById(R.id.placesContainer);
		ViewGroup placesListContainer = (ViewGroup) findViewById(R.id.placesListContainer);
		TextView priorityTitleView = (TextView) findViewById(R.id.priorityTitle);
		ViewGroup advicesContainer = (ViewGroup) findViewById(R.id.advicesContainer);
		TextView advicesTitleView = (TextView) findViewById(R.id.advicesTitle);
		ViewGroup advicesListContainer = (ViewGroup) findViewById(R.id.advicesListContainer);
		ImageButton advicesButton = (ImageButton) findViewById(R.id.advicesButton);

		textView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf"));
		remindersTimeTitle.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf"));
		remindersTimeValue.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf"));
		dateValueView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf"));
		advicesTitleView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf"));
		priorityTitleView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf"));

		advicesButton.setOnClickListener(this);

		Intent intent = getIntent();
		int taskId = intent.getIntExtra("taskId", -1);

		if (taskId == -1) {
			Log.e("DEV", this + " incorrect task id!");
			finish();
		}

		SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();
		Cursor cursor = db.rawQuery(QueryBuilder.buildTaskInfoQuery(taskId), null);

		int columnText = cursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_TEXT);
		int columnDate = cursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_DATE);
		int columnNeedAdvice = cursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_NEED_ADVICES);
		int columnAdvicesCount = cursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_ADVICES_COUNT);
		int columnColor = cursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_COLOR);
		int columnCompleted = cursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_COMPLETED);
		int columnReminderDays = cursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_REMINDER_DAYS);
		int columnReminderTime = cursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_REMINDER_TIME);
		int columnImportant = cursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_IMPORTANT);

		cursor.moveToFirst();

		int color = cursor.getInt(columnColor);
		colorView.setBackgroundColor(color);

		String text = cursor.getString(columnText);
		textView.setText(text);

		long date = cursor.getLong(columnDate);
		if (date == 0) {
			dateContainer.setVisibility(View.GONE);
		} else {
			dateContainer.setVisibility(View.VISIBLE);
			SimpleDateFormat sdf = new SimpleDateFormat("dd.M.yyyy HH:mm");

			dateValueView.setText(sdf.format(new Date(date)));
		}

		priorityTitleView.setText(new PriorityAdapter(this).getItem(cursor.getInt(columnImportant)));

		int completed = cursor.getInt(columnCompleted);
		completedIcon.setImageResource((completed == 1 ? R.drawable.ic_checkbox_marked_circle_grey600_24dp : R.drawable.ic_checkbox_blank_circle_outline_grey600_24dp));

		String reminderDays = cursor.getString(columnReminderDays);
		if (reminderDays.equalsIgnoreCase("0000000")) {
			remindersContainer.setVisibility(View.GONE);
		} else {
			remindersContainer.setVisibility(View.VISIBLE);

			String[] daysOfWeek = {"ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ", "ВС"};
			ToggleButton [] dayButtons = new ToggleButton[daysOfWeek.length];

			for (int i = 0; i < daysOfWeek.length; i++) {
				View view = getLayoutInflater().inflate(R.layout.day_button, remindersDaysContainer, false);
				dayButtons[i] = (ToggleButton) view.findViewById(R.id.dayBtn);

				dayButtons[i].setText(daysOfWeek[i]);
				dayButtons[i].setTextOn(daysOfWeek[i]);
				dayButtons[i].setTextOff(daysOfWeek[i]);
				dayButtons[i].setEnabled(false);
				dayButtons[i].setChecked(reminderDays.charAt(i) == '1');

				remindersDaysContainer.addView(view);
			}

			int time = cursor.getInt(columnReminderTime);

			remindersTimeValue.setText(String.format("%2d:%02d", (time / 60), (time % 60)));
		}

		int needAdvice = cursor.getInt(columnNeedAdvice);
		int advicesCount = cursor.getInt(columnAdvicesCount);

		cursor.close();


		if (needAdvice == 1) {
			advicesContainer.setVisibility(View.VISIBLE);
			advicesListContainer.setVisibility(View.GONE);

			if (advicesCount > 0) {
				advicesButton.setVisibility(View.VISIBLE);

				cursor = db.rawQuery(QueryBuilder.buildTaskAdvicesQuery(taskId, true), null);

				int columnTitle = cursor.getColumnIndex(QueryBuilder.FIELD_TASKADVICES_TITLE);
				int columnDescription = cursor.getColumnIndex(QueryBuilder.FIELD_TASKADVICES_DESCRIPTION);
				int columnUrl = cursor.getColumnIndex(QueryBuilder.FIELD_TASKADVICES_URL);

				while (cursor.moveToNext()) {
					String title = cursor.getString(columnTitle);
					String desc = cursor.getString(columnDescription);
					String url = cursor.getString(columnUrl);

					View adviceView = AdviceViewBuilder
							.init(this, advicesListContainer)
							.setTitle(title)
							.setDescription(desc)
							.build();

					adviceView.setTag(url);
					adviceView.setOnClickListener(this);

					advicesListContainer.addView(adviceView);
				}

				cursor.close();
			}
			else {
				advicesButton.setVisibility(View.GONE);
			}
		} else {
			advicesContainer.setVisibility(View.GONE);
		}


		cursor = db.rawQuery(QueryBuilder.buildContactsQuery(taskId), null);

		int columnUri = cursor.getColumnIndex(QueryBuilder.FIELD_CONTACT_CONTACT_URI);

		contactsContainer.setVisibility(cursor.getCount() != 0 ? View.VISIBLE : View.GONE);

		while (cursor.moveToNext()) {
			Uri contact = Uri.parse(cursor.getString(columnUri));

			View contactView = ContactViewBuilder.init(this, contactsListContainer)
					.setContactUri(contact)
					.build();

			contactsListContainer.addView(contactView);
		}

		cursor.close();


		cursor = db.rawQuery(QueryBuilder.buildPlacesQuery(taskId), null);

		int columnDescription = cursor.getColumnIndex(QueryBuilder.FIELD_PLACE_DESCRIPTION);
		int columnCoords = cursor.getColumnIndex(QueryBuilder.FIELD_PLACE_COORDS);

		placesContainer.setVisibility(cursor.getCount() != 0 ? View.VISIBLE : View.GONE);

		while (cursor.moveToNext()) {
			String desc = cursor.getString(columnDescription);
			String coordsString = cursor.getString(columnCoords);

			String[] coords = coordsString.split(":");
			LatLng pos = new LatLng(Double.valueOf(coords[0]), Double.valueOf(coords[1]));

			View place = PlaceViewBuilder.init(this, placesListContainer)
					.setPlaceInfo(desc, pos)
					.build();

			placesListContainer.addView(place);
		}

		cursor.close();
	}

	@Override
	public void onClick(View v)
	{
		if(v.getId() == R.id.advicesButton) {
			ImageButton button = (ImageButton) v;
			ViewGroup advicesListContainer = (ViewGroup) findViewById(R.id.advicesListContainer);

			if(advicesListContainer.getVisibility() == View.VISIBLE) {
				advicesListContainer.setVisibility(View.GONE);
				button.setImageResource(R.drawable.ic_chevron_down_grey600_24dp);
			}
			else {
				advicesListContainer.setVisibility(View.VISIBLE);
				button.setImageResource(R.drawable.ic_chevron_up_grey600_24dp);
			}
		}
		else {
			String url = (String) v.getTag();
			if (url == null) return;

			Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			startActivity(myIntent);
		}
	}
}

package ru.alex009.purpose.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import ru.alex009.purpose.R;
import ru.alex009.purpose.fragments.TaskGridFragment;

/**
 * Created by AlekseyMikhailov on 20.11.14.
 */
public class PurposeTasksActivity extends ActionBarActivity
{
	private Toolbar mToolbar;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_purpose_tasks);

		mToolbar = (Toolbar) findViewById(R.id.toolbar);

		if (mToolbar != null) {
			setSupportActionBar(mToolbar);
			getSupportActionBar().setHomeButtonEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}

		if (savedInstanceState == null) {
			Intent intent = getIntent();
			int purpose_id = intent.getIntExtra("purposeId", -1);
			if (purpose_id == -1) {
				Log.d("DEV", "what a fuck is it? " + this + " " + intent);
			} else {
				Fragment fragment = TaskGridFragment.newInstance(purpose_id);
				FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
				fragmentTransaction.replace(R.id.container, fragment);
				fragmentTransaction.commit();
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId()) {
			case android.R.id.home: {
				finish();
				return true;
			}
		}

		return super.onOptionsItemSelected(item);
	}
}

package ru.alex009.purpose.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;

import ru.alex009.purpose.R;

/**
 * Created by AlekseyMikhailov on 06.12.14.
 */
public class HelpActivity extends ActionBarActivity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

		if (toolbar != null) {
			setSupportActionBar(toolbar);
			getSupportActionBar().setHomeButtonEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}

		WebView webView = (WebView) findViewById(R.id.content);
		webView.loadUrl("file:///android_asset/help/index.html");
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId()) {
			case android.R.id.home: {
				finish();
				return true;
			}
		}
		return super.onOptionsItemSelected(item);
	}
}

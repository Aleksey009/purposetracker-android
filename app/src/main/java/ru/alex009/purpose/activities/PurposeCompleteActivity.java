package ru.alex009.purpose.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import ru.alex009.purpose.R;
import ru.alex009.purpose.core.DataBaseConnector;
import ru.alex009.purpose.core.QueryBuilder;

/**
 * Created by AlekseyMikhailov on 01.12.14.
 */
public class PurposeCompleteActivity extends ActionBarActivity implements View.OnClickListener
{
	private TextView mPurposeTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_purpose_complete);

		TextView titleView = (TextView) findViewById(R.id.completedTitle);
		mPurposeTextView = (TextView) findViewById(R.id.purposeText);
		Button closeBtn = (Button) findViewById(R.id.closeBtn);
		Button shareBtn = (Button) findViewById(R.id.shareBtn);

		titleView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf"));
		mPurposeTextView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf"));

		Intent intent = getIntent();
		int purposeId = intent.getIntExtra("purposeId", -1);

		if (purposeId == -1) {
			Log.e("DEV", this + " incorrect purpose id!");
			finish();
		}

		SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();
		Cursor cursor = db.rawQuery(QueryBuilder.buildPurposeInfoQuery(purposeId), null);

		int columnText = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSELIST_TEXT);

		cursor.moveToFirst();

		String text = cursor.getString(columnText);

		cursor.close();

		mPurposeTextView.setText(text);

		closeBtn.setOnClickListener(this);
		shareBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View view)
	{
		switch (view.getId()) {
			case R.id.closeBtn: {
				finish();
				break;
			}
			case R.id.shareBtn: {
				Resources res = getResources();
				Intent share = new Intent(Intent.ACTION_SEND);
				share.setType("text/plain");
				share.putExtra(Intent.EXTRA_TEXT,
						res.getString(R.string.i_complete_purpose) + " "
								+ mPurposeTextView.getText()
								+ "\n"
								+ res.getString(R.string.sended_from) + " "
								+ res.getString(R.string.app_name));
				startActivity(share);
				break;
			}
		}
	}
}
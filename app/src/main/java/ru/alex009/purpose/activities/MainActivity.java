package ru.alex009.purpose.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;

import ru.alex009.purpose.R;
import ru.alex009.purpose.core.MainApplication;
import ru.alex009.purpose.core.UserSettings;
import ru.alex009.purpose.fragments.PurposeGridFragment;
import ru.alex009.purpose.fragments.SideNavFragment;
import ru.alex009.purpose.fragments.TaskGridFragment;

/**
 * Created by AlekseyMikhailov on 16.11.14.
 */
public class MainActivity extends ActionBarActivity implements SideNavFragment.OnSideNavItemClickListener
{
	private DrawerLayout mDrawer;
	private ActionBarDrawerToggle mDrawerToggle;
	private Toolbar mToolbar;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mToolbar = (Toolbar) findViewById(R.id.toolbar);

		mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.drawer_open, R.string.drawer_close);
		mDrawer.setDrawerListener(mDrawerToggle);
		mDrawer.setStatusBarBackground(R.color.primaryDark);
		mDrawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		if (mToolbar != null) {
			setSupportActionBar(mToolbar);
			getSupportActionBar().setHomeButtonEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}

		if (savedInstanceState == null) {
			Fragment mainFragment = null;
			switch (UserSettings.getMainPage()) {
				case UserSettings.PURPOSES_PAGE_ID: {
					mainFragment = PurposeGridFragment.newInstance();
					break;
				}
				case UserSettings.TASKS_PAGE_ID: {
					mainFragment = TaskGridFragment.newInstance(-1);
					break;
				}
			}
			replaceContent(mainFragment);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int responseCode, Intent intent)
	{
		if (requestCode == MainApplication.GOOGLE_API_LOGIN_REQUEST_CODE && responseCode == Activity.RESULT_OK) {
			MainApplication.getInstance().connectGoogleApiClient(null);
		}
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState)
	{
		super.onPostCreate(savedInstanceState);

		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);

		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (mDrawerToggle.onOptionsItemSelected(item)) return true;

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed()
	{
		FragmentManager fragmentManager = getSupportFragmentManager();
		if (fragmentManager.getBackStackEntryCount() == 0) {
			super.onBackPressed();
		} else {
			fragmentManager.popBackStack();
		}
	}

	protected void replaceContent(Fragment fragment)
	{
		FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.container, fragment);
		fragmentTransaction.commit();
	}

	@Override
	public void onItemClick(int itemId)
	{
		switch (itemId) {
			case SideNavFragment.ITEM_PURPOSES: {
				replaceContent(PurposeGridFragment.newInstance());
				break;
			}
			case SideNavFragment.ITEM_TASKS: {
				replaceContent(TaskGridFragment.newInstance(-1));
				break;
			}
			case SideNavFragment.ITEM_SETTINGS: {
				Intent intent = new Intent(this, SettingsActivity.class);
				startActivity(intent);
				break;
			}
			case SideNavFragment.ITEM_HELP: {
				Intent intent = new Intent(this, HelpActivity.class);
				startActivity(intent);
				break;
			}
		}

		mDrawer.closeDrawers();
	}
}

package ru.alex009.purpose.activities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import ru.alex009.purpose.utils.DynamicListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ru.alex009.purpose.R;
import ru.alex009.purpose.core.DataBaseConnector;
import ru.alex009.purpose.core.QueryBuilder;

/**
 * Created by AlekseyMikhailov on 30.11.14.
 */
public class ColorSortingActivity extends ActionBarActivity
{
	private ArrayList<ColorRow> mColorsList;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_color_sorting);

		mColorsList = new ArrayList<ColorRow>();

		SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();
		Cursor cursor = db.rawQuery(QueryBuilder.buildColorQuery(), null);

		int columnId = cursor.getColumnIndex(QueryBuilder.FIELD_COLOR_ID);
		int columnColor = cursor.getColumnIndex(QueryBuilder.FIELD_COLOR_COLOR);

		while (cursor.moveToNext()) {
			int id = cursor.getInt(columnId);
			int color = cursor.getInt(columnColor);

			ColorRow row = new ColorRow();
			row.id = id;
			row.color = color;

			mColorsList.add(row);
		}

		cursor.close();

		StableArrayAdapter adapter = new StableArrayAdapter(this, R.layout.view_color_sorting, mColorsList);
		DynamicListView listView = (DynamicListView) findViewById(R.id.listview);

		listView.setCheeseList(mColorsList);
		listView.setAdapter(adapter);
		listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

		if (toolbar != null) {
			setSupportActionBar(toolbar);
			getSupportActionBar().setHomeButtonEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setTitle("");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.color_sorting, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId()) {
			case android.R.id.home: {
				finish();
				return true;
			}
			case R.id.action_confirm: {
				SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();

				for (int i = 0; i < mColorsList.size(); i++) {
					ColorRow row = mColorsList.get(i);

					ContentValues cv = new ContentValues();
					cv.put(QueryBuilder.FIELD_COLOR_SORTING, i);

					db.update("Color", cv, "id = ?", new String[]{String.valueOf(row.id)});
				}

				finish();
				return true;
			}
		}
		return super.onOptionsItemSelected(item);
	}

	private static class ColorRow
	{
		public int id;
		public int color;
	}

	public class StableArrayAdapter extends ArrayAdapter<ColorRow>
	{
		final int INVALID_ID = -1;

		HashMap<ColorRow, Integer> mIdMap = new HashMap<ColorRow, Integer>();

		public StableArrayAdapter(Context context, int textViewResourceId, List<ColorRow> objects)
		{
			super(context, textViewResourceId, objects);

			for (int i = 0; i < objects.size(); ++i) {
				mIdMap.put(objects.get(i), i);
			}
		}

		@Override
		public long getItemId(int position)
		{
			if (position < 0 || position >= mIdMap.size()) {
				return INVALID_ID;
			}
			ColorRow item = getItem(position);
			return mIdMap.get(item);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.view_color_sorting, parent, false);
			}

			ColorRow item = getItem(position);

			convertView.setBackgroundColor(item.color);

			return convertView;
		}

		@Override
		public boolean hasStableIds()
		{
			return true;
		}
	}
}

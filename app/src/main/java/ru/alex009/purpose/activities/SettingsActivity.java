package ru.alex009.purpose.activities;

import android.accounts.Account;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import ru.alex009.crossgrid.activity.GameActivity;
import ru.alex009.purpose.R;
import ru.alex009.purpose.core.MainApplication;
import ru.alex009.purpose.core.UserSettings;
import ru.alex009.purpose.sync.SyncAdapter;

/**
 * Created by AlekseyMikhailov on 16.11.14.
 */
public class SettingsActivity extends ActionBarActivity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

		if (toolbar != null) {
			setSupportActionBar(toolbar);
			getSupportActionBar().setHomeButtonEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}

		getFragmentManager()
				.beginTransaction()
				.replace(R.id.container, new SettingsFragment())
				.commit();
	}

	@Override
	public void onActivityResult(int requestCode, int responseCode, Intent intent)
	{
		if (requestCode == MainApplication.GOOGLE_API_LOGIN_REQUEST_CODE && responseCode == Activity.RESULT_OK) {
			MainApplication.getInstance().connectGoogleApiClient(null);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId()) {
			case android.R.id.home: {
				finish();
				return true;
			}
		}
		return super.onOptionsItemSelected(item);
	}

	public static class SettingsFragment extends PreferenceFragment implements Preference.OnPreferenceChangeListener, Preference.OnPreferenceClickListener, MainApplication.LoginListener
	{
		private int mBuildClicksCount;

		public void onCreate(Bundle savedInstanceState)
		{
			super.onCreate(savedInstanceState);

			mBuildClicksCount = 0;

			Activity activity = getActivity();

			addPreferencesFromResource(R.xml.pref_general);

			Preference buildPref = findPreference("pref_build");

			String version = "unknown";
			int versionCode = -1;
			Date buildTime = null;
			PackageInfo pInfo = null;
			ApplicationInfo aInfo = null;

			try {
				pInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
			} catch (PackageManager.NameNotFoundException e) {
				Log.e("DEV", this.toString() + " exception " + e.toString());
			}

			try {
				aInfo = activity.getPackageManager().getApplicationInfo(activity.getPackageName(), 0);
			} catch (PackageManager.NameNotFoundException e) {
				Log.e("DEV", this.toString() + " exception " + e.toString());
			}

			if (pInfo != null) {
				version = pInfo.versionName;
				versionCode = pInfo.versionCode;
			}

			if (aInfo != null) {
				try {
					ZipFile zf = new ZipFile(aInfo.sourceDir);
					ZipEntry ze = zf.getEntry("classes.dex");
					buildTime = new Date(ze.getTime());
					zf.close();
				} catch (IOException e) {
					Log.e("DEV", this.toString() + " exception " + e.toString());
				}
			}

			SimpleDateFormat sdf = new SimpleDateFormat();
			sdf.applyPattern("dd.MM.yyyy");
			buildPref.setSummary("Версия " + version + " (" + versionCode + ") от " + sdf.format(buildTime));

			Preference licenses = findPreference("pref_licenses");
			Preference colorSorting = findPreference("pref_color_sorting");
			EditTextPreference notificationsFrequency = (EditTextPreference) findPreference("pref_notifications_frequency");
			EditTextPreference adviceFrequency = (EditTextPreference) findPreference("pref_advice_frequency");
			ListPreference mainPage = (ListPreference) findPreference("pref_main_page");
			Preference syncAccount = findPreference("pref_sync_account");
			CheckBoxPreference syncIsActive = (CheckBoxPreference) findPreference("pref_sync_is_active");
			Preference syncManual = findPreference("pref_sync_manual");

			notificationsFrequency.setText(String.valueOf(UserSettings.getNotificationsPeriod()));
			adviceFrequency.setText(String.valueOf(UserSettings.getAdvicesCheckPeriod()));
			mainPage.setEntries(new String[]{
					getResources().getString(R.string.purposes),
					getResources().getString(R.string.tasks)
			});
			mainPage.setEntryValues(new String[]{
					String.valueOf(UserSettings.PURPOSES_PAGE_ID),
					String.valueOf(UserSettings.TASKS_PAGE_ID)
			});
			mainPage.setValueIndex(UserSettings.getMainPage());
			mainPage.setSummary(mainPage.getEntry());

			updateSyncPreferences();

			notificationsFrequency.setOnPreferenceChangeListener(this);
			adviceFrequency.setOnPreferenceChangeListener(this);
			mainPage.setOnPreferenceChangeListener(this);
			licenses.setOnPreferenceClickListener(this);
			colorSorting.setOnPreferenceClickListener(this);
			buildPref.setOnPreferenceClickListener(this);
			syncAccount.setOnPreferenceClickListener(this);
			syncIsActive.setOnPreferenceChangeListener(this);
			syncManual.setOnPreferenceClickListener(this);
		}

		@Override
		public void onResume()
		{
			super.onResume();

			MainApplication.getInstance().setLoginListener(this);
		}

        @Override
        public void onPause() {
            super.onPause();

            MainApplication.getInstance().setLoginListener(null);
        }

		@Override
		public boolean onPreferenceChange(Preference preference, Object newValue)
		{
			if (preference.getKey().contentEquals("pref_notifications_frequency")) {
				UserSettings.setNotificationsPeriod(Long.valueOf((String) newValue));
				((EditTextPreference) preference).setText((String) newValue);
			}
			if (preference.getKey().contentEquals("pref_advice_frequency")) {
				UserSettings.setAdvicesCheckPeriod(Long.valueOf((String) newValue));
				((EditTextPreference) preference).setText((String) newValue);
			}
			if (preference.getKey().contentEquals("pref_main_page")) {
				int pageId = Integer.valueOf((String) newValue);
				UserSettings.setMainPage(pageId);

				ListPreference mainPage = ((ListPreference) preference);

				mainPage.setValueIndex(pageId);
				mainPage.setSummary(mainPage.getEntry());
			}
			if(preference.getKey().contentEquals("pref_sync_is_active")) {
				Account account = MainApplication.getInstance().getGoogleAccount();

				if(account != null) {
					CheckBoxPreference checkBoxPreference = (CheckBoxPreference) preference;
					boolean newState = !checkBoxPreference.isChecked();
					checkBoxPreference.setChecked(newState);

                    if(newState) {
                        ContentResolver.setIsSyncable(account, SyncAdapter.AUTHORITY, 1);
                        ContentResolver.setSyncAutomatically(account, SyncAdapter.AUTHORITY, true);
                    }
                    else {
                        ContentResolver.setIsSyncable(account, SyncAdapter.AUTHORITY, 0);
                        ContentResolver.setSyncAutomatically(account, SyncAdapter.AUTHORITY, false);
                    }
				}
			}
			return false;
		}

		@Override
		public boolean onPreferenceClick(Preference preference)
		{
			if (preference.getKey().contentEquals("pref_licenses")) {
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder
						.setTitle(R.string.pref_licenses_title)
						.setMessage(R.string.licenses)
						.setCancelable(true)
						.create()
						.show();

				return true;
			}
			if (preference.getKey().contentEquals("pref_color_sorting")) {
				Intent intent = new Intent(getActivity(), ColorSortingActivity.class);
				startActivity(intent);
				return true;
			}
			if (preference.getKey().contentEquals("pref_build")) {
				mBuildClicksCount++;
				if (mBuildClicksCount == 5) {
					mBuildClicksCount = 0;
					Intent intent = new Intent(getActivity(), GameActivity.class);
					startActivity(intent);
				} else {
					Toast.makeText(getActivity(), "click!", Toast.LENGTH_SHORT).show();
				}
				return true;
			}
			if(preference.getKey().contentEquals("pref_sync_account")) {
				MainApplication app = MainApplication.getInstance();
				GoogleApiClient googleApiClient = app.getGoogleApiClient();

				if (googleApiClient.isConnected()) {
					app.disconnectGoogleApiClient();
					updateSyncPreferences();
				}
				else {
					app.connectGoogleApiClient(getActivity());
				}
			}
			if(preference.getKey().contentEquals("pref_sync_manual")) {
				MainApplication.getInstance().requestSync();
			}
			return false;
		}

		@Override
		public void onConnected(Bundle bundle)
		{
			updateSyncPreferences();
		}

		@Override
		public void onDisconnected()
		{
			updateSyncPreferences();
		}

		private void updateSyncPreferences()
		{
			Preference syncAccount = findPreference("pref_sync_account");
			CheckBoxPreference syncIsActive = (CheckBoxPreference) findPreference("pref_sync_is_active");
			Preference syncManual = findPreference("pref_sync_manual");

			MainApplication app = MainApplication.getInstance();
			GoogleApiClient googleApiClient = app.getGoogleApiClient();

			if(googleApiClient.isConnected()) {
				String email = Plus.AccountApi.getAccountName(googleApiClient);

				boolean syncable = ContentResolver.getSyncAutomatically(app.getGoogleAccount(), SyncAdapter.AUTHORITY);

				syncAccount.setSummary(email);
				syncIsActive.setChecked(syncable);
			}
			else {
				syncAccount.setSummary(R.string.not_connected);
				syncIsActive.setEnabled(false);
				syncManual.setEnabled(false);
			}
		}
	}
}

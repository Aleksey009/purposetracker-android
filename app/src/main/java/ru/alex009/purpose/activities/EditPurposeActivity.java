package ru.alex009.purpose.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import ru.alex009.purpose.R;
import ru.alex009.purpose.adapters.ColorAdapter;
import ru.alex009.purpose.adapters.PriorityAdapter;
import ru.alex009.purpose.core.DataBaseConnector;
import ru.alex009.purpose.core.DataBaseEvents;
import ru.alex009.purpose.core.QueryBuilder;

/**
 * Created by AlekseyMikhailov on 16.11.14.
 */
public class EditPurposeActivity extends ActionBarActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener
{
	private static final String STATE_TEXT = "stateText";
	private static final String STATE_NEED_DATE = "stateNeedDate";
	private static final String STATE_DATE = "stateDate";
	private static final String STATE_TIME = "stateTime";
	private static final String STATE_NEED_ADVICES = "stateNeedAdvices";
	private static final String STATE_COLOR = "stateColor";
	private static final String STATE_IMPORTANT = "stateImportant";

	private Toolbar mToolbar;
	private int mPurposeId;
	private EditText mTextValue;
	private Switch mNeedDateValue;
	private ViewGroup mDateContainer;
	private TextView mDateValue;
	private TextView mTimeValue;
	private Switch mAdvicesValue;
	private ImageView mColorIcon;
	private TextView mColorTitle;
	private TextView mImportantTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_purpose);

		Intent intent = getIntent();
		mPurposeId = intent.getIntExtra("purposeId", -1);

		mToolbar = (Toolbar) findViewById(R.id.toolbar);
		mTextValue = (EditText) findViewById(R.id.textValue);

		mNeedDateValue = (Switch) findViewById(R.id.needDateValue);
		mDateContainer = (ViewGroup) findViewById(R.id.dateContainer);
		mDateValue = (TextView) findViewById(R.id.dateValue);
		mTimeValue = (TextView) findViewById(R.id.timeValue);

		mAdvicesValue = (Switch) findViewById(R.id.advicesValue);

		mColorIcon = (ImageView) findViewById(R.id.colorIcon);
		mColorTitle = (TextView) findViewById(R.id.colorTitle);

		mImportantTitle = (TextView) findViewById(R.id.priorityTitle);

		if (mToolbar != null) {
			setSupportActionBar(mToolbar);
			getSupportActionBar().setHomeButtonEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}

		mNeedDateValue.setOnCheckedChangeListener(this);
		mDateValue.setOnClickListener(this);
		mTimeValue.setOnClickListener(this);
		mColorTitle.setOnClickListener(this);
		mImportantTitle.setOnClickListener(this);

		if (savedInstanceState == null) {
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);
			int hour = c.get(Calendar.HOUR_OF_DAY);
			int minute = c.get(Calendar.MINUTE);

			mDateValue.setText(day + "." + (month + 1) + "." + year);
			mTimeValue.setText(String.format("%2d:%02d", hour, minute));

			mColorTitle.setTag(Integer.valueOf(0));
			mImportantTitle.setTag(Integer.valueOf(PriorityAdapter.DEFAULT_VALUE));

			if (mPurposeId != -1) {
				SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();
				Cursor cursor = db.rawQuery(QueryBuilder.buildSinglePurposeQuery(mPurposeId), null);

				if (cursor.getCount() == 0) {
					Log.e("DEV", this + " PURPOSE NOT FOUND");
					return;
				}

				cursor.moveToFirst();

				int columnText = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSE_TEXT);
				int columnDate = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSE_DATE);
				int columnAdvices = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSE_NEED_ADVICES);
				int columnColor = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSE_COLOR_ID);
				int columnImportant = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSE_IMPORTANT);

				int colorId = cursor.getInt(columnColor);
				int importantValue = cursor.getInt(columnImportant);

				mColorTitle.setTag(colorId);
				mImportantTitle.setTag(importantValue);

				mTextValue.setText(cursor.getString(columnText));
				mAdvicesValue.setChecked(cursor.getInt(columnAdvices) == 1);

				long date = cursor.getLong(columnDate);
				if (date != 0) {
					c.setTimeInMillis(date);
					year = c.get(Calendar.YEAR);
					month = c.get(Calendar.MONTH);
					day = c.get(Calendar.DAY_OF_MONTH);
					hour = c.get(Calendar.HOUR_OF_DAY);
					minute = c.get(Calendar.MINUTE);

					mDateValue.setText(day + "." + (month + 1) + "." + year);
					mTimeValue.setText(String.format("%2d:%02d", hour, minute));

					mNeedDateValue.setChecked(true);
				} else {
					mNeedDateValue.setChecked(false);
				}
			}
		} else {
			mTextValue.setText(savedInstanceState.getString(STATE_TEXT));
			mNeedDateValue.setChecked(savedInstanceState.getBoolean(STATE_NEED_DATE));
			mDateValue.setText(savedInstanceState.getString(STATE_DATE));
			mTimeValue.setText(savedInstanceState.getString(STATE_TIME));
			mAdvicesValue.setChecked(savedInstanceState.getBoolean(STATE_NEED_ADVICES));
			mColorTitle.setTag(savedInstanceState.getInt(STATE_COLOR));
			mImportantTitle.setTag(savedInstanceState.getInt(STATE_IMPORTANT));
		}

		int importantValue = (Integer) mImportantTitle.getTag();
		mImportantTitle.setText(new PriorityAdapter(this).getItem(importantValue));
		updateColorIcon();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		outState.putString(STATE_TEXT, mTextValue.getText().toString());
		outState.putBoolean(STATE_NEED_DATE, mNeedDateValue.isChecked());
		outState.putString(STATE_DATE, mDateValue.getText().toString());
		outState.putString(STATE_TIME, mTimeValue.getText().toString());
		outState.putBoolean(STATE_NEED_ADVICES, mAdvicesValue.isChecked());
		outState.putInt(STATE_COLOR, (Integer) mColorTitle.getTag());
		outState.putInt(STATE_IMPORTANT, (Integer) mImportantTitle.getTag());

		super.onSaveInstanceState(outState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.edit_purpose, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId()) {
			case android.R.id.home: {
				finish();
				return true;
			}
			case R.id.action_confirm: {
				SimpleDateFormat sdf = new SimpleDateFormat("d.M.yyyy H:m");

				String text = mTextValue.getText().toString();
				long date = 0;
				if (mNeedDateValue.isChecked()) {
					try {
						date = sdf.parse(mDateValue.getText() + " " + mTimeValue.getText()).getTime();
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				int needAdvices = (mAdvicesValue.isChecked() ? 1 : 0);
				int colorId = (Integer) mColorTitle.getTag();
				int importantValue = (Integer) mImportantTitle.getTag();

				ContentValues cv = new ContentValues();
				cv.put(QueryBuilder.FIELD_PURPOSE_TEXT, text);
				cv.put(QueryBuilder.FIELD_PURPOSE_DATE, date);
				cv.put(QueryBuilder.FIELD_PURPOSE_NEED_ADVICES, needAdvices);
				cv.put(QueryBuilder.FIELD_PURPOSE_COLOR_ID, colorId);
				cv.put(QueryBuilder.FIELD_PURPOSE_IMPORTANT, importantValue);

				long timeNow = Calendar.getInstance().getTime().getTime();

				SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();
				if (mPurposeId == -1) {
					cv.put(QueryBuilder.FIELD_PURPOSE_CREATED, timeNow);
					cv.put(QueryBuilder.FIELD_PURPOSE_UPDATED, timeNow);

					db.insert("Purpose", null, cv);

					DataBaseEvents.onRowCreated(DataBaseEvents.TABLE_PURPOSE_ID, -1, cv);

					Toast.makeText(this, R.string.purpose_added, Toast.LENGTH_LONG).show();
				} else {
					cv.put(QueryBuilder.FIELD_PURPOSE_UPDATED, timeNow);

					db.update("Purpose", cv, QueryBuilder.FIELD_PURPOSE_ID + " = ?", new String[]{String.valueOf(mPurposeId)});

					DataBaseEvents.onRowEdited(DataBaseEvents.TABLE_PURPOSE_ID, mPurposeId, cv);

					Toast.makeText(this, R.string.purpose_changed, Toast.LENGTH_LONG).show();
				}

				finish();
				return true;
			}
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View view)
	{
		switch (view.getId()) {
			case R.id.dateValue: {
				final Calendar c = Calendar.getInstance();
				int year = c.get(Calendar.YEAR);
				int month = c.get(Calendar.MONTH);
				int day = c.get(Calendar.DAY_OF_MONTH);

				new DatePickerDialog(this, this, year, month, day).show();
				break;
			}
			case R.id.timeValue: {
				final Calendar c = Calendar.getInstance();
				int hour = c.get(Calendar.HOUR_OF_DAY);
				int minute = c.get(Calendar.MINUTE);

				new TimePickerDialog(this, this, hour, minute, true).show();
				break;
			}
			case R.id.colorTitle: {
				final AlertDialog dialog = new AlertDialog.Builder(this)
						.setTitle(R.string.color)
						.create();

				GridView gridView = new GridView(this);
				gridView.setNumColumns(4);
				gridView.setAdapter(new ColorAdapter(this));
				gridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
				{
					@Override
					public void onItemClick(AdapterView<?> adapterView, View view, int position, long id)
					{
						mColorTitle.setTag((int) id);
						updateColorIcon();
						dialog.dismiss();
					}
				});

				dialog.setView(gridView);
				dialog.show();
				break;
			}
			case R.id.priorityTitle: {
				new AlertDialog.Builder(this)
						.setTitle(R.string.priority)
						.setAdapter(new PriorityAdapter(this), new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialogInterface, int i)
							{
								mImportantTitle.setTag(i);
								mImportantTitle.setText(new PriorityAdapter(EditPurposeActivity.this).getItem(i));
							}
						})
						.show();
				break;
			}
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
	{
		if (buttonView == mNeedDateValue) {
			if (isChecked) {
				mDateContainer.setVisibility(View.VISIBLE);
			} else {
				mDateContainer.setVisibility(View.GONE);
			}
		}
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
	{
		mDateValue.setText(dayOfMonth + "." + (monthOfYear + 1) + "." + year);

		checkDateTimeValid();
	}

	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute)
	{
		mTimeValue.setText(String.format("%2d:%02d", hourOfDay, minute));

		checkDateTimeValid();
	}

	private void checkDateTimeValid()
	{
		String dateTimeString = mDateValue.getText() + " " + mTimeValue.getText();
		SimpleDateFormat sdf = new SimpleDateFormat("d.M.yyyy H:m");

		try {
			Date dateTime = sdf.parse(dateTimeString);

			if (Calendar.getInstance().getTimeInMillis() > dateTime.getTime()) {
				Toast.makeText(this, R.string.date_time_invalid, Toast.LENGTH_SHORT).show();
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	private void updateColorIcon()
	{
		int colorId = (Integer) mColorTitle.getTag();

		SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();
		Cursor cursor = db.rawQuery(QueryBuilder.buildSingleColorQuery(colorId), null);
		if(cursor.moveToNext()) {
			long color = cursor.getLong(cursor.getColumnIndex(QueryBuilder.FIELD_COLOR_COLOR));
			mColorIcon.setColorFilter((int)color, PorterDuff.Mode.MULTIPLY);
		}
		cursor.close();
	}
}

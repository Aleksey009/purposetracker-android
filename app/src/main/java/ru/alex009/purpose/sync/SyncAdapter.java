package ru.alex009.purpose.sync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.content.SyncResult;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.exceptions.BackendlessException;
import com.backendless.persistence.BackendlessDataQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.alex009.purpose.backend.Contact;
import ru.alex009.purpose.backend.Place;
import ru.alex009.purpose.backend.Purpose;
import ru.alex009.purpose.backend.Task;
import ru.alex009.purpose.core.DataBaseConnector;
import ru.alex009.purpose.core.MainApplication;
import ru.alex009.purpose.core.QueryBuilder;

/**
 * Created by AlekseyMikhailov on 11.12.14.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter
{
	private static final byte[] mData1 = new byte[]{0x33, 0x37, 0x39, 0x44, 0x30, 0x34, 0x46, 0x35, 0x2D, 0x32, 0x35, 0x42, 0x46, 0x2D, 0x46, 0x39, 0x30, 0x44, 0x2D, 0x46, 0x46, 0x37, 0x36, 0x2D, 0x31, 0x32, 0x36, 0x44, 0x42, 0x30, 0x39, 0x46, 0x31, 0x32, 0x30, 0x30};
	private static final byte[] mData2 = new byte[]{0x41, 0x33, 0x36, 0x37, 0x34, 0x46, 0x32, 0x42, 0x2D, 0x42, 0x41, 0x42, 0x43, 0x2D, 0x45, 0x33, 0x43, 0x30, 0x2D, 0x46, 0x46, 0x36, 0x41, 0x2D, 0x44, 0x39, 0x35, 0x45, 0x35, 0x36, 0x38, 0x45, 0x36, 0x45, 0x30, 0x30};
	private static final byte[] mData3 = new byte[]{0x76, 0x31};

	public static final String AUTHORITY = "ru.alex009.purpose.AUTHORITY";

	public SyncAdapter(Context context, boolean autoInitialize)
	{
		super(context, autoInitialize);
	}

	@Override
	public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult)
	{
		String owner = account.name;

		Log.d("DEV", "account: " + account);

		Account loggedAccount = MainApplication.getInstance().getGoogleAccount();

		if(loggedAccount == null || !loggedAccount.name.equalsIgnoreCase(account.name)) {
			Log.d("DEV", "not logged account");
			syncResult.stats.numAuthExceptions++;
			return;
		}

		Backendless.initApp(getContext(), new String(mData1), new String(mData2), new String(mData3));

		BackendlessUser user = null;

		try {
			user = Backendless.UserService.login(owner, owner);
			Log.d("DEV", "login: " + user);
		}
		catch (BackendlessException exc) {
			Log.e("DEV", "not login: " + exc);
		}

		if(user == null) {
			user = new BackendlessUser();
			user.setEmail(owner);
			user.setPassword(owner);

			try {
				Backendless.UserService.register(user);
			}
			catch(BackendlessException exc) {
				Log.e("DEV", "not register: " + exc);

				user = null;
			}
		}

		if(user == null) {
			Log.d("DEV", "user not found");
			syncResult.stats.numAuthExceptions++;
			return;
		}

		SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();

		// получаем список удаленных после последней синхронизации объектов
		List<String> deletedObjects = new ArrayList<>();
		{
			Cursor cursor = db.rawQuery("SELECT remote_id FROM DeletedObject", null);

			while(cursor.moveToNext()) {
				deletedObjects.add(cursor.getString(0));
			}

			cursor.close();
		}

		// синхронизация таблиц
		// цели
		new DataMerger<Purpose>().sync(Purpose.class,
				db,
				user,
				deletedObjects,
				syncResult,
				new DataMerger.DataMergerListener<Purpose>()
				{
					@Override
					public List<Purpose> loadLocalObjects(SQLiteDatabase db, BackendlessUser backendlessUser, BackendlessDataQuery backendlessDataQuery)
					{
						Cursor cursor = db.rawQuery(QueryBuilder.buildPurposeQuery(), null);

						int columnId = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSE_ID);
						int columnText = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSE_TEXT);
						int columnDate = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSE_DATE);
						int columnColorId = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSE_COLOR_ID);
						int columnImportant = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSE_IMPORTANT);
						int columnNeedAdvices = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSE_NEED_ADVICES);
						int columnCreated = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSE_CREATED);
						int columnUpdated = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSE_UPDATED);
						int columnRemoteId = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSE_REMOTE_ID);

						List<Purpose> data = new ArrayList<>();

						while(cursor.moveToNext()) {
							Purpose purpose = new Purpose();

							purpose.ownerId = backendlessUser.getObjectId();

							long datetime = cursor.getLong(columnDate);
							long created = cursor.getLong(columnCreated);
							long updated = cursor.getLong(columnUpdated);

							purpose.id = cursor.getInt(columnId);
							purpose.text = cursor.getString(columnText);
							purpose.datetime = (datetime > 0 ? new Date(datetime) : null);
							purpose.colorId = cursor.getInt(columnColorId);
							purpose.important = cursor.getInt(columnImportant);
							purpose.needAdvices = cursor.getInt(columnNeedAdvices);
							purpose.created = (created > 0 ? new Date(created) : null);
							purpose.updated = (updated > 0 ? new Date(updated) : null);
							purpose.objectId = cursor.getString(columnRemoteId);

							data.add(purpose);
						}

						cursor.close();

						return data;
					}

					@Override
					public void setLocalObjectId(SQLiteDatabase db, BackendlessUser backendlessUser, Purpose object)
					{
						ContentValues cv = new ContentValues();
						cv.put(QueryBuilder.FIELD_PURPOSE_REMOTE_ID, object.objectId);

						db.update("Purpose", cv, QueryBuilder.FIELD_PURPOSE_ID + " = ?", new String[]{String.valueOf(object.id)});
					}

					@Override
					public boolean deleteLocalObject(SQLiteDatabase db, BackendlessUser backendlessUser, Purpose object)
					{
						try {
							db.delete("Purpose", QueryBuilder.FIELD_PURPOSE_ID + " = ?", new String[] { String.valueOf(object.id) });
							return true;
						}
						catch (Exception exc) {
							exc.printStackTrace();
							return false;
						}
					}

					@Override
					public boolean saveLocalObject(SQLiteDatabase db, BackendlessUser backendlessUser, Purpose object, int saveMode)
					{
						try {
							ContentValues cv = new ContentValues();
							cv.put(QueryBuilder.FIELD_PURPOSE_ID, object.id);
							cv.put(QueryBuilder.FIELD_PURPOSE_TEXT, object.text);
							if(object.datetime != null) cv.put(QueryBuilder.FIELD_PURPOSE_DATE, object.datetime.getTime());
							cv.put(QueryBuilder.FIELD_PURPOSE_COLOR_ID, object.colorId);
							cv.put(QueryBuilder.FIELD_PURPOSE_IMPORTANT, object.important);
							cv.put(QueryBuilder.FIELD_PURPOSE_NEED_ADVICES, object.needAdvices);
							if(object.created != null) cv.put(QueryBuilder.FIELD_PURPOSE_CREATED, object.created.getTime());
							if(object.updated != null) cv.put(QueryBuilder.FIELD_PURPOSE_UPDATED, object.updated.getTime());
							cv.put(QueryBuilder.FIELD_PURPOSE_REMOTE_ID, object.objectId);

							if(saveMode == DataMerger.SAVE_MODE_INSERT) {
								db.insert("Purpose", null, cv);
							}
							else if(saveMode == DataMerger.SAVE_MODE_UPDATE) {
								db.update("Purpose", cv, QueryBuilder.FIELD_PURPOSE_ID + " = ?", new String[]{String.valueOf(object.id)});
							}
							return true;
						}
						catch (Exception exc) {
							exc.printStackTrace();
							return false;
						}
					}
				});
		// задачи
		new DataMerger<Task>().sync(Task.class,
				db,
				user,
				deletedObjects,
				syncResult,
				new DataMerger.DataMergerListener<Task>()
				{
					@Override
					public List<Task> loadLocalObjects(SQLiteDatabase db, BackendlessUser backendlessUser, BackendlessDataQuery backendlessDataQuery)
					{
						Cursor cursor = db.rawQuery(QueryBuilder.buildTaskQuery(), null);

						int columnId = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_ID);
                        int columnPurposeId = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_PURPOSE_ID);
						int columnText = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_TEXT);
						int columnDate = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_DATE);
						int columnCompleted = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_COMPLETED);
						int columnNeedAdvices = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_NEED_ADVICES);
						int columnColorId = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_COLOR_ID);
						int columnImportant = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_IMPORTANT);
						int columnReminderDays = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_REMINDER_DAYS);
						int columnReminderTime = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_REMINDER_TIME);
						int columnCreated = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_CREATED);
						int columnUpdated = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_UPDATED);
						int columnRemoteId = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_REMOTE_ID);

						List<Task> data = new ArrayList<>();

						while(cursor.moveToNext()) {
							Task task = new Task();

							task.ownerId = backendlessUser.getObjectId();

							long datetime = cursor.getLong(columnDate);
							long created = cursor.getLong(columnCreated);
							long updated = cursor.getLong(columnUpdated);

							task.id = cursor.getInt(columnId);
                            task.purposeId = cursor.getInt(columnPurposeId);
							task.text = cursor.getString(columnText);
							task.datetime = (datetime > 0 ? new Date(datetime) : null);
							task.completed = cursor.getInt(columnCompleted);
							task.colorId = cursor.getInt(columnColorId);
							task.important = cursor.getInt(columnImportant);
							task.needAdvices = cursor.getInt(columnNeedAdvices);
							task.reminderDays = cursor.getString(columnReminderDays);
							task.reminderTime = cursor.getInt(columnReminderTime);
							task.created = (created > 0 ? new Date(created) : null);
							task.updated = (updated > 0 ? new Date(updated) : null);
							task.objectId = cursor.getString(columnRemoteId);

							data.add(task);
						}

						cursor.close();

						return data;
					}

					@Override
					public void setLocalObjectId(SQLiteDatabase db, BackendlessUser backendlessUser, Task object)
					{
						ContentValues cv = new ContentValues();
						cv.put(QueryBuilder.FIELD_TASK_REMOTE_ID, object.objectId);

						db.update("Task", cv, QueryBuilder.FIELD_TASK_ID + " = ?", new String[]{String.valueOf(object.id)});
					}

					@Override
					public boolean deleteLocalObject(SQLiteDatabase db, BackendlessUser backendlessUser, Task object)
					{
						try {
							db.delete("Task", QueryBuilder.FIELD_TASK_ID + " = ?", new String[] { String.valueOf(object.id) });
							return true;
						}
						catch (Exception exc) {
							exc.printStackTrace();
							return false;
						}
					}

					@Override
					public boolean saveLocalObject(SQLiteDatabase db, BackendlessUser backendlessUser, Task object, int saveMode)
					{
						try {
							ContentValues cv = new ContentValues();
							cv.put(QueryBuilder.FIELD_TASK_ID, object.id);
                            cv.put(QueryBuilder.FIELD_TASK_PURPOSE_ID, object.purposeId);
							cv.put(QueryBuilder.FIELD_TASK_TEXT, object.text);
							if(object.datetime != null) cv.put(QueryBuilder.FIELD_TASK_DATE, object.datetime.getTime());
							cv.put(QueryBuilder.FIELD_TASK_COMPLETED, object.completed);
							cv.put(QueryBuilder.FIELD_TASK_COLOR_ID, object.colorId);
							cv.put(QueryBuilder.FIELD_TASK_IMPORTANT, object.important);
							cv.put(QueryBuilder.FIELD_TASK_NEED_ADVICES, object.needAdvices);
							cv.put(QueryBuilder.FIELD_TASK_REMINDER_DAYS, object.reminderDays);
							cv.put(QueryBuilder.FIELD_TASK_REMINDER_TIME, object.reminderTime);
							if(object.created != null) cv.put(QueryBuilder.FIELD_TASK_CREATED, object.created.getTime());
							if(object.updated != null) cv.put(QueryBuilder.FIELD_TASK_UPDATED, object.updated.getTime());
							cv.put(QueryBuilder.FIELD_TASK_REMOTE_ID, object.objectId);

							if(saveMode == DataMerger.SAVE_MODE_INSERT) {
								db.insert("Task", null, cv);
							}
							else if(saveMode == DataMerger.SAVE_MODE_UPDATE) {
								db.update("Task", cv, QueryBuilder.FIELD_TASK_ID + " = ?", new String[]{String.valueOf(object.id)});
							}
							return true;
						}
						catch (Exception exc) {
							exc.printStackTrace();
							return false;
						}
					}
				});
		// контакты
		new DataMerger<Contact>().sync(Contact.class,
				db,
				user,
				deletedObjects,
				syncResult,
				new DataMerger.DataMergerListener<Contact>()
				{
					@Override
					public List<Contact> loadLocalObjects(SQLiteDatabase db, BackendlessUser backendlessUser, BackendlessDataQuery backendlessDataQuery)
					{
						Cursor cursor = db.rawQuery(QueryBuilder.buildContactsQuery(), null);

						int columnId = cursor.getColumnIndex(QueryBuilder.FIELD_CONTACT_ID);
						int columnTaskId = cursor.getColumnIndex(QueryBuilder.FIELD_CONTACT_TASK_ID);
						int columnContactUri = cursor.getColumnIndex(QueryBuilder.FIELD_CONTACT_CONTACT_URI);
						int columnCreated = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_CREATED);
						int columnUpdated = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_UPDATED);
						int columnRemoteId = cursor.getColumnIndex(QueryBuilder.FIELD_TASK_REMOTE_ID);

						List<Contact> data = new ArrayList<>();

						while(cursor.moveToNext()) {
							Contact contact = new Contact();

							contact.ownerId = backendlessUser.getObjectId();

							long created = cursor.getLong(columnCreated);
							long updated = cursor.getLong(columnUpdated);

							contact.id = cursor.getInt(columnId);
							contact.task_id = cursor.getInt(columnTaskId);
							contact.contact_uri = cursor.getString(columnContactUri);
							contact.created = (created > 0 ? new Date(created) : null);
							contact.updated = (updated > 0 ? new Date(updated) : null);
							contact.objectId = cursor.getString(columnRemoteId);

							data.add(contact);
						}

						cursor.close();

						return data;
					}

					@Override
					public void setLocalObjectId(SQLiteDatabase db, BackendlessUser backendlessUser, Contact object)
					{
						ContentValues cv = new ContentValues();
						cv.put(QueryBuilder.FIELD_CONTACT_REMOTE_ID, object.objectId);

						db.update("Contact", cv, QueryBuilder.FIELD_CONTACT_ID + " = ?", new String[]{String.valueOf(object.id)});
					}

					@Override
					public boolean deleteLocalObject(SQLiteDatabase db, BackendlessUser backendlessUser, Contact object)
					{
						try {
							db.delete("Contact", QueryBuilder.FIELD_CONTACT_ID + " = ?", new String[] { String.valueOf(object.id) });
							return true;
						}
						catch (Exception exc) {
							exc.printStackTrace();
							return false;
						}
					}

					@Override
					public boolean saveLocalObject(SQLiteDatabase db, BackendlessUser backendlessUser, Contact object, int saveMode)
					{
						try {
							ContentValues cv = new ContentValues();
							cv.put(QueryBuilder.FIELD_CONTACT_ID, object.id);
							cv.put(QueryBuilder.FIELD_CONTACT_TASK_ID, object.task_id);
							cv.put(QueryBuilder.FIELD_CONTACT_CONTACT_URI, object.contact_uri);
							if(object.created != null) cv.put(QueryBuilder.FIELD_CONTACT_CREATED, object.created.getTime());
							if(object.updated != null) cv.put(QueryBuilder.FIELD_CONTACT_UPDATED, object.updated.getTime());
							cv.put(QueryBuilder.FIELD_CONTACT_REMOTE_ID, object.objectId);

							if(saveMode == DataMerger.SAVE_MODE_INSERT) {
								db.insert("Contact", null, cv);
							}
							else if(saveMode == DataMerger.SAVE_MODE_UPDATE) {
								db.update("Contact", cv, QueryBuilder.FIELD_CONTACT_ID + " = ?", new String[]{String.valueOf(object.id)});
							}
							return true;
						}
						catch (Exception exc) {
							exc.printStackTrace();
							return false;
						}
					}
				});
		// места
		new DataMerger<Place>().sync(Place.class,
				db,
				user,
				deletedObjects,
				syncResult,
				new DataMerger.DataMergerListener<Place>()
				{
					@Override
					public List<Place> loadLocalObjects(SQLiteDatabase db, BackendlessUser backendlessUser, BackendlessDataQuery backendlessDataQuery)
					{
						Cursor cursor = db.rawQuery(QueryBuilder.buildPlacesQuery(), null);

						int columnId = cursor.getColumnIndex(QueryBuilder.FIELD_PLACE_ID);
						int columnTaskId = cursor.getColumnIndex(QueryBuilder.FIELD_PLACE_TASK_ID);
						int columnDescription = cursor.getColumnIndex(QueryBuilder.FIELD_PLACE_DESCRIPTION);
						int columnCoords = cursor.getColumnIndex(QueryBuilder.FIELD_PLACE_COORDS);
						int columnCreated = cursor.getColumnIndex(QueryBuilder.FIELD_PLACE_CREATED);
						int columnUpdated = cursor.getColumnIndex(QueryBuilder.FIELD_PLACE_UPDATED);
						int columnRemoteId = cursor.getColumnIndex(QueryBuilder.FIELD_PLACE_REMOTE_ID);

						List<Place> data = new ArrayList<>();

						while(cursor.moveToNext()) {
							Place place = new Place();

							place.ownerId = backendlessUser.getObjectId();

							long created = cursor.getLong(columnCreated);
							long updated = cursor.getLong(columnUpdated);

							place.id = cursor.getInt(columnId);
							place.task_id = cursor.getInt(columnTaskId);
							place.description = cursor.getString(columnDescription);
							place.coords = cursor.getString(columnCoords);
							place.created = (created > 0 ? new Date(created) : null);
							place.updated = (updated > 0 ? new Date(updated) : null);
							place.objectId = cursor.getString(columnRemoteId);

							data.add(place);
						}

						cursor.close();

						return data;
					}

					@Override
					public void setLocalObjectId(SQLiteDatabase db, BackendlessUser backendlessUser, Place object)
					{
						ContentValues cv = new ContentValues();
						cv.put(QueryBuilder.FIELD_PLACE_REMOTE_ID, object.objectId);

						db.update("Place", cv, QueryBuilder.FIELD_PLACE_ID + " = ?", new String[]{String.valueOf(object.id)});
					}

					@Override
					public boolean deleteLocalObject(SQLiteDatabase db, BackendlessUser backendlessUser, Place object)
					{
						try {
							db.delete("Place", QueryBuilder.FIELD_PLACE_ID + " = ?", new String[]{String.valueOf(object.id)});
							return true;
						}
						catch (Exception exc) {
							exc.printStackTrace();
							return false;
						}
					}

					@Override
					public boolean saveLocalObject(SQLiteDatabase db, BackendlessUser backendlessUser, Place object, int saveMode)
					{
						try {
							ContentValues cv = new ContentValues();
							cv.put(QueryBuilder.FIELD_PLACE_ID, object.id);
							cv.put(QueryBuilder.FIELD_PLACE_TASK_ID, object.task_id);
							cv.put(QueryBuilder.FIELD_PLACE_DESCRIPTION, object.description);
							cv.put(QueryBuilder.FIELD_PLACE_COORDS, object.coords);
							if(object.created != null) cv.put(QueryBuilder.FIELD_PLACE_CREATED, object.created.getTime());
							if(object.updated != null) cv.put(QueryBuilder.FIELD_PLACE_UPDATED, object.updated.getTime());
							cv.put(QueryBuilder.FIELD_PLACE_REMOTE_ID, object.objectId);

							if(saveMode == DataMerger.SAVE_MODE_INSERT) {
								db.insert("Place", null, cv);
							}
							else if(saveMode == DataMerger.SAVE_MODE_UPDATE) {
								db.update("Place", cv, QueryBuilder.FIELD_PLACE_ID + " = ?", new String[]{String.valueOf(object.id)});
							}
							return true;
						}
						catch (Exception exc) {
							exc.printStackTrace();
							return false;
						}
					}
				});

		// так как синхронизация завершена можно очистить список удаленных объектов (на серваке уже удалили их)
		db.delete("DeletedObject", null, null);

		// ливаем пацаны!
		Backendless.UserService.logout();

		// результаты
		Log.d("DEV", "syncResult: " + syncResult);
	}
}

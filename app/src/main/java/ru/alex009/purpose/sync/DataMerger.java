package ru.alex009.purpose.sync;

import android.content.SyncResult;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.exceptions.BackendlessException;
import com.backendless.persistence.BackendlessDataQuery;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by AlekseyMikhailov on 12.12.14.
 */
public class DataMerger<E>
{
	public static final int SAVE_MODE_INSERT = 0;
	public static final int SAVE_MODE_UPDATE = 1;

	public interface DataMergerListener<E>
	{
		public List<E> loadLocalObjects(SQLiteDatabase db, BackendlessUser backendlessUser, BackendlessDataQuery backendlessDataQuery);

		public void setLocalObjectId(SQLiteDatabase db, BackendlessUser backendlessUser, E object);

		public boolean saveLocalObject(SQLiteDatabase db, BackendlessUser backendlessUser, E object, int saveMode);
		public boolean deleteLocalObject(SQLiteDatabase db, BackendlessUser backendlessUser, E object);
	}

	public void sync(java.lang.Class<E> entityClass,
					 SQLiteDatabase db,
					 BackendlessUser backendlessUser,
					 List<String> deletedObjects,
					 SyncResult syncResult,
					 DataMergerListener<E> listener)
	{
		List<E> remoteData;
		List<E> localData;
		List<E> insertedRows = new ArrayList<>();
		List<E> updatedRows = new ArrayList<>();
		List<E> deletedRows = new ArrayList<>();

		// забираем с сервака данные о дате создания, обновления и идентификатор объекта
		BackendlessDataQuery backendlessDataQuery = new BackendlessDataQuery();
		backendlessDataQuery.setWhereClause("ownerId = '" + backendlessUser.getObjectId() + "'");
		backendlessDataQuery.addProperty("created");
		backendlessDataQuery.addProperty("updated");
		backendlessDataQuery.addProperty("objectId");
		backendlessDataQuery.addProperty("id");

		// грузим данные с сервера
		{
			BackendlessCollection<E> result = Backendless.Persistence.of(entityClass)
					.find(backendlessDataQuery);

			remoteData = new ArrayList<>();

			int totalObjects = result.getTotalObjects();

			while(totalObjects > 0) {
				List<E> page = result.getCurrentPage();

				remoteData.addAll(page);

				totalObjects -= page.size();

				result = result.nextPage();
			}
		}

		// грузим локальные данные
		localData = listener.loadLocalObjects(db, backendlessUser, backendlessDataQuery);

		// рефлексия для получения ид объекта и даты обновления
		Field fieldObjectId;
		Field fieldUpdated;

		try {
			fieldObjectId = entityClass.getField("objectId");
			fieldUpdated = entityClass.getField("updated");
		}
		catch (NoSuchFieldException exc) {
			exc.printStackTrace();
			return;
		}

		// ищем локальные изменения
		{
			insertedRows.clear();
			updatedRows.clear();
			deletedRows.clear();

			for(E localRow: localData) {
				boolean founded = false;

				String localObjectId = getFieldString(fieldObjectId, localRow);

				for(E remoteRow: remoteData) {
					String remoteObjectId = getFieldString(fieldObjectId, remoteRow);

					if(!remoteObjectId.equalsIgnoreCase(localObjectId)) continue;

					founded = true;

					Date remoteUpdated = getFieldDate(fieldUpdated, remoteRow);
					Date localUpdated = getFieldDate(fieldUpdated, localRow);

					if(localUpdated == null) continue;

					if(remoteUpdated == null || localUpdated.getTime() > remoteUpdated.getTime()) {
						updatedRows.add(localRow);
					}
				}

				if(!founded) {
					if(localObjectId == null) insertedRows.add(localRow);
					else deletedRows.add(localRow);
				}
			}

			for(E insertedRow: insertedRows) {
				try {
					Backendless.Persistence.of(entityClass).save(insertedRow);

					listener.setLocalObjectId(db, backendlessUser, insertedRow);
					syncResult.stats.numInserts++;
				}
				catch (BackendlessException exc) {
					syncResult.stats.numIoExceptions++;
				}
				Log.d("DEV", "(" + entityClass.getSimpleName() + ")[L->R] insert [" + getFieldString(fieldObjectId, insertedRow) + "]");
			}
			for(E updatedRow: updatedRows) {
				try {
					Backendless.Persistence.of(entityClass).save(updatedRow);
					syncResult.stats.numUpdates++;
				}
				catch (BackendlessException exc) {
					syncResult.stats.numIoExceptions++;
				}
				Log.d("DEV", "(" + entityClass.getSimpleName() + ")[L->R] update [" + getFieldString(fieldObjectId, updatedRow) + "]");
			}
			for(E deletedRow: deletedRows) {
				if(listener.deleteLocalObject(db, backendlessUser, deletedRow)) {
					syncResult.stats.numDeletes++;
				}
				else {
					syncResult.stats.numIoExceptions++;
				}
				Log.d("DEV", "(" + entityClass.getSimpleName() + ")[L->R] delete [" + getFieldString(fieldObjectId, deletedRow) + "]");
			}
		}
		// ищем внешние изменения
		{
			insertedRows.clear();
			updatedRows.clear();
			deletedRows.clear();

			for(E remoteRow: remoteData) {
				boolean founded = false;

				String remoteObjectId = getFieldString(fieldObjectId, remoteRow);

				for(E localRow: localData) {
					String localObjectId = getFieldString(fieldObjectId, localRow);

					if(!remoteObjectId.equalsIgnoreCase(localObjectId)) continue;

					founded = true;

					Date localUpdated = getFieldDate(fieldUpdated, localRow);
					Date remoteUpdated = getFieldDate(fieldUpdated, remoteRow);

					if(remoteUpdated == null) continue;

					if(localUpdated == null || remoteUpdated.getTime() > localUpdated.getTime()) {
						updatedRows.add(remoteRow);
					}
				}

				if(!founded) {
					boolean deleted = false;
					for(String deleted_remote_id: deletedObjects) {
						if(!deleted_remote_id.equalsIgnoreCase(remoteObjectId)) continue;

						deleted = true;
					}

					if(deleted) {
						deletedRows.add(remoteRow);
					}
					else {
						insertedRows.add(remoteRow);
					}
				}
			}

			for(E insertedRow: insertedRows) {
				E fullInfo = Backendless.Persistence.of(entityClass).findById(insertedRow);

				if(listener.saveLocalObject(db, backendlessUser, fullInfo, SAVE_MODE_INSERT)) {
					syncResult.stats.numInserts++;
				}
				else {
					syncResult.stats.numIoExceptions++;
				}
				Log.d("DEV", "(" + entityClass.getSimpleName() + ")[R->L] insert [" + getFieldString(fieldObjectId, fullInfo) + "]");
			}
			for(E updatedRow: updatedRows) {
				E fullInfo = Backendless.Persistence.of(entityClass).findById(updatedRow);

				if(listener.saveLocalObject(db, backendlessUser, fullInfo, SAVE_MODE_UPDATE)) {
					syncResult.stats.numInserts++;
				}
				else {
					syncResult.stats.numIoExceptions++;
				}
				Log.d("DEV", "(" + entityClass.getSimpleName() + ")[R->L] update [" + getFieldString(fieldObjectId, updatedRow) + "]");
			}
			for(E deletedRow: deletedRows) {
				try {
					Backendless.Persistence.of(entityClass).remove(deletedRow);
					syncResult.stats.numDeletes++;
				}
				catch (BackendlessException exc) {
					syncResult.stats.numIoExceptions++;
				}
				Log.d("DEV", "(" + entityClass.getSimpleName() + ")[R->L] delete [" + getFieldString(fieldObjectId, deletedRow) + "]");
			}
		}
	}

	public static String getFieldString(Field field, Object object)
	{
		try {
			return (String) field.get(object);
		}
		catch (IllegalAccessException exc) {
			return null;
		}
	}

	public static Date getFieldDate(Field field, Object object)
	{
		try {
			return (Date) field.get(object);
		}
		catch (IllegalAccessException exc) {
			return null;
		}
	}
}

package ru.alex009.purpose.core;

import android.app.Activity;
import android.graphics.Typeface;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.alex009.purpose.R;

/**
 * Created by AlekseyMikhailov on 26.11.14.
 */
public class AdviceViewBuilder
{
	private View mMainView;
	private TextView mTitle;
	private TextView mDescription;

	public static AdviceViewBuilder init(Activity activity, ViewGroup parent)
	{
		AdviceViewBuilder builder = new AdviceViewBuilder();

		builder.mMainView = activity.getLayoutInflater().inflate(R.layout.view_advice, parent, false);
		builder.mTitle = (TextView) builder.mMainView.findViewById(R.id.advice_title);
		builder.mDescription = (TextView) builder.mMainView.findViewById(R.id.advice_description);

		builder.mTitle.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/Roboto-Medium.ttf"));
		builder.mDescription.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/Roboto-Regular.ttf"));

		return builder;
	}

	public AdviceViewBuilder setTitle(String title)
	{
		mTitle.setText(title);
		return this;
	}

	public AdviceViewBuilder setDescription(String description)
	{
		mDescription.setText(Html.fromHtml(description));
		return this;
	}

	public View build()
	{
		return mMainView;
	}
}

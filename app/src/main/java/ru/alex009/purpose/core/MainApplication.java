package ru.alex009.purpose.core;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Application;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;

import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;

import ru.alex009.purpose.receivers.SyncAlarm;
import ru.alex009.purpose.services.AdvicesService;
import ru.alex009.purpose.services.NotificationsService;
import ru.alex009.purpose.services.RemindersService;
import ru.alex009.purpose.sync.SyncAdapter;

/**
 * Created by AlekseyMikhailov on 16.11.14.
 */
public class MainApplication extends Application implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
{
	private static MainApplication mInstance;

	public static MainApplication getInstance()
	{
		return mInstance;
	}

	public static int GOOGLE_API_LOGIN_REQUEST_CODE = 9000;

	public static final long SECONDS_PER_MINUTE = 60L;
	public static final long SYNC_INTERVAL_IN_MINUTES = 60L;
	public static final long SYNC_INTERVAL = SYNC_INTERVAL_IN_MINUTES * SECONDS_PER_MINUTE;

	private GoogleApiClient mGoogleApiClient;
	private Activity mLoginRequestActivity;
	private LoginListener mLoginListener;

	@Override
	public void onCreate()
	{
		super.onCreate();

		mInstance = this;

		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addApi(Plus.API)
				.addScope(Plus.SCOPE_PLUS_LOGIN)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.build();

		connectGoogleApiClient(null);

		startService(new Intent(this, NotificationsService.class));
		startService(new Intent(this, RemindersService.class));
		startService(new Intent(this, AdvicesService.class));
	}

	@Override
	public void onConnected(Bundle bundle)
	{
		if(mLoginListener != null) mLoginListener.onConnected(bundle);

		SyncAlarm.setAlarm(this, 60*1000);
	}

	@Override
	public void onConnectionSuspended(int i)
	{

	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult)
	{
		if(mLoginRequestActivity == null) return;

		if (connectionResult.hasResolution()) {
			try {
				connectionResult.startResolutionForResult(mLoginRequestActivity, GOOGLE_API_LOGIN_REQUEST_CODE);
			} catch (IntentSender.SendIntentException e) {
				mGoogleApiClient.connect();
			}
		}
	}

	public void connectGoogleApiClient(Activity requestActivity)
	{
		mLoginRequestActivity = requestActivity;

		mGoogleApiClient.connect();
	}

	public void disconnectGoogleApiClient()
	{
		if(mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();

			if(mLoginListener != null) mLoginListener.onDisconnected();
		}
	}

	public Account getGoogleAccount()
	{
		if(!mGoogleApiClient.isConnected()) return null;

		String accountName = Plus.AccountApi.getAccountName(mGoogleApiClient);

		final AccountManager manager = AccountManager.get(this);
		Account[] accounts = manager.getAccountsByType(GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE);

		for (Account account : accounts) {
			if (account.name.equals(accountName)) {
				return account;
			}
		}

		return null;
	}

    public void requestSync()
    {
        Account account = getGoogleAccount();

        if(account != null && ContentResolver.getSyncAutomatically(account, SyncAdapter.AUTHORITY)) {
            ContentResolver.setIsSyncable(account, SyncAdapter.AUTHORITY, 1);
            ContentResolver.requestSync(account, SyncAdapter.AUTHORITY, Bundle.EMPTY);
            ContentResolver.addPeriodicSync(getGoogleAccount(), SyncAdapter.AUTHORITY, Bundle.EMPTY, SYNC_INTERVAL);
        }
    }

	public GoogleApiClient getGoogleApiClient()
	{
		return mGoogleApiClient;
	}

	public void setLoginListener(LoginListener listener)
	{
		mLoginListener = listener;
	}

	public interface LoginListener
	{
		public void onConnected(Bundle bundle);
		public void onDisconnected();
	}
}

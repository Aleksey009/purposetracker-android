package ru.alex009.purpose.core;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by AlekseyMikhailov on 16.11.14.
 */

/**
 * Набор статических функций выполняющих загрузку данных с сети
 */
public class NetworkConnector
{
	private static String convertStreamToString(InputStream is)
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	/**
	 * Загружает данные по ссылке и возвращает их в виде JSON объекта
	 *
	 * @param url адрес по которому находятся JSON данные
	 * @return загруженные данные
	 */
	public static JSONObject getJSONFromURL(String url)
	{
		HttpClient client = new DefaultHttpClient();
		HttpGet request = new HttpGet(url);

		try {
			HttpResponse response = client.execute(request);
			HttpEntity entity = response.getEntity();

			if (entity != null) {
				InputStream stream = entity.getContent();
				String result = convertStreamToString(stream);

				return new JSONObject(result);
			}
		} catch (Exception e) {
			Log.e("DEV", "exc: " + e.toString());
		}

		return null;
	}

	/**
	 * Загружает изображение по ссылке и возвращает в виде Bitmap объекта
	 * @param url адрес по которому находится изображение
	 * @return загруженное изображение
	 */
	public static Bitmap getBitmapFromURL(String url) {
		try {
			InputStream stream = new URL(url).openStream();
			return BitmapFactory.decodeStream(stream);
		} catch (Exception e) {
			Log.e("DEV", "getBitmapFromURL exc: " + e.toString());
		}
		return null;
	}
}

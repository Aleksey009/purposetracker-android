package ru.alex009.purpose.core;

/**
 * Created by AlekseyMikhailov on 22.11.14.
 */
public class Notifications
{
	public static final int NOTIFICATION_NORMAL = 1;
	public static final int NOTIFICATION_ADVICE = 2;
}
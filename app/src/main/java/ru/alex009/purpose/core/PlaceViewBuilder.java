package ru.alex009.purpose.core;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import ru.alex009.purpose.R;

/**
 * Created by AlekseyMikhailov on 01.12.14.
 */
public class PlaceViewBuilder
{
	private static final int ACTION_FIND = 1;

	private Context mContext;
	private View mMainView;
	private TextView mName;
	private ImageButton mFindBtn;
	private ImageButton mDeleteBtn;

	private static class PlaceData
	{
		public LatLng position;
		public String name;
	}

	public static PlaceViewBuilder init(Activity activity, ViewGroup parent)
	{
		return new PlaceViewBuilder(activity, parent);
	}

	private PlaceViewBuilder(Activity activity, ViewGroup parent)
	{
		mContext = activity;

		mMainView = activity.getLayoutInflater().inflate(R.layout.view_place, parent, false);
		mName = (TextView) mMainView.findViewById(R.id.place_name);
		mFindBtn = (ImageButton) mMainView.findViewById(R.id.place_find);
		mDeleteBtn = (ImageButton) mMainView.findViewById(R.id.place_delete);

		mName.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/Roboto-Regular.ttf"));

		mDeleteBtn.setVisibility(View.GONE);

		mFindBtn.setTag(R.id.action, ACTION_FIND);

		mDeleteBtn.setTag(mMainView);

		mFindBtn.setOnClickListener(mListener);
	}

	public PlaceViewBuilder setPlaceInfo(String name, LatLng position)
	{
		PlaceData data = new PlaceData();
		data.position = position;
		data.name = name;

		mName.setText(name);
		mFindBtn.setTag(R.id.data, data);
		return this;
	}

	public PlaceViewBuilder setDeleteClickListener(View.OnClickListener listener)
	{
		mDeleteBtn.setOnClickListener(listener);
		mDeleteBtn.setVisibility(View.VISIBLE);

		mFindBtn.setVisibility(View.GONE);
		return this;
	}

	public View build()
	{
		return mMainView;
	}

	private static View.OnClickListener mListener = new View.OnClickListener()
	{
		@Override
		public void onClick(View view)
		{
			Integer action = (Integer) view.getTag(R.id.action);
			PlaceData data = (PlaceData) view.getTag(R.id.data);

			switch (action) {
				case ACTION_FIND: {
					LatLng position = data.position;
					String name = data.name;

					Uri uri = Uri.parse("geo:" + position.latitude + "," + position.longitude +
							"?q=" + position.latitude + "," + position.longitude +
							" (" + name + ")");
					Intent mapIntent = new Intent(Intent.ACTION_VIEW, uri);
					view.getContext().startActivity(mapIntent);
					break;
				}
			}
		}
	};
}

package ru.alex009.purpose.core;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.ContactsContract;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import ru.alex009.purpose.R;

/**
 * Created by AlekseyMikhailov on 01.12.14.
 */
public class ContactViewBuilder
{
	private static final int ACTION_CALL = 1;
	private static final int ACTION_SMS = 2;
	private static final int ACTION_EMAIL = 3;

	private Context mContext;
	private View mMainView;
	private TextView mName;
	private ImageButton mCallBtn;
	private ImageButton mSmsBtn;
	private ImageButton mEmailBtn;
	private ImageButton mDeleteBtn;

	public static ContactViewBuilder init(Activity activity, ViewGroup parent)
	{
		return new ContactViewBuilder(activity, parent);
	}

	private ContactViewBuilder(Activity activity, ViewGroup parent)
	{
		mContext = activity;

		mMainView = activity.getLayoutInflater().inflate(R.layout.view_contact, parent, false);
		mName = (TextView) mMainView.findViewById(R.id.contact_name);
		mCallBtn = (ImageButton) mMainView.findViewById(R.id.contact_call);
		mSmsBtn = (ImageButton) mMainView.findViewById(R.id.contact_sms);
		mEmailBtn = (ImageButton) mMainView.findViewById(R.id.contact_email);
		mDeleteBtn = (ImageButton) mMainView.findViewById(R.id.contact_delete);

		mName.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/Roboto-Regular.ttf"));

		mDeleteBtn.setVisibility(View.GONE);

		mCallBtn.setTag(R.id.action, ACTION_CALL);
		mSmsBtn.setTag(R.id.action, ACTION_SMS);
		mEmailBtn.setTag(R.id.action, ACTION_EMAIL);

		mDeleteBtn.setTag(mMainView);

		mCallBtn.setOnClickListener(mListener);
		mSmsBtn.setOnClickListener(mListener);
		mEmailBtn.setOnClickListener(mListener);
	}

	public ContactViewBuilder setContactUri(Uri contactUrl)
	{
		String displayName = null;
		ArrayList<String> phoneNumbers = new ArrayList<String>();
		ArrayList<String> emails = new ArrayList<String>();

		ContentResolver cr = mContext.getContentResolver();
		Cursor cursor = cr.query(contactUrl, null, null, null, null);

		int columnId = cursor.getColumnIndex(ContactsContract.Contacts._ID);
		int columnHasPhone = cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
		int columnDisplayName = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);

		if (cursor.moveToFirst()) {
			String contactId = cursor.getString(columnId);
			int hasPhone = cursor.getInt(columnHasPhone);
			displayName = cursor.getString(columnDisplayName);

			if (hasPhone == 1) {
				Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
						null,
						ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId,
						null, null);

				int columnNumber = phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

				while (phones.moveToNext()) {
					String number = phones.getString(columnNumber);

					if (!phoneNumbers.contains(number)) phoneNumbers.add(number);
				}

				phones.close();
			}

			Cursor email = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
					null,
					ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + contactId,
					null, null);

			int columnEmail = email.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS);

			while (email.moveToNext()) {
				String addr = email.getString(columnEmail);

				if (!emails.contains(addr)) emails.add(addr);
			}

			email.close();
		}

		cursor.close();

		if (displayName != null) {
			mName.setText(displayName);

			if (phoneNumbers.size() > 0) {
				mCallBtn.setTag(R.id.data, phoneNumbers);
				mSmsBtn.setTag(R.id.data, phoneNumbers);
			} else {
				mCallBtn.setVisibility(View.GONE);
				mSmsBtn.setVisibility(View.GONE);
			}

			if (emails.size() > 0) {
				mEmailBtn.setTag(R.id.data, emails);
			} else {
				mEmailBtn.setVisibility(View.GONE);
			}
		}

		return this;
	}

	public ContactViewBuilder setDeleteClickListener(View.OnClickListener listener)
	{
		mDeleteBtn.setOnClickListener(listener);
		mDeleteBtn.setVisibility(View.VISIBLE);

		mCallBtn.setVisibility(View.GONE);
		mSmsBtn.setVisibility(View.GONE);
		mEmailBtn.setVisibility(View.GONE);
		return this;
	}

	public View build()
	{
		return mMainView;
	}

	private static View.OnClickListener mListener = new View.OnClickListener()
	{
		@Override
		public void onClick(View view)
		{
			Integer action = (Integer) view.getTag(R.id.action);

			ArrayList<String> items = (ArrayList<String>) view.getTag(R.id.data);
			if (items.size() == 1) {

				switch (action) {
					case ACTION_CALL: {
						Intent callIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + items.get(0)));
						view.getContext().startActivity(callIntent);
						break;
					}
					case ACTION_SMS: {
						Intent sendIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + items.get(0)));
						view.getContext().startActivity(sendIntent);
						break;
					}
					case ACTION_EMAIL: {
						Intent sendIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + items.get(0)));
						view.getContext().startActivity(sendIntent);
						break;
					}
				}
			} else {
				String[] itemsArray = items.toArray(new String[items.size()]);
				new AlertDialog.Builder(view.getContext())
						.setItems(itemsArray, new DialogList(view.getContext(), items, action))
						.setCancelable(true)
						.show();
			}
		}
	};

	private static class DialogList implements DialogInterface.OnClickListener
	{
		private Context mContext;
		private ArrayList<String> mItemsList;
		private int mAction;

		public DialogList(Context ctx, ArrayList<String> items, int action)
		{
			mContext = ctx;
			mItemsList = items;
			mAction = action;
		}

		@Override
		public void onClick(DialogInterface dialog, int which)
		{
			Intent intent = null;

			switch (mAction) {
				case ACTION_CALL: {
					intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + mItemsList.get(which)));
					break;
				}
				case ACTION_SMS: {
					intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + mItemsList.get(which)));
					break;
				}
				case ACTION_EMAIL: {
					intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + mItemsList.get(which)));
					break;
				}
			}
			mContext.startActivity(intent);
		}
	}
}

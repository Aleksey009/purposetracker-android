package ru.alex009.purpose.core;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * Created by AlekseyMikhailov on 16.11.14.
 */
public class DataBaseConnector extends SQLiteOpenHelper
{
	private static final String DATABASE_NAME = "main.db";
	private static final int DATABASE_VERSION = 1;
	private static DataBaseConnector mInstance;
	protected Context mContext;

	public DataBaseConnector(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);

		mContext = context;
	}

	public static DataBaseConnector getInstance()
	{
		if (mInstance == null) {
			Context ctx = MainApplication.getInstance().getBaseContext();
			mInstance = new DataBaseConnector(ctx);
		}
		return mInstance;
	}

	public static void clearInstance()
	{
		mInstance = null;
	}

	@Override
	public void onCreate(SQLiteDatabase sqLiteDatabase)
	{
		executeSQLScript(sqLiteDatabase, "db/create_db_" + DATABASE_VERSION + ".sql");
	}

	@Override
	public void onUpgrade(SQLiteDatabase sqLiteDatabase, int old_version, int new_version)
	{
		for (int i = old_version + 1; i <= new_version; i++) {
			executeSQLScript(sqLiteDatabase, "db/update_db_" + i + ".sql");
		}
	}

	private void executeSQLScript(SQLiteDatabase db, String scriptFile)
	{
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		byte buf[] = new byte[1024];
		int len;

		AssetManager assetManager = mContext.getAssets();
		InputStream inputStream = null;

		try {
			inputStream = assetManager.open(scriptFile);

			while ((len = inputStream.read(buf)) != -1)
				outputStream.write(buf, 0, len);

			outputStream.close();
			inputStream.close();

			String[] createScript = outputStream.toString().split("#end#");
			for (int i = 0; i < createScript.length; i++) {
				String sqlStatement = createScript[i].trim();

				if (sqlStatement.length() > 0) {
					db.execSQL(sqlStatement);
				}
			}
		} catch (Exception e) {
			Log.e("DEV", this.toString() + " exception: " + e.toString());
		}
	}
}

package ru.alex009.purpose.core;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.Calendar;

import ru.alex009.purpose.activities.PurposeCompleteActivity;
import ru.alex009.purpose.receivers.SyncAlarm;
import ru.alex009.purpose.services.AdvicesService;
import ru.alex009.purpose.services.NotificationsService;
import ru.alex009.purpose.services.RemindersService;

/**
 * Created by AlekseyMikhailov on 30.11.14.
 */
public class DataBaseEvents
{
	public static final int TABLE_PURPOSE_ID = 0;
	public static final int TABLE_TASK_ID = 1;

	public static void onRowCreated(int tableId, int rowId, ContentValues contentValues)
	{
		MainApplication app = MainApplication.getInstance();
		switch (tableId) {
			case TABLE_PURPOSE_ID: {
				app.startService(new Intent(app, AdvicesService.class));
				break;
			}
			case TABLE_TASK_ID: {
				app.startService(new Intent(app, NotificationsService.class));
				app.startService(new Intent(app, RemindersService.class));
				app.startService(new Intent(app, AdvicesService.class));
				break;
			}
		}

		SyncAlarm.setAlarm(app, 60*1000);
	}

	public static void onRowEdited(int tableId, int rowId, ContentValues contentValues)
	{
		MainApplication app = MainApplication.getInstance();
		SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();

		switch (tableId) {
			case TABLE_PURPOSE_ID: {
				db.delete("Advice", "id IN (SELECT advice_id FROM Purpose_Advice WHERE purpose_id = ?)", new String[]{String.valueOf(rowId)});
				db.delete("Purpose_Advice", "purpose_id = ?", new String[]{String.valueOf(rowId)});

				app.startService(new Intent(app, AdvicesService.class));
				break;
			}
			case TABLE_TASK_ID: {
				db.delete("Advice", "id IN (SELECT advice_id FROM Task_Advice WHERE task_id = ?)", new String[]{String.valueOf(rowId)});
				db.delete("Task_Advice", "task_id = ?", new String[]{String.valueOf(rowId)});

				if (contentValues.containsKey(QueryBuilder.FIELD_TASK_PURPOSE_ID)) {
					int purposeId = contentValues.getAsInteger(QueryBuilder.FIELD_TASK_PURPOSE_ID);

					checkPurposeCompleting(purposeId);
				}

				app.startService(new Intent(app, NotificationsService.class));
				app.startService(new Intent(app, RemindersService.class));
				app.startService(new Intent(app, AdvicesService.class));
				break;
			}
		}

		SyncAlarm.setAlarm(app, 60*1000);
	}

	public static void onRowDeleted(int tableId, int rowId, ContentValues contentValues)
	{
		MainApplication app = MainApplication.getInstance();
		switch (tableId) {
			case TABLE_TASK_ID: {
				if (contentValues.containsKey(QueryBuilder.FIELD_TASK_PURPOSE_ID)) {
					int purposeId = contentValues.getAsInteger(QueryBuilder.FIELD_TASK_PURPOSE_ID);

					checkPurposeCompleting(purposeId);
				}

				app.startService(new Intent(app, NotificationsService.class));
				app.startService(new Intent(app, RemindersService.class));
				break;
			}
		}

		SyncAlarm.setAlarm(app, 60*1000);
	}

	private static void checkPurposeCompleting(int purposeId)
	{
		MainApplication app = MainApplication.getInstance();
		SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();

		Cursor cursor = db.rawQuery(QueryBuilder.buildPurposeInfoQuery(purposeId), null);

		int columnProgress = cursor.getColumnIndex(QueryBuilder.FIELD_PURPOSELIST_PROGRESS);

		if (cursor.moveToFirst()) {
			float progress = cursor.getFloat(columnProgress);

			if (progress > 99.0f) {
				ContentValues cv = new ContentValues();
				cv.put(QueryBuilder.FIELD_PURPOSE_DATE, Calendar.getInstance().getTimeInMillis());
				db.update("Purpose", cv, "id = ?", new String[]{String.valueOf(purposeId)});

				Intent intent = new Intent(app, PurposeCompleteActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.putExtra("purposeId", purposeId);
				app.startActivity(intent);
			}
		}
	}
}

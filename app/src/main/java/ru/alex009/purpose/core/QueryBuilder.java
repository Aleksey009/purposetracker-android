package ru.alex009.purpose.core;

/**
 * Created by AlekseyMikhailov on 16.11.14.
 */
public class QueryBuilder
{
	public static final String FIELD_PURPOSE_ID = "id";
	public static final String FIELD_PURPOSE_TEXT = "text";
	public static final String FIELD_PURPOSE_DATE = "date";
	public static final String FIELD_PURPOSE_COLOR_ID = "color_id";
	public static final String FIELD_PURPOSE_NEED_ADVICES = "need_advices";
	public static final String FIELD_PURPOSE_IMPORTANT = "important";
	public static final String FIELD_PURPOSE_CREATED = "created";
	public static final String FIELD_PURPOSE_UPDATED = "updated";
	public static final String FIELD_PURPOSE_REMOTE_ID = "remote_id";

	public static final String FIELD_PURPOSELIST_REMOTE_ID = "remote_id";
	public static final String FIELD_PURPOSELIST_ID = "id";
	public static final String FIELD_PURPOSELIST_TEXT = "text";
	public static final String FIELD_PURPOSELIST_DATE = "date";
	public static final String FIELD_PURPOSELIST_NEED_ADVICES = "need_advices";
	public static final String FIELD_PURPOSELIST_ADVICES_COUNT = "advices_count";
	public static final String FIELD_PURPOSELIST_PROGRESS = "progress";
	public static final String FIELD_PURPOSELIST_COLOR = "color";
	public static final String FIELD_PURPOSELIST_COLOR_SORTING = "color_sorting";
	public static final String FIELD_PURPOSELIST_IMPORTANT = "important";

	public static final String FIELD_TASK_ID = "id";
	public static final String FIELD_TASK_PURPOSE_ID = "purpose_id";
	public static final String FIELD_TASK_TEXT = "text";
	public static final String FIELD_TASK_DATE = "date";
	public static final String FIELD_TASK_COMPLETED = "completed";
	public static final String FIELD_TASK_NEED_ADVICES = "need_advices";
	public static final String FIELD_TASK_COLOR_ID = "color_id";
	public static final String FIELD_TASK_REMINDER_DAYS = "reminder_days";
	public static final String FIELD_TASK_REMINDER_TIME = "reminder_time";
	public static final String FIELD_TASK_IMPORTANT = "important";
	public static final String FIELD_TASK_CREATED = "created";
	public static final String FIELD_TASK_UPDATED = "updated";
	public static final String FIELD_TASK_REMOTE_ID = "remote_id";

	public static final String FIELD_TASKLIST_REMOTE_ID = "remote_id";
	public static final String FIELD_TASKLIST_ID = "id";
	public static final String FIELD_TASKLIST_PURPOSE_ID = "purpose_id";
	public static final String FIELD_TASKLIST_TEXT = "text";
	public static final String FIELD_TASKLIST_DATE = "date";
	public static final String FIELD_TASKLIST_COMPLETED = "completed";
	public static final String FIELD_TASKLIST_NEED_ADVICES = "need_advices";
	public static final String FIELD_TASKLIST_ADVICES_COUNT = "advices_count";
	public static final String FIELD_TASKLIST_REMINDER_DAYS = "reminder_days";
	public static final String FIELD_TASKLIST_REMINDER_TIME = "reminder_time";
	public static final String FIELD_TASKLIST_COLOR = "color";
	public static final String FIELD_TASKLIST_COLOR_SORTING = "color_sorting";
	public static final String FIELD_TASKLIST_IMPORTANT = "important";
	public static final String FIELD_TASKLIST_CONTACTS_COUNT = "contacts_count";
	public static final String FIELD_TASKLIST_PLACES_COUNT = "places_count";

	public static final String FIELD_COLOR_ID = "id";
	public static final String FIELD_COLOR_COLOR = "color";
	public static final String FIELD_COLOR_SORTING = "sorting";

	public static final String FIELD_ADVICESTARGETS_TYPE = "type";
	public static final String FIELD_ADVICESTARGETS_ID = "id";
	public static final String FIELD_ADVICESTARGETS_TEXT = "text";

	public static final String FIELD_PURPOSEADVICES_PURPOSE_ID = "purpose_id";
	public static final String FIELD_PURPOSEADVICES_ADVICE_ID = "advice_id";
	public static final String FIELD_PURPOSEADVICES_TITLE = "title";
	public static final String FIELD_PURPOSEADVICES_DESCRIPTION = "description";
	public static final String FIELD_PURPOSEADVICES_URL = "url";
	public static final String FIELD_PURPOSEADVICES_VISIBLE = "visible";

	public static final String FIELD_TASKADVICES_TASK_ID = "task_id";
	public static final String FIELD_TASKADVICES_ADVICE_ID = "advice_id";
	public static final String FIELD_TASKADVICES_TITLE = "title";
	public static final String FIELD_TASKADVICES_DESCRIPTION = "description";
	public static final String FIELD_TASKADVICES_URL = "url";
	public static final String FIELD_TASKADVICES_VISIBLE = "visible";

	public static final String FIELD_CONTACT_ID = "id";
	public static final String FIELD_CONTACT_TASK_ID = "task_id";
	public static final String FIELD_CONTACT_CONTACT_URI = "contact_uri";
	public static final String FIELD_CONTACT_CREATED = "created";
	public static final String FIELD_CONTACT_UPDATED = "updated";
	public static final String FIELD_CONTACT_REMOTE_ID = "remote_id";

	public static final String FIELD_PLACE_ID = "id";
	public static final String FIELD_PLACE_TASK_ID = "task_id";
	public static final String FIELD_PLACE_DESCRIPTION = "description";
	public static final String FIELD_PLACE_COORDS = "coords";
	public static final String FIELD_PLACE_CREATED = "created";
	public static final String FIELD_PLACE_UPDATED = "updated";
	public static final String FIELD_PLACE_REMOTE_ID = "remote_id";


	public static String buildPurposeQuery()
	{
		return "SELECT * FROM Purpose";
	}

	public static String buildSinglePurposeQuery(int id)
	{
		return "SELECT * FROM Purpose WHERE id = " + id;
	}

	public static String buildPurposeListQuery(String where)
	{
		UserSettings.SortingParams params = UserSettings.getSortingParams();
		int filter = UserSettings.getFilter();

		String result = "SELECT * FROM PurposeList";

		if (where != null || filter != UserSettings.FILTER_ALL) {
			result += " WHERE ";
		}

		if (where != null) {
			result += where;
		}

		switch (filter) {
			case UserSettings.FILTER_COMPLETED: {
				if (where != null) result += " AND ";
				result += FIELD_PURPOSELIST_PROGRESS + " > 99.0";
				break;
			}
			case UserSettings.FILTER_INCOMPLETED: {
				if (where != null) result += " AND ";
				result += FIELD_PURPOSELIST_PROGRESS + " < 100.0";
				break;
			}
		}

		if (!params.isEmpty()) {
			result += " ORDER BY ";

			boolean need_comma = false;

			if (params.byProgress) {
				if (need_comma) result += ",";
				result += FIELD_PURPOSELIST_PROGRESS;
				need_comma = true;
			}
			if (params.byColor) {
				if (need_comma) result += ",";
				result += FIELD_PURPOSELIST_COLOR_SORTING + " DESC";
				need_comma = true;
			}
			if (params.byImportant) {
				if (need_comma) result += ",";
				result += FIELD_PURPOSELIST_IMPORTANT;
				need_comma = true;
			}
			if (params.byDate) {
				if (need_comma) result += ",";
				result += FIELD_PURPOSELIST_DATE;
				need_comma = true;
			}
		}

		return result;
	}

	public static String buildColorQuery()
	{
		return "SELECT * FROM Color ORDER BY sorting";
	}

	public static String buildSingleColorQuery(int colorId)
	{
		return "SELECT * FROM Color WHERE id = " + colorId;
	}

	public static String buildTaskQuery()
	{
		return "SELECT * FROM Task";
	}

	public static String buildTaskQuery(int purposeId)
	{
		return "SELECT * FROM Task WHERE purpose_id = " + purposeId;
	}

	public static String buildTaskQuery(boolean completed)
	{
		return "SELECT * FROM Task WHERE completed = " + (completed ? 1 : 0);
	}

	public static String buildSingleTaskQuery(int id)
	{
		return "SELECT * FROM Task WHERE id = " + id;
	}

	public static String buildTaskListQuery(String where)
	{
		UserSettings.SortingParams params = UserSettings.getSortingParams();
		int filter = UserSettings.getFilter();

		String result = "SELECT * FROM TaskList";

		if (where != null || filter != UserSettings.FILTER_ALL) {
			result += " WHERE ";
		}

		if (where != null) {
			result += where;
		}

		switch (filter) {
			case UserSettings.FILTER_COMPLETED: {
				if (where != null) result += " AND ";
				result += FIELD_TASKLIST_COMPLETED + " = 1";
				break;
			}
			case UserSettings.FILTER_INCOMPLETED: {
				if (where != null) result += " AND ";
				result += FIELD_TASKLIST_COMPLETED + " = 0";
				break;
			}
		}

		if (!params.isEmpty()) {
			result += " ORDER BY ";

			boolean need_comma = false;

			if (params.byProgress) {
				if (need_comma) result += ",";
				result += FIELD_TASKLIST_COMPLETED;
				need_comma = true;
			}
			if (params.byColor) {
				if (need_comma) result += ",";
				result += FIELD_TASKLIST_COLOR_SORTING + " DESC";
				need_comma = true;
			}
			if (params.byImportant) {
				if (need_comma) result += ",";
				result += FIELD_TASKLIST_IMPORTANT + " DESC";
				need_comma = true;
			}
			if (params.byDate) {
				if (need_comma) result += ",";
				result += FIELD_TASKLIST_DATE;
				need_comma = true;
			}
		}

		return result;
	}

	public static String buildAdvicesTargetsQuery()
	{
		return "SELECT * FROM AdvicesTargets";
	}

	public static String buildMaxAdviceIdQuery()
	{
		return "SELECT MAX(id) FROM Advice";
	}

	public static String buildPurposeInfoQuery(int purposeId)
	{
		return "SELECT * FROM PurposeList WHERE id = " + purposeId;
	}

	public static String buildPurposeAdvicesQuery(int purposeId)
	{
		return "SELECT * FROM PurposeAdvices WHERE purpose_id = " + purposeId;
	}

	public static String buildPurposeAdvicesQuery(int purposeId, boolean visible)
	{
		return "SELECT * FROM PurposeAdvices WHERE purpose_id = " + purposeId + " AND visible = " + (visible ? 1 : 0);
	}

	public static String buildTaskInfoQuery(int taskId)
	{
		return "SELECT * FROM TaskList WHERE id = " + taskId;
	}

	public static String buildTaskAdvicesQuery(int taskId)
	{
		return "SELECT * FROM TaskAdvices WHERE task_id = " + taskId;
	}

	public static String buildTaskAdvicesQuery(int taskId, boolean visible)
	{
		return "SELECT * FROM TaskAdvices WHERE task_id = " + taskId + " AND visible = " + (visible ? 1 : 0);
	}

	public static String buildMaxTaskIdQuery()
	{
		return "SELECT MAX(id) FROM Task";
	}

	public static String buildContactsQuery()
	{
		return "SELECT * FROM Contact";
	}

	public static String buildContactsQuery(int taskId)
	{
		return "SELECT * FROM Contact WHERE task_id = " + taskId;
	}

	public static String buildPlacesQuery()
	{
		return "SELECT * FROM Place";
	}

	public static String buildPlacesQuery(int taskId)
	{
		return "SELECT * FROM Place WHERE task_id = " + taskId;
	}
}

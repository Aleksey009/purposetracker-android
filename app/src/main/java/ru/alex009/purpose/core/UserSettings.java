package ru.alex009.purpose.core;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by AlekseyMikhailov on 25.11.14.
 */
public class UserSettings
{
	private static final String PREFS_NAME = "userSettingsFile";

	private static final String ADVICES_CHECK_PERIOD = "advices_check_period";

	private static final String NOTIFICATIONS_PERIOD = "notifications_period";

	public static final String MAIN_PAGE = "main_page";

	private static final String SORTING_PARAMS_COLOR = "sorting_params_color";
	private static final String SORTING_PARAMS_DATE = "sorting_params_date";
	private static final String SORTING_PARAMS_IMPORTANT = "sorting_params_important";
	private static final String SORTING_PARAMS_PROGRESS = "sorting_params_progress";

	private static final String FILTER_CURRENT = "filter";


	public static class SortingParams
	{
		public boolean byColor;
		public boolean byDate;
		public boolean byImportant;
		public boolean byProgress;

		public boolean isEmpty()
		{
			return !(byColor || byDate || byImportant || byProgress);
		}
	}

	public static final int FILTER_ALL = 0;
	public static final int FILTER_COMPLETED = 1;
	public static final int FILTER_INCOMPLETED = 2;

	public static final int PURPOSES_PAGE_ID = 0;
	public static final int TASKS_PAGE_ID = 1;

	public static long getAdvicesCheckPeriod()
	{
		Context ctx = MainApplication.getInstance();
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		return settings.getLong(ADVICES_CHECK_PERIOD, 60);
	}

	public static void setAdvicesCheckPeriod(long period)
	{
		Context ctx = MainApplication.getInstance();
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putLong(ADVICES_CHECK_PERIOD, period);
		editor.apply();
	}

	public static long getNotificationsPeriod()
	{
		Context ctx = MainApplication.getInstance();
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		return settings.getLong(NOTIFICATIONS_PERIOD, 30);
	}

	public static void setNotificationsPeriod(long period)
	{
		Context ctx = MainApplication.getInstance();
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putLong(NOTIFICATIONS_PERIOD, period);
		editor.apply();
	}

	public static int getMainPage()
	{
		Context ctx = MainApplication.getInstance();
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		return settings.getInt(MAIN_PAGE, PURPOSES_PAGE_ID);
	}

	public static void setMainPage(int pageId)
	{
		Context ctx = MainApplication.getInstance();
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(MAIN_PAGE, pageId);
		editor.apply();
	}

	public static SortingParams getSortingParams()
	{
		Context ctx = MainApplication.getInstance();
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

		SortingParams params = new SortingParams();

		params.byColor = settings.getBoolean(SORTING_PARAMS_COLOR, true);
		params.byDate = settings.getBoolean(SORTING_PARAMS_DATE, true);
		params.byImportant = settings.getBoolean(SORTING_PARAMS_IMPORTANT, true);
		params.byProgress = settings.getBoolean(SORTING_PARAMS_PROGRESS, true);

		return params;
	}

	public static void setSortingParams(SortingParams params)
	{
		Context ctx = MainApplication.getInstance();
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(SORTING_PARAMS_COLOR, params.byColor);
		editor.putBoolean(SORTING_PARAMS_DATE, params.byDate);
		editor.putBoolean(SORTING_PARAMS_IMPORTANT, params.byImportant);
		editor.putBoolean(SORTING_PARAMS_PROGRESS, params.byProgress);
		editor.apply();
	}

	public static int getFilter()
	{
		Context ctx = MainApplication.getInstance();
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

		return settings.getInt(FILTER_CURRENT, FILTER_ALL);
	}

	public static void setFilter(int filter)
	{
		Context ctx = MainApplication.getInstance();
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(FILTER_CURRENT, filter);
		editor.apply();
	}
}

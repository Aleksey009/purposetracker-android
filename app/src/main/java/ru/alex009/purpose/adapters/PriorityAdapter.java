package ru.alex009.purpose.adapters;

import android.content.Context;
import android.widget.ArrayAdapter;

import ru.alex009.purpose.R;

/**
 * Created by AlekseyMikhailov on 17.05.15.
 */
public class PriorityAdapter extends ArrayAdapter<String>
{
	public static final int DEFAULT_VALUE = 2;

	public PriorityAdapter(Context context)
	{
		super(context, android.R.layout.simple_selectable_list_item);

		add("Минимальный");
		add("Низкий");
		add("Средний");
		add("Высокий");
		add("Максимальный");
	}
}

package ru.alex009.purpose.adapters;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import ru.alex009.purpose.R;
import ru.alex009.purpose.activities.EditTaskActivity;
import ru.alex009.purpose.activities.TaskActivity;
import ru.alex009.purpose.core.DataBaseConnector;
import ru.alex009.purpose.core.DataBaseEvents;
import ru.alex009.purpose.core.QueryBuilder;
import ru.alex009.purpose.utils.Utils;

/**
 * Created by AlekseyMikhailov on 16.11.14.
 */
public class TaskGridAdapter extends RecyclerView.Adapter<TaskGridAdapter.TaskViewHolder> implements View.OnClickListener, PopupMenu.OnMenuItemClickListener
{
	protected Activity mActivity;
	protected String mQuery;
	protected int mPurposeId;
	protected Cursor mCursor;
	protected int mColumnId;
	protected int mColumnPurposeId;
	protected int mColumnTitle;
	protected int mColumnDate;
	protected int mColumnColor;
	protected int mColumnCompleted;
	protected int mColumnNeedAdvices;
	protected int mColumnAdvicesCount;
	protected int mColumnReminderDays;
	protected int mColumnContactsCount;
	protected int mColumnPlacesCount;
	protected int mColumnRemoteId;
	protected int mPopupMenuItemPosition;

	public TaskGridAdapter(Activity activity)
	{
		super();

		mActivity = activity;
		mPurposeId = -1;
		mQuery = QueryBuilder.buildTaskListQuery(null);
	}

	public TaskGridAdapter(Activity activity, int purposeId)
	{
		super();

		mActivity = activity;
		mPurposeId = purposeId;
		mQuery = QueryBuilder.buildTaskListQuery(QueryBuilder.FIELD_TASKLIST_PURPOSE_ID + " = " + mPurposeId);
	}

	public void reloadData()
	{
		if (mCursor != null) {
			mCursor.close();
		}

		SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();
		mCursor = db.rawQuery(mQuery, null);

		mColumnRemoteId = mCursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_REMOTE_ID);
		mColumnId = mCursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_ID);
		mColumnPurposeId = mCursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_PURPOSE_ID);
		mColumnTitle = mCursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_TEXT);
		mColumnDate = mCursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_DATE);
		mColumnColor = mCursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_COLOR);
		mColumnCompleted = mCursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_COMPLETED);
		mColumnNeedAdvices = mCursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_NEED_ADVICES);
		mColumnAdvicesCount = mCursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_ADVICES_COUNT);
		mColumnReminderDays = mCursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_REMINDER_DAYS);
		mColumnContactsCount = mCursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_CONTACTS_COUNT);
		mColumnPlacesCount = mCursor.getColumnIndex(QueryBuilder.FIELD_TASKLIST_PLACES_COUNT);

		notifyDataSetChanged();
	}

	public void updateSettings()
	{
		if (mPurposeId == -1) mQuery = QueryBuilder.buildTaskListQuery(null);
		else
			mQuery = QueryBuilder.buildTaskListQuery(QueryBuilder.FIELD_TASKLIST_PURPOSE_ID + " = " + mPurposeId);

		reloadData();
	}

	@Override
	public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
	{
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_task, parent, false);
		return new TaskViewHolder(view);
	}

	@Override
	public void onBindViewHolder(TaskViewHolder holder, int position)
	{
		if (mCursor == null) return;

		mCursor.moveToPosition(position);

		String title = mCursor.getString(mColumnTitle);
		long date = mCursor.getLong(mColumnDate);
		int color = mCursor.getInt(mColumnColor);
		boolean completed = (mCursor.getInt(mColumnCompleted) == 1);
		int needAdvices = mCursor.getInt(mColumnNeedAdvices);
		int advicesCount = mCursor.getInt(mColumnAdvicesCount);
		String reminderDays = mCursor.getString(mColumnReminderDays);
		int contactsCount = mCursor.getInt(mColumnContactsCount);
		int placesCount = mCursor.getInt(mColumnPlacesCount);

		holder.mCard.setCardBackgroundColor(color);
		holder.mHeaderTitle.setText(title);

		if (date == 0) {
			holder.mHeaderSubtitle.setVisibility(View.GONE);
		}
		else {
			holder.mHeaderSubtitle.setVisibility(View.VISIBLE);
			if (completed) {
				holder.mHeaderSubtitle.setText(R.string.completed);
			}
			else {
				holder.mHeaderSubtitle.setText(Utils.getHumanStringFromDate(new Date(date), Utils.HUMAN_STRING_MODE_SHORT));
			}
		}

		if(completed) {
			holder.mCompletedMark.setImageResource(R.drawable.ic_checkbox_marked_circle_grey600_48dp);
		}
		else {
			holder.mCompletedMark.setImageResource(R.drawable.ic_checkbox_blank_circle_outline_grey600_48dp);
		}

		if (needAdvices == 1) {
			holder.mAdvicesIndicator.setVisibility(View.VISIBLE);

			if (advicesCount > 0) {
				holder.mAdvicesIndicator.setImageResource(R.drawable.ic_lightbulb_grey600_24dp);
			}
			else {
				holder.mAdvicesIndicator.setImageResource(R.drawable.ic_lightbulb_outline_grey600_24dp);
			}
		}
		else {
			holder.mAdvicesIndicator.setVisibility(View.GONE);
		}

		if (!reminderDays.equalsIgnoreCase("0000000")) {
			holder.mRemindersIndicator.setVisibility(View.VISIBLE);
		}
		else {
			holder.mRemindersIndicator.setVisibility(View.GONE);
		}

		holder.mContactsIndicator.setVisibility(contactsCount > 0 ? View.VISIBLE : View.GONE);

		holder.mPlacesIndicator.setVisibility(placesCount > 0 ? View.VISIBLE : View.GONE);

		holder.mMenuButton.setTag(R.id.position, position);
		holder.mCard.setTag(R.id.position, position);
	}

	@Override
	public int getItemCount()
	{
		if (mCursor == null) {
			this.reloadData();
			return 0;
		}

		return mCursor.getCount();
	}

	@Override
	public void onClick(View view)
	{
		if (view.getTag(R.id.position) == null) return;

		int position = (Integer) view.getTag(R.id.position);

		switch (view.getId()) {
			case R.id.menu_btn: {
				mPopupMenuItemPosition = position;
				mCursor.moveToPosition(position);

				int completed = mCursor.getInt(mColumnCompleted);

				PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
				popupMenu.inflate(R.menu.task_popup);
				if (completed == 1) popupMenu.getMenu().removeItem(R.id.action_complete);
				popupMenu.setOnMenuItemClickListener(this);
				popupMenu.show();
				break;
			}
			case R.id.card: {
				mCursor.moveToPosition(position);

				int taskId = mCursor.getInt(mColumnId);

				Intent intent = new Intent(mActivity, TaskActivity.class);
				intent.putExtra("taskId", taskId);
				mActivity.startActivity(intent);
				break;
			}
		}
	}

	@Override
	public boolean onMenuItemClick(MenuItem item)
	{
		mCursor.moveToPosition(mPopupMenuItemPosition);
		int taskId = mCursor.getInt(mColumnId);

		switch (item.getItemId()) {
			case R.id.action_complete: {
				SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();
				ContentValues cv = new ContentValues();
				cv.put(QueryBuilder.FIELD_TASK_DATE, Calendar.getInstance().getTimeInMillis());
				cv.put(QueryBuilder.FIELD_TASK_COMPLETED, 1);
                cv.put(QueryBuilder.FIELD_TASK_UPDATED, Calendar.getInstance().getTimeInMillis());
				db.update("Task", cv, QueryBuilder.FIELD_TASK_ID + " = ?", new String[]{String.valueOf(taskId)});

				cv.put(QueryBuilder.FIELD_TASK_ID, mCursor.getInt(mColumnId));
				cv.put(QueryBuilder.FIELD_TASK_PURPOSE_ID, mCursor.getInt(mColumnPurposeId));
				cv.put(QueryBuilder.FIELD_TASK_TEXT, mCursor.getInt(mColumnTitle));
				cv.put(QueryBuilder.FIELD_TASK_DATE, mCursor.getInt(mColumnDate));
				cv.put(QueryBuilder.FIELD_TASK_COMPLETED, mCursor.getInt(mColumnCompleted));

				DataBaseEvents.onRowEdited(DataBaseEvents.TABLE_TASK_ID, taskId, cv);

				reloadData();
				break;
			}
			case R.id.action_edit: {
				Intent intent = new Intent(mActivity, EditTaskActivity.class);
				intent.putExtra("taskId", taskId);
				mActivity.startActivity(intent);
				break;
			}
			case R.id.action_delete: {
				SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();
				db.delete("Task", QueryBuilder.FIELD_TASK_ID + " = ?", new String[]{String.valueOf(taskId)});

				ContentValues cv = new ContentValues();
				cv.put(QueryBuilder.FIELD_TASK_ID, mCursor.getInt(mColumnId));
				cv.put(QueryBuilder.FIELD_TASK_PURPOSE_ID, mCursor.getInt(mColumnPurposeId));
				cv.put(QueryBuilder.FIELD_TASK_TEXT, mCursor.getInt(mColumnTitle));
				cv.put(QueryBuilder.FIELD_TASK_DATE, mCursor.getInt(mColumnDate));
				cv.put(QueryBuilder.FIELD_TASK_COMPLETED, mCursor.getInt(mColumnCompleted));
				cv.put(QueryBuilder.FIELD_TASK_REMOTE_ID, mCursor.getInt(mColumnRemoteId));

				DataBaseEvents.onRowDeleted(DataBaseEvents.TABLE_TASK_ID, taskId, cv);

				reloadData();
				break;
			}
		}
		return false;
	}

	public class TaskViewHolder extends RecyclerView.ViewHolder
	{
		public CardView mCard;
		public TextView mHeaderTitle;
		public TextView mHeaderSubtitle;
		public ImageButton mMenuButton;
		public ImageView mAdvicesIndicator;
		public ImageView mRemindersIndicator;
		public ImageView mContactsIndicator;
		public ImageView mPlacesIndicator;
		public ImageView mCompletedMark;

		public TaskViewHolder(View itemView)
		{
			super(itemView);

			mCard = (CardView) itemView.findViewById(R.id.card);
			mHeaderTitle = (TextView) itemView.findViewById(R.id.header_title);
			mHeaderSubtitle = (TextView) itemView.findViewById(R.id.header_subtitle);
			mMenuButton = (ImageButton) itemView.findViewById(R.id.menu_btn);
			mAdvicesIndicator = (ImageView) itemView.findViewById(R.id.need_advices_indicator);
			mRemindersIndicator = (ImageView) itemView.findViewById(R.id.need_reminders_indicator);
			mContactsIndicator = (ImageView) itemView.findViewById(R.id.have_contacts_indicator);
			mPlacesIndicator = (ImageView) itemView.findViewById(R.id.have_places_indicator);
			mCompletedMark = (ImageView) itemView.findViewById(R.id.completed_mark);

			mHeaderTitle.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), "fonts/Roboto-Medium.ttf"));
			mHeaderSubtitle.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), "fonts/Roboto-Regular.ttf"));

			mMenuButton.setOnClickListener(TaskGridAdapter.this);

			mCard.setOnClickListener(TaskGridAdapter.this);
		}
	}
}


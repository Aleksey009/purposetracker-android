package ru.alex009.purpose.adapters;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import ru.alex009.purpose.R;
import ru.alex009.purpose.core.DataBaseConnector;
import ru.alex009.purpose.core.QueryBuilder;

/**
 * Created by AlekseyMikhailov on 18.11.14.
 */
public class ColorAdapter extends BaseAdapter
{
	private Cursor mCursor;
	private int mColumnId;
	private int mColumnColor;
	private Context mContext;

	public ColorAdapter(Context context)
	{
		super();
		mContext = context;
	}

	public void reloadData()
	{
		if (mCursor != null) {
			mCursor.close();
		}

		SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();
		mCursor = db.rawQuery(QueryBuilder.buildColorQuery(), null);

		mColumnId = mCursor.getColumnIndex(QueryBuilder.FIELD_COLOR_ID);
		mColumnColor = mCursor.getColumnIndex(QueryBuilder.FIELD_COLOR_COLOR);

		notifyDataSetChanged();
	}

	@Override
	public int getCount()
	{
		if (mCursor == null) {
			reloadData();
			return 0;
		}

		return mCursor.getCount();
	}

	@Override
	public Object getItem(int position)
	{
		if (mCursor == null) return null;

		mCursor.moveToPosition(position);

		return mCursor.getInt(mColumnColor);
	}

	@Override
	public long getItemId(int position)
	{
		if (mCursor == null) return 0;

		mCursor.moveToPosition(position);

		return mCursor.getInt(mColumnId);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		Integer color = (Integer) getItem(position);

		if (convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(mContext);
			convertView = inflater.inflate(R.layout.view_color, parent, false);
		}

		ImageView icon = (ImageView) convertView.findViewById(R.id.colorIcon);
		icon.setColorFilter(color, PorterDuff.Mode.MULTIPLY);

		return convertView;
	}

	public int findPositionById(int id)
	{
		if(mCursor == null) return 0;

		mCursor.moveToFirst();
		int position = 0;

		do {
			int _id = mCursor.getInt(mColumnId);

			if(_id == id) {
				return position;
			}

			position++;
		}
		while (mCursor.moveToNext());

		return 0;
	}
}

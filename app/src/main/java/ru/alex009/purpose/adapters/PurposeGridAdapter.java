package ru.alex009.purpose.adapters;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.philjay.circledisplay.CircleDisplay;

import java.text.SimpleDateFormat;
import java.util.Date;

import ru.alex009.purpose.R;
import ru.alex009.purpose.activities.EditPurposeActivity;
import ru.alex009.purpose.activities.PurposeActivity;
import ru.alex009.purpose.activities.PurposeTasksActivity;
import ru.alex009.purpose.core.DataBaseConnector;
import ru.alex009.purpose.core.DataBaseEvents;
import ru.alex009.purpose.core.QueryBuilder;
import ru.alex009.purpose.utils.Utils;

/**
 * Created by AlekseyMikhailov on 16.11.14.
 */
public class PurposeGridAdapter extends RecyclerView.Adapter<PurposeGridAdapter.PurposeViewHolder> implements View.OnClickListener, PopupMenu.OnMenuItemClickListener
{
	protected Activity mActivity;
	protected Cursor mCursor;
	protected int mColumnRemoteId;
	protected int mColumnId;
	protected int mColumnTitle;
	protected int mColumnProgress;
	protected int mColumnDate;
	protected int mColumnColor;
	protected int mColumnNeedAdvices;
	protected int mColumnAdvicesCount;
	protected int mPopupMenuItemPosition;

	public PurposeGridAdapter(Activity activity)
	{
		super();

		mActivity = activity;
	}

	public void reloadData()
	{
		if (mCursor != null) {
			mCursor.close();
		}

		SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();
		mCursor = db.rawQuery(QueryBuilder.buildPurposeListQuery(null), null);

		mColumnRemoteId = mCursor.getColumnIndex(QueryBuilder.FIELD_PURPOSELIST_REMOTE_ID);
		mColumnId = mCursor.getColumnIndex(QueryBuilder.FIELD_PURPOSELIST_ID);
		mColumnTitle = mCursor.getColumnIndex(QueryBuilder.FIELD_PURPOSELIST_TEXT);
		mColumnDate = mCursor.getColumnIndex(QueryBuilder.FIELD_PURPOSELIST_DATE);
		mColumnProgress = mCursor.getColumnIndex(QueryBuilder.FIELD_PURPOSELIST_PROGRESS);
		mColumnColor = mCursor.getColumnIndex(QueryBuilder.FIELD_PURPOSELIST_COLOR);
		mColumnNeedAdvices = mCursor.getColumnIndex(QueryBuilder.FIELD_PURPOSELIST_NEED_ADVICES);
		mColumnAdvicesCount = mCursor.getColumnIndex(QueryBuilder.FIELD_PURPOSELIST_ADVICES_COUNT);

		notifyDataSetChanged();
	}

	public void updateSettings()
	{
		reloadData();
	}

	@Override
	public PurposeViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
	{
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_purpose, parent, false);
		return new PurposeViewHolder(view);
	}

	@Override
	public void onBindViewHolder(PurposeViewHolder holder, int position)
	{
		if (mCursor == null) return;

		mCursor.moveToPosition(position);

		String title = mCursor.getString(mColumnTitle);
		long date = mCursor.getLong(mColumnDate);
		float progress = mCursor.getFloat(mColumnProgress);
		int color = mCursor.getInt(mColumnColor);
		int needAdvices = mCursor.getInt(mColumnNeedAdvices);
		int advicesCount = mCursor.getInt(mColumnAdvicesCount);

		holder.mCard.setCardBackgroundColor(color);
		holder.mHeaderTitle.setText(title);
		if(progress > 99.0f) {
			holder.mHeaderProgress.setVisibility(View.GONE);

			holder.mHeaderSubtitle.setVisibility(View.VISIBLE);
			holder.mHeaderSubtitle.setText(R.string.completed);
		}
		else {
			holder.mHeaderProgress.setVisibility(View.VISIBLE);
			holder.mHeaderProgress.showValue(progress, 100.0f, false);

			if (date == 0) {
				holder.mHeaderSubtitle.setVisibility(View.GONE);
			} else {
				holder.mHeaderSubtitle.setVisibility(View.VISIBLE);
				holder.mHeaderSubtitle.setText(Utils.getHumanStringFromDate(new Date(date), Utils.HUMAN_STRING_MODE_SHORT));
			}
		}


		//holder.mTracksButton.setVisibility(View.GONE);

		if (needAdvices == 1) {
			holder.mAdvicesIndicator.setVisibility(View.VISIBLE);
			if (advicesCount > 0) {
				holder.mAdvicesIndicator.setImageResource(R.drawable.ic_lightbulb_grey600_24dp);
			}
			else {
				holder.mAdvicesIndicator.setImageResource(R.drawable.ic_lightbulb_outline_grey600_24dp);
			}
		} else {
			holder.mAdvicesIndicator.setVisibility(View.GONE);
		}

		holder.mMenuButton.setTag(R.id.position, position);
		holder.mTasksButton.setTag(R.id.position, position);
		//holder.mTracksButton.setTag(R.id.position, position);

		holder.mCard.setTag(R.id.position, position);
	}

	@Override
	public int getItemCount()
	{
		if (mCursor == null) {
			this.reloadData();
			return 0;
		}

		return mCursor.getCount();
	}

	@Override
	public void onClick(View view)
	{
		if (view.getTag(R.id.position) == null) return;

		int position = (Integer) view.getTag(R.id.position);

		switch (view.getId()) {
			case R.id.menu_btn: {
				mPopupMenuItemPosition = position;

				PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
				popupMenu.inflate(R.menu.purpose_popup);
				popupMenu.setOnMenuItemClickListener(this);
				popupMenu.show();
				break;
			}
			case R.id.tasks_button: {
				mCursor.moveToPosition(position);

				int purposeId = mCursor.getInt(mColumnId);

				Intent intent = new Intent(mActivity, PurposeTasksActivity.class);
				intent.putExtra("purposeId", purposeId);
				mActivity.startActivity(intent);
				break;
			}
			case R.id.card: {
				mCursor.moveToPosition(position);

				int purposeId = mCursor.getInt(mColumnId);

				Intent intent = new Intent(mActivity, PurposeActivity.class);
				intent.putExtra("purposeId", purposeId);
				mActivity.startActivity(intent);
				break;
			}
		}
	}

	@Override
	public boolean onMenuItemClick(MenuItem item)
	{
		mCursor.moveToPosition(mPopupMenuItemPosition);
		int purposeId = mCursor.getInt(mColumnId);

		switch (item.getItemId()) {
			case R.id.action_edit: {
				Intent intent = new Intent(mActivity, EditPurposeActivity.class);
				intent.putExtra("purposeId", purposeId);
				mActivity.startActivity(intent);
				break;
			}
			case R.id.action_delete: {
				ContentValues cv = new ContentValues();
				cv.put(QueryBuilder.FIELD_PURPOSE_ID, mCursor.getInt(mColumnId));
				cv.put(QueryBuilder.FIELD_PURPOSE_TEXT, mCursor.getInt(mColumnTitle));
				cv.put(QueryBuilder.FIELD_PURPOSE_DATE, mCursor.getInt(mColumnDate));
				cv.put(QueryBuilder.FIELD_PURPOSE_REMOTE_ID, mCursor.getInt(mColumnRemoteId));

				SQLiteDatabase db = DataBaseConnector.getInstance().getWritableDatabase();
				db.delete("Purpose", QueryBuilder.FIELD_PURPOSE_ID + " = ?", new String[]{String.valueOf(purposeId)});

				DataBaseEvents.onRowDeleted(DataBaseEvents.TABLE_PURPOSE_ID, purposeId, cv);

				reloadData();
				break;
			}
		}
		return false;
	}

	public class PurposeViewHolder extends RecyclerView.ViewHolder
	{
		public CardView mCard;
		public TextView mHeaderTitle;
		public TextView mHeaderSubtitle;
		public CircleDisplay mHeaderProgress;
		public ImageButton mTasksButton;
		public ImageView mAdvicesIndicator;
		//public TextView mTracksButton;
		public ImageButton mMenuButton;

		public PurposeViewHolder(View itemView)
		{
			super(itemView);

			mCard = (CardView) itemView.findViewById(R.id.card);
			mHeaderTitle = (TextView) itemView.findViewById(R.id.header_title);
			mHeaderSubtitle = (TextView) itemView.findViewById(R.id.header_subtitle);
			mHeaderProgress = (CircleDisplay) itemView.findViewById(R.id.header_progress);
			mTasksButton = (ImageButton) itemView.findViewById(R.id.tasks_button);
			//mTracksButton = (TextView) itemView.findViewById(R.id.tracks_button);
			mAdvicesIndicator = (ImageView) itemView.findViewById(R.id.need_advices_indicator);
			mMenuButton = (ImageButton) itemView.findViewById(R.id.menu_btn);

			mHeaderProgress.setDrawText(false);
			mHeaderProgress.setTouchEnabled(false);

			mHeaderTitle.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), "fonts/Roboto-Medium.ttf"));
			mHeaderSubtitle.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), "fonts/Roboto-Regular.ttf"));

			mMenuButton.setOnClickListener(PurposeGridAdapter.this);
			mTasksButton.setOnClickListener(PurposeGridAdapter.this);
			//mTracksButton.setOnClickListener(PurposeGridAdapter.this);

			mCard.setOnClickListener(PurposeGridAdapter.this);
		}
	}
}

package ru.alex009.purpose.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;

import ru.alex009.purpose.core.MainApplication;

/**
 * Created by AlekseyMikhailov on 14.12.14.
 */
public class SyncAlarm extends BroadcastReceiver
{
	public static void setAlarm(Context context, long after)
	{
		Intent intent = new Intent(context, SyncAlarm.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

		AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		am.set(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis() + after, pendingIntent);
	}

	@Override
	public void onReceive(Context context, Intent intent)
	{
		MainApplication.getInstance().requestSync();
	}
}
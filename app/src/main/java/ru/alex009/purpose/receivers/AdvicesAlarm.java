package ru.alex009.purpose.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ru.alex009.purpose.services.AdvicesService;

public class AdvicesAlarm extends BroadcastReceiver
{
	public static void setAlarm(Context context, long time)
	{
		Intent intent = new Intent(context, AdvicesAlarm.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

		AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		am.set(AlarmManager.RTC_WAKEUP, time, pendingIntent);
	}

	@Override
	public void onReceive(Context context, Intent intent)
	{
		Intent alarmIntent = new Intent(context, AdvicesService.class);
		context.startService(alarmIntent);
	}
}

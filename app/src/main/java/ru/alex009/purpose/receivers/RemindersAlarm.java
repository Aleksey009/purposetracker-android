package ru.alex009.purpose.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ru.alex009.purpose.services.RemindersService;

public class RemindersAlarm extends BroadcastReceiver
{
	public static void setAlarm(Context context, long time, int id)
	{
		Intent intent = new Intent(context, RemindersAlarm.class);
		intent.putExtra("id", id);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

		AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		am.set(AlarmManager.RTC_WAKEUP, time, pendingIntent);
	}

	public static void clearAlarm(Context context)
	{
		Intent intent = new Intent(context, RemindersAlarm.class);

		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

		AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		am.cancel(pendingIntent);
	}

	@Override
	public void onReceive(Context context, Intent intent)
	{
		Intent alarmIntent = new Intent(context, RemindersService.class);
		alarmIntent.putExtra("action", RemindersService.ACTION_ALARM);
		if (intent != null) alarmIntent.putExtra("id", intent.getIntExtra("id", -1));

		context.startService(alarmIntent);
	}
}

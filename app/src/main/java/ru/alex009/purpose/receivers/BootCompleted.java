package ru.alex009.purpose.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ru.alex009.purpose.services.AdvicesService;
import ru.alex009.purpose.services.NotificationsService;
import ru.alex009.purpose.services.RemindersService;

/**
 * Created by AlekseyMikhailov on 01.12.14.
 */
public class BootCompleted extends BroadcastReceiver
{

	@Override
	public void onReceive(Context context, Intent intent)
	{
		context.startService(new Intent(context, NotificationsService.class));
		context.startService(new Intent(context, RemindersService.class));
		context.startService(new Intent(context, AdvicesService.class));
	}
}

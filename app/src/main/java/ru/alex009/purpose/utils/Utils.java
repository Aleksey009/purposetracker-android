package ru.alex009.purpose.utils;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Набор полезных функций
 *
 * Created by AlekseyMikhailov on 13.08.14.
 */
public class Utils
{
	/** Режим преобразования в строку вида "5 часов 10 минут назад" */
	public static final int HUMAN_STRING_MODE_LONG = 0;
	/** Режим преобразования в строку вида "5 часов назад" */
	public static final int HUMAN_STRING_MODE_SHORT = 1;

	/** Количество миллисекунд в дне */
	private static final long MILLISECONDS_DAY = 1000 * 60 * 60 * 24;
	/** Количество миллисекунд в часе */
	private static final long MILLISECONDS_HOUR = 1000 * 60 * 60;
	/** Количество миллисекунд в минуте */
	private static final long MILLISECONDS_MINUTE = 1000 * 60;
	/** Количество миллисекунд в секунде */
	//private static final long MILLISECONDS_SECOND = 1000;

	/** Преобразователь строки в дату и обратно */
	private static SimpleDateFormat sdf;

	/**
	 * Получает склонение для числа
	 *
	 * @param number      число
	 * @param inflections массив склонений, пример: "просмотр", "просмотра", "просмотров"
	 * @return одно из склонений
	 */
	public static String getInflectionForNumber(long number, String[] inflections)
	{
		if (((number % 10) == 1) && ((number % 100) != 11)) return inflections[0];
		if (((number % 10) >= 2) && ((number % 10) <= 4) && (((number % 100) < 10) || ((number % 100) >= 20)))
			return inflections[1];
		return inflections[2];
	}

	/**
	 * Получает дату в виде человекопонятной строки
	 *
	 * @param date дата
	 * @param mode режим преобразования (HUMAN_STRING_MODE_LONG или HUMAN_STRING_MODE_SHORT)
	 * @return строка с датой
	 */
	public static String getHumanStringFromDate(Date date, int mode)
	{
		long diff = new Date().getTime() - date.getTime();
		long days = diff / MILLISECONDS_DAY;

		if (sdf == null) sdf = new SimpleDateFormat();

		if (days == 0) {
			if(diff < -MILLISECONDS_HOUR) {
				long hours = -diff / MILLISECONDS_HOUR;
				String hours_str = getInflectionForNumber(hours, new String[]{"час", "часа", "часов"});

				if (mode == HUMAN_STRING_MODE_LONG) {
					long minutes = (-diff - hours * MILLISECONDS_HOUR) / MILLISECONDS_MINUTE;
					String minutes_str = getInflectionForNumber(minutes, new String[]{"минута", "минуты", "минут"});

					return String.format("через %d %s %d %s", hours, hours_str, minutes, minutes_str);
				} else if (mode == HUMAN_STRING_MODE_SHORT) {
					return String.format("через %d %s", hours, hours_str);
				}
			}
			else if(diff < -MILLISECONDS_MINUTE * 5) {
				long minutes = -diff / MILLISECONDS_MINUTE;
				String minutes_str = getInflectionForNumber(minutes, new String[]{"минута", "минуты", "минут"});

				return String.format("через %d %s", minutes, minutes_str);
			}
			else if (diff > MILLISECONDS_HOUR) {
				long hours = diff / MILLISECONDS_HOUR;
				String hours_str = getInflectionForNumber(hours, new String[]{"час", "часа", "часов"});

				if (mode == HUMAN_STRING_MODE_LONG) {
					long minutes = (diff - hours * MILLISECONDS_HOUR) / MILLISECONDS_MINUTE;
					String minutes_str = getInflectionForNumber(minutes, new String[]{"минута", "минуты", "минут"});

					return String.format("%d %s %d %s назад", hours, hours_str, minutes, minutes_str);
				} else if (mode == HUMAN_STRING_MODE_SHORT) {
					return String.format("%d %s назад", hours, hours_str);
				}
			} else if (diff > MILLISECONDS_MINUTE * 5) {
				long minutes = diff / MILLISECONDS_MINUTE;
				String minutes_str = getInflectionForNumber(minutes, new String[]{"минута", "минуты", "минут"});

				return String.format("%d %s назад", minutes, minutes_str);
			}
			else return "только что";
		}
		else if(days < 0) {
			days *= -1;
			if(days == 1) {
				sdf.applyPattern("завтра в HH:mm");
			}
			else if(days == 2) {
				sdf.applyPattern("послезавтра в HH:mm");
			}
			else if(days < 30) {
				String inflection = getInflectionForNumber(days, new String[]{"день", "дня", "дней"});

				sdf.applyPattern(String.format("через %d %s в HH:mm", days, inflection));
			}
			else {
				sdf.applyPattern("d MMM yyyy в HH:mm");
			}
		}
		else {
			if (days == 1) {
				sdf.applyPattern("вчера в HH:mm");
			}
			else if (days == 2) {
				sdf.applyPattern("позавчера в HH:mm");
			}
			else if (days < 30) {
				String inflection = getInflectionForNumber(days, new String[]{"день", "дня", "дней"});

				sdf.applyPattern(String.format("%d %s назад в HH:mm", days, inflection));
			}
			else {
				sdf.applyPattern("d MMM yyyy в HH:mm");
			}
		}

		return sdf.format(date);
	}

	/**
	 * Получает id поста из его url
	 *
	 * @param url url поста
	 * @return id поста или 0
	 */
	public static int getPostIdFromUrl(String url)
	{
		Pattern p = Pattern.compile("([^\\/]+)(?=\\.\\w+(#[\\d\\-]+)?$)|(([\\d]+)?$)");
		Matcher m = p.matcher(url);

		if (m.find())
			return Integer.parseInt(m.group());

		return 0;
	}

	/**
	 * Получает ссылку на изображение заданного размера из json изображений
	 *
	 * @param images объект json с изображениями
	 * @param size   размер изображения (full, large, medium, thumbnail)
	 * @return url изображения
	 */
	public static String getImageFromJson(JSONObject images, String size)
	{
		String result = null;
		JSONObject image = images.optJSONObject("mk-" + size);
		if (image == null) image = images.optJSONObject(size);
		if (image != null) result = image.optString("url");
		return result;
	}
}

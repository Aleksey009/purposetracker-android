CREATE TABLE DeletedObject (
	remote_id TEXT
);
#end#
CREATE TABLE Color (
	id INTEGER PRIMARY KEY,
	color INTEGER,
	sorting INTEGER
);
#end#
CREATE TABLE Purpose (
	id INTEGER PRIMARY KEY,
	text TEXT,
	date INTEGER,
	need_advices INTEGER,
	color_id INTEGER,
	important INTEGER,
	created INTEGER,
	updated INTEGER,
	remote_id TEXT
);
#end#
CREATE TABLE Task (
	id INTEGER PRIMARY KEY,
	purpose_id INTEGER,
	text TEXT,
	date INTEGER,
	completed INTEGER,
	need_advices INTEGER,
	color_id INTEGER,
	important INTEGER,
	reminder_days TEXT,
	reminder_time INTEGER,
	created INTEGER,
	updated INTEGER,
	remote_id TEXT
);
#end#
CREATE TABLE Contact (
	id INTEGER PRIMARY KEY,
	task_id INTEGER,
	contact_uri TEXT,
	created INTEGER,
	updated INTEGER,
	remote_id TEXT
);
#end#
CREATE TABLE Place (
	id INTEGER PRIMARY KEY,
	task_id INTEGER,
	description TEXT,
	coords TEXT,
	created INTEGER,
	updated INTEGER,
	remote_id TEXT
);
#end#
CREATE TABLE Advice (
	id INTEGER PRIMARY KEY,
	title TEXT,
	description TEXT,
	url TEXT UNIQUE,
	visible INTEGER
);
#end#
CREATE TABLE Purpose_Advice (
	purpose_id INTEGER,
	advice_id INTEGER,
	PRIMARY KEY (purpose_id, advice_id)
);
#end#
CREATE TABLE Task_Advice (
	task_id INTEGER,
	advice_id INTEGER,
	PRIMARY KEY (task_id, advice_id)
);
#end#
CREATE VIEW TaskList AS
	SELECT
		t.id AS id,
		t.purpose_id AS purpose_id,
		t.text AS text,
		t.date AS date,
		t.completed AS completed,
		t.need_advices AS need_advices,
		(SELECT COUNT(*)
			FROM Task_Advice
			WHERE task_id = t.id) AS advices_count,
		t.reminder_days AS reminder_days,
		t.reminder_time AS reminder_time,
		t.important AS important,
		c.color AS color,
		c.sorting AS color_sorting,
		(SELECT COUNT(*)
			FROM Contact
			WHERE task_id = t.id) AS contacts_count,
		(SELECT COUNT(*)
			FROM Place
			WHERE task_id = t.id) AS places_count,
		t.remote_id AS remote_id
	FROM
		Task t,
		Color c
	WHERE
		t.color_id = c.id;
#end#
CREATE VIEW PurposeList AS
	SELECT
		p.id AS id,
		p.text AS text,
		p.date AS date,
		p.need_advices AS need_advices,
		(SELECT COUNT(*)
			FROM Purpose_Advice
			WHERE purpose_id = p.id) AS advices_count,
		p.important AS important,
		c.color AS color,
		c.sorting AS color_sorting,
		100.0
		*
		(SELECT COUNT(*)
			FROM Task
			WHERE purpose_id = p.id AND
				completed = 1)
			/
		(SELECT
				CASE WHEN COUNT(*) = 0
				THEN
					1
				ELSE
					COUNT(*)
				END
			FROM Task
			WHERE purpose_id = p.id) AS progress,
			p.remote_id AS remote_id
	FROM
		Purpose p,
		Color c
	WHERE
		p.color_id = c.id;
#end#
CREATE VIEW AdvicesTargets AS
	SELECT
		0 AS type,
		id AS id,
		text AS text
	FROM
		Purpose
	WHERE
		need_advices = 1
	UNION
	SELECT
		1 AS type,
		id AS id,
		text AS text
	FROM
		Task
	WHERE
		need_advices = 1 AND
		completed = 0;
#end#
CREATE VIEW PurposeAdvices AS
	SELECT
		p.id AS purpose_id,
		a.id AS advice_id,
		a.title AS title,
		a.description AS description,
		a.url AS url,
		a.visible AS visible
	FROM
		Purpose p,
		Advice a,
		Purpose_Advice pa
	WHERE
		pa.purpose_id = p.id AND pa.advice_id = a.id;
#end#
CREATE VIEW TaskAdvices AS
	SELECT
		t.id AS task_id,
		a.id AS advice_id,
		a.title AS title,
		a.description AS description,
		a.url AS url,
		a.visible AS visible
	FROM
		Task t,
		Advice a,
		Task_Advice ta
	WHERE
		ta.task_id = t.id AND ta.advice_id = a.id;
#end#
CREATE TRIGGER DeletePurposeTrigger BEFORE DELETE ON Purpose FOR EACH ROW
BEGIN
	INSERT INTO DeletedObject (remote_id) VALUES (OLD.remote_id);
	DELETE FROM Task WHERE purpose_id = OLD.id;
	DELETE FROM Advice WHERE id IN (SELECT advice_id FROM Purpose_Advice WHERE purpose_id = OLD.id);
	DELETE FROM Purpose_Advice WHERE purpose_id = OLD.id;
END;
#end#
CREATE TRIGGER DeleteTaskTrigger BEFORE DELETE ON Task FOR EACH ROW
BEGIN
	INSERT INTO DeletedObject (remote_id) VALUES (OLD.remote_id);
	DELETE FROM Contact WHERE task_id = OLD.id;
	DELETE FROM Place WHERE task_id = OLD.id;
	DELETE FROM Advice WHERE id IN (SELECT advice_id FROM Task_Advice WHERE task_id = OLD.id);
	DELETE FROM Task_Advice WHERE task_id = OLD.id;
END;
#end#
CREATE TRIGGER DeleteContactTrigger BEFORE DELETE ON Contact FOR EACH ROW
BEGIN
	INSERT INTO DeletedObject (remote_id) VALUES (OLD.remote_id);
END;
#end#
CREATE TRIGGER DeletePlaceTrigger BEFORE DELETE ON Place FOR EACH ROW
BEGIN
	INSERT INTO DeletedObject (remote_id) VALUES (OLD.remote_id);
END;
#end#
INSERT INTO Color (id, color, sorting) VALUES (0, 4294967295, 0);
#end#
INSERT INTO Color (id, color, sorting) VALUES (1, 4294929727, 1);
#end#
INSERT INTO Color (id, color, sorting) VALUES (2, 4294941440, 2);
#end#
INSERT INTO Color (id, color, sorting) VALUES (3, 4294957568, 3);
#end#
INSERT INTO Color (id, color, sorting) VALUES (4, 4288009793, 4);
#end#
INSERT INTO Color (id, color, sorting) VALUES (5, 4280084661, 5);
#end#
INSERT INTO Color (id, color, sorting) VALUES (6, 4282369023, 6);
#end#
INSERT INTO Color (id, color, sorting) VALUES (7, 4290299081, 7);
#end#

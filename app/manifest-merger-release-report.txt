-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
	package
		ADDED from AndroidManifest.xml:3:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionCode
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-permission#android.permission.RECEIVE_BOOT_COMPLETED
ADDED from AndroidManifest.xml:5:5
	android:name
		ADDED from AndroidManifest.xml:5:22
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:7:5
	android:name
		ADDED from AndroidManifest.xml:7:22
uses-permission#android.permission.ACCESS_NETWORK_STATE
ADDED from AndroidManifest.xml:8:5
	android:name
		ADDED from AndroidManifest.xml:8:22
uses-permission#android.permission.READ_CONTACTS
ADDED from AndroidManifest.xml:10:5
	android:name
		ADDED from AndroidManifest.xml:10:22
uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:12:5
	android:name
		ADDED from AndroidManifest.xml:12:22
uses-permission#android.permission.ACCESS_COARSE_LOCATION
ADDED from AndroidManifest.xml:13:5
	android:name
		ADDED from AndroidManifest.xml:13:22
uses-permission#android.permission.ACCESS_FINE_LOCATION
ADDED from AndroidManifest.xml:14:5
	android:name
		ADDED from AndroidManifest.xml:14:22
uses-permission#android.permission.GET_ACCOUNTS
ADDED from AndroidManifest.xml:16:5
	android:name
		ADDED from AndroidManifest.xml:16:22
uses-permission#android.permission.USE_CREDENTIALS
ADDED from AndroidManifest.xml:17:5
	android:name
		ADDED from AndroidManifest.xml:17:22
uses-permission#android.permission.READ_SYNC_STATS
ADDED from AndroidManifest.xml:19:5
	android:name
		ADDED from AndroidManifest.xml:19:22
uses-permission#android.permission.READ_SYNC_SETTINGS
ADDED from AndroidManifest.xml:20:5
	android:name
		ADDED from AndroidManifest.xml:20:22
uses-permission#android.permission.WRITE_SYNC_SETTINGS
ADDED from AndroidManifest.xml:21:5
	android:name
		ADDED from AndroidManifest.xml:21:22
uses-feature#0x00020000
ADDED from AndroidManifest.xml:23:5
	android:required
		ADDED from AndroidManifest.xml:25:9
	android:glEsVersion
		ADDED from AndroidManifest.xml:24:9
application
ADDED from AndroidManifest.xml:27:5
MERGED from com.android.support:support-v4:21.0.2:16:5
MERGED from com.android.support:appcompat-v7:21.0.2:16:5
MERGED from com.android.support:support-v4:21.0.2:16:5
MERGED from com.android.support:cardview-v7:21.0.2:16:5
MERGED from com.android.support:recyclerview-v7:21.0.2:17:5
MERGED from com.android.support:support-v4:21.0.2:16:5
MERGED from com.getbase:floatingactionbutton:1.1.0:12:5
MERGED from com.google.android.gms:play-services:6.1.71:16:5
MERGED from com.android.support:support-v4:21.0.2:16:5
	android:label
		ADDED from AndroidManifest.xml:31:9
	android:allowBackup
		ADDED from AndroidManifest.xml:29:9
	android:icon
		ADDED from AndroidManifest.xml:30:9
	android:theme
		ADDED from AndroidManifest.xml:32:9
	android:name
		ADDED from AndroidManifest.xml:28:9
meta-data#com.google.android.gms.version
ADDED from AndroidManifest.xml:33:9
	android:name
		ADDED from AndroidManifest.xml:34:13
	android:value
		ADDED from AndroidManifest.xml:35:13
meta-data#com.google.android.maps.v2.API_KEY
ADDED from AndroidManifest.xml:36:9
	android:name
		ADDED from AndroidManifest.xml:37:13
	android:value
		ADDED from AndroidManifest.xml:38:13
activity#ru.alex009.purpose.activities.MainActivity
ADDED from AndroidManifest.xml:40:9
	android:label
		ADDED from AndroidManifest.xml:42:13
	android:theme
		ADDED from AndroidManifest.xml:43:13
	android:name
		ADDED from AndroidManifest.xml:41:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:44:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:45:17
	android:name
		ADDED from AndroidManifest.xml:45:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:47:17
	android:name
		ADDED from AndroidManifest.xml:47:27
activity#ru.alex009.purpose.activities.PurposeTasksActivity
ADDED from AndroidManifest.xml:50:9
	android:label
		ADDED from AndroidManifest.xml:52:13
	android:name
		ADDED from AndroidManifest.xml:51:13
activity#ru.alex009.purpose.activities.EditPurposeActivity
ADDED from AndroidManifest.xml:53:9
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:56:13
	android:label
		ADDED from AndroidManifest.xml:55:13
	android:name
		ADDED from AndroidManifest.xml:54:13
activity#ru.alex009.purpose.activities.EditTaskActivity
ADDED from AndroidManifest.xml:57:9
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:60:13
	android:label
		ADDED from AndroidManifest.xml:59:13
	android:name
		ADDED from AndroidManifest.xml:58:13
activity#ru.alex009.purpose.activities.ReminderActivity
ADDED from AndroidManifest.xml:61:9
	android:noHistory
		ADDED from AndroidManifest.xml:64:13
	android:label
		ADDED from AndroidManifest.xml:63:13
	android:name
		ADDED from AndroidManifest.xml:62:13
activity#ru.alex009.purpose.activities.SettingsActivity
ADDED from AndroidManifest.xml:65:9
	android:label
		ADDED from AndroidManifest.xml:67:13
	android:name
		ADDED from AndroidManifest.xml:66:13
activity#ru.alex009.purpose.activities.HelpActivity
ADDED from AndroidManifest.xml:68:9
	android:label
		ADDED from AndroidManifest.xml:70:13
	android:name
		ADDED from AndroidManifest.xml:69:13
activity#ru.alex009.purpose.activities.PurposeActivity
ADDED from AndroidManifest.xml:71:9
	android:theme
		ADDED from AndroidManifest.xml:73:13
	android:name
		ADDED from AndroidManifest.xml:72:13
activity#ru.alex009.purpose.activities.TaskActivity
ADDED from AndroidManifest.xml:74:9
	android:theme
		ADDED from AndroidManifest.xml:76:13
	android:name
		ADDED from AndroidManifest.xml:75:13
activity#ru.alex009.purpose.activities.ColorSortingActivity
ADDED from AndroidManifest.xml:77:9
	android:theme
		ADDED from AndroidManifest.xml:79:13
	android:name
		ADDED from AndroidManifest.xml:78:13
activity#ru.alex009.purpose.activities.PurposeCompleteActivity
ADDED from AndroidManifest.xml:80:9
	android:noHistory
		ADDED from AndroidManifest.xml:82:13
	android:theme
		ADDED from AndroidManifest.xml:83:13
	android:name
		ADDED from AndroidManifest.xml:81:13
activity#ru.alex009.purpose.activities.SelectMapLocationActivity
ADDED from AndroidManifest.xml:84:9
	android:label
		ADDED from AndroidManifest.xml:86:13
	android:name
		ADDED from AndroidManifest.xml:85:13
activity#ru.alex009.crossgrid.activity.GameActivity
ADDED from AndroidManifest.xml:87:9
	android:noHistory
		ADDED from AndroidManifest.xml:90:13
	android:label
		ADDED from AndroidManifest.xml:89:13
	android:name
		ADDED from AndroidManifest.xml:88:13
service#ru.alex009.purpose.services.NotificationsService
ADDED from AndroidManifest.xml:92:9
	android:name
		ADDED from AndroidManifest.xml:92:18
service#ru.alex009.purpose.services.RemindersService
ADDED from AndroidManifest.xml:93:9
	android:name
		ADDED from AndroidManifest.xml:93:18
service#ru.alex009.purpose.services.AdvicesService
ADDED from AndroidManifest.xml:94:9
	android:name
		ADDED from AndroidManifest.xml:94:18
service#com.backendless.AndroidService
ADDED from AndroidManifest.xml:95:9
	android:name
		ADDED from AndroidManifest.xml:95:18
service#ru.alex009.purpose.sync.SyncService
ADDED from AndroidManifest.xml:96:9
	android:exported
		ADDED from AndroidManifest.xml:98:13
	android:name
		ADDED from AndroidManifest.xml:97:13
intent-filter#android.content.SyncAdapter
ADDED from AndroidManifest.xml:100:13
action#android.content.SyncAdapter
ADDED from AndroidManifest.xml:101:17
	android:name
		ADDED from AndroidManifest.xml:101:25
meta-data#android.content.SyncAdapter
ADDED from AndroidManifest.xml:104:13
	android:resource
		ADDED from AndroidManifest.xml:106:17
	android:name
		ADDED from AndroidManifest.xml:105:17
provider#ru.alex009.purpose.sync.SyncProvider
ADDED from AndroidManifest.xml:109:9
	android:syncable
		ADDED from AndroidManifest.xml:111:13
	android:exported
		ADDED from AndroidManifest.xml:112:13
	android:authorities
		ADDED from AndroidManifest.xml:113:13
	android:name
		ADDED from AndroidManifest.xml:110:13
receiver#ru.alex009.purpose.receivers.NotificationsAlarm
ADDED from AndroidManifest.xml:115:9
	android:exported
		ADDED from AndroidManifest.xml:118:13
	android:enabled
		ADDED from AndroidManifest.xml:117:13
	android:name
		ADDED from AndroidManifest.xml:116:13
receiver#ru.alex009.purpose.receivers.RemindersAlarm
ADDED from AndroidManifest.xml:119:9
	android:exported
		ADDED from AndroidManifest.xml:122:13
	android:enabled
		ADDED from AndroidManifest.xml:121:13
	android:name
		ADDED from AndroidManifest.xml:120:13
receiver#ru.alex009.purpose.receivers.AdvicesAlarm
ADDED from AndroidManifest.xml:123:9
	android:exported
		ADDED from AndroidManifest.xml:126:13
	android:enabled
		ADDED from AndroidManifest.xml:125:13
	android:name
		ADDED from AndroidManifest.xml:124:13
receiver#ru.alex009.purpose.receivers.SyncAlarm
ADDED from AndroidManifest.xml:127:9
	android:exported
		ADDED from AndroidManifest.xml:130:13
	android:enabled
		ADDED from AndroidManifest.xml:129:13
	android:name
		ADDED from AndroidManifest.xml:128:13
receiver#ru.alex009.purpose.receivers.BootCompleted
ADDED from AndroidManifest.xml:131:9
	android:exported
		ADDED from AndroidManifest.xml:134:13
	android:enabled
		ADDED from AndroidManifest.xml:133:13
	android:name
		ADDED from AndroidManifest.xml:132:13
intent-filter#android.intent.action.BOOT_COMPLETED
ADDED from AndroidManifest.xml:135:13
action#android.intent.action.BOOT_COMPLETED
ADDED from AndroidManifest.xml:136:17
	android:name
		ADDED from AndroidManifest.xml:136:25
uses-sdk
INJECTED from AndroidManifest.xml:0:0 reason: use-sdk injection requested
MERGED from com.android.support:support-v4:21.0.2:15:5
MERGED from com.android.support:appcompat-v7:21.0.2:15:5
MERGED from com.android.support:support-v4:21.0.2:15:5
MERGED from com.android.support:cardview-v7:21.0.2:15:5
MERGED from com.android.support:recyclerview-v7:21.0.2:15:5
MERGED from com.android.support:support-v4:21.0.2:15:5
MERGED from com.getbase:floatingactionbutton:1.1.0:8:5
MERGED from com.google.android.gms:play-services:6.1.71:15:5
MERGED from com.android.support:support-v4:21.0.2:15:5
	android:targetSdkVersion
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
activity#android.support.v7.widget.TestActivity
ADDED from com.android.support:recyclerview-v7:21.0.2:18:9
	android:label
		ADDED from com.android.support:recyclerview-v7:21.0.2:18:19
	android:name
		ADDED from com.android.support:recyclerview-v7:21.0.2:18:60
